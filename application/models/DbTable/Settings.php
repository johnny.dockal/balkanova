<?php
class Model_DbTable_Settings extends Zend_Db_Table_Abstract {

    protected $_name = 'settings';
    protected $_primary = 'setting_id';
    
    public function getAccountCZ() {
        $result = $this->find('account_cz');
        foreach ($result as $value) {
            $account = $value;
        }
        return $account;
    }
    
    public function getAccountSK() {
        $result = $this->find('account_sk');
        foreach ($result as $value) {
            $account = $value;
        }
        return $account;
    }
    
    public function getAccountEU() {
        $result = $this->find('account_sipo');
        foreach ($result as $value) {
            $account = $value;
        }
        return $account;
    }
    
    public function getExchangeRate() {
        $result = $this->find('kurz_euro');
        foreach ($result as $value) {
            $exchange = $value->setting_value1;
        }
        return $exchange;
    }
    
    public function getVAT() {
        $result = $this->find('vat');
        foreach ($result as $value) {
            $vat = $value->setting_value1;
        }
        return $vat;
    }

}
?>
