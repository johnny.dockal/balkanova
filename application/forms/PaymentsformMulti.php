<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Paymentsform extends Zend_Form {

    public function __construct($payments = null) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction('/admin/payments/save/');
        $this->setAttrib('class', 'admintable');

        if (!isset($payments)) {
            $payments = array('0');
            $i = 0;
        } else {
            $i = 1;
        }
        foreach ($payments as $value) {
            $setting_id = new Zend_Form_Element_Hidden($i.'payment_id');
            $setting_id->setDecorators(array('ViewHelper'));
            $public = new Zend_Form_Element_Radio($i.'public');
            $public->addMultiOptions(array(
                           '0'    => 'Zákázáno',
                           '1'    => 'Umožněno', 
                ));  
            $public->setLabel('Veřejné:')->setRequired(true);
            $wait = new Zend_Form_Element_Radio($i.'wait');
            $wait->addMultiOptions(array(
                           '1'    => 'Čekat na platbu před odesláním',
                           '0'    => 'Odeslat / připravit hned (dobírka, osobní vyzvednutí)', 
                )); 
            $wait->setLabel('Čekat na platbu před odesláním:')->setRequired(true);
            $text_cz = new Zend_Dojo_Form_Element_Textarea($i.'text_cz', array('class' => 'textbox'));
            $text_cz->setLabel('Doprovodný text česky')->setRequired(true);
            $text_en = new Zend_Dojo_Form_Element_Textarea($i.'text_en', array('class' => 'textbox'));
            $text_en->setLabel('Doprovodný text anglicky')->setRequired(true);
            $text_de= new Zend_Dojo_Form_Element_Textarea($i.'text_de', array('class' => 'textbox'));
            $text_de->setLabel('Doprovodný text německy')->setRequired(true);
            $price_cz = new Zend_Form_Element_Text($i.'price_cz', array('class' => 'textbox'));
            $price_cz->setLabel('Cena v korunách:')->setRequired(true); 
            $price_de = new Zend_Form_Element_Text($i.'price_de', array('class' => 'textbox'));
            $price_de->setLabel('Cena v eurech:')->setRequired(true);
            $price_percent = new Zend_Form_Element_Text($i.'price_percent', array('class' => 'textbox'));
            $price_percent->setLabel('Cena v procentech (přičte se výše uvedená částka - pokud je - plus uvedený počet procent z celého nákupu):')->setRequired(true);
            $this->addElements(array($setting_id, $public, $wait, $text_cz, $text_en, $text_de, $price_cz, $price_de, $price_percent));
            $groupTitle = (empty($value['title_cz'])) ? "Nový způsob platby" : $value['title_cz'] . " (payment_id: " . $value['payment_id'] . ")";
            $this->addDisplayGroup(array(  
                    $i.'public',
                    $i.'wait',
                    $i.'text_cz',
                    $i.'text_en',
                    $i.'text_de',
                    $i.'price_cz',                
                    $i.'price_de',
                    $i.'price_percent'
            ),$i.'group',array('legend' => $groupTitle));
            $group = $this->getDisplayGroup($i.'group');
            $group->setDecorators(array(        
                    'FormElements',
                    'Fieldset',
                    array('HtmlTag',array('tag'=>'div'))
            ));
            $i++;
        }
        
        $count = new Zend_Form_Element_Hidden('count');
        $count->setValue($i);
        $count->removeDecorator('label');
        $this->addElement($count);

        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "button bg-green border-style active"));
        $this->addElement($submit);
    }

}
