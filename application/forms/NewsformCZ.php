<?php
class Form_NewsformCZ extends Zend_Form {

    public function __construct($action = null) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction($action);        
        $this->setAttrib('enctype', 'multipart/form-data');
        $this->setAttrib('class', 'admintable');
        
        //momentálně je formulář nastaven na dva jazyky (cz a en), nevím jak ho upravit pro libovolný počet jazyků z configu
        $news_id = new Zend_Form_Element_Hidden('news_id');
        //zabání zobrazení labelu
        $news_id->setDecorators(array('ViewHelper'));
        
        $select = new Zend_Form_Element_Select('eshop_select');  
        $model = new Model_DbTable_EshopShops();
        $eshops = $model->fetchEshops();
        foreach ($eshops as $value) {
            $select->addMultiOption($value['eshop_id'], $value['title']);
        }        
        $select->setLabel('Eshop:')->setRequired(true);
        
        $date = new Zend_Dojo_Form_Element_TextBox('date');
        $date->setLabel('Datum (YYYY-MM-DD):')->setRequired(true);
        $date->setValue(date("Y-m-d"));
        
        $title_cz = new Zend_Dojo_Form_Element_TextBox('title_cz', array('class' => 'textboxwide'));
        $title_cz->setLabel('Nadpis česky (nepovinný):');
        
        /*$link_cz = new Zend_Dojo_Form_Element_TextBox('link_cz', array('class' => 'textboxwide'));
        $link_cz->setLabel('Odkaz na lokaci či event (lze nechat prázdné):')->setRequired(true);*/

        $text_cz = new Zend_Dojo_Form_Element_Textarea('text_cz', array('class' => "textboxhuge"));
        $text_cz->setLabel('Text česky:')->setRequired(true);
        
        $image_upload = new Zend_Form_Element_File('upload');
        $image_upload->setLabel('Nahrát obrázek k novince:');
        $image_upload->addValidator('Count', false, 1);
        //$image->addValidator('Size', false, 10240000);
        $image_upload->addValidator('Extension', false, 'jpg,JPG');
        
        $image_select = new Zend_Form_Element_Radio('image');
        $image_select->setLabel('Vybrat obrázek k novince:');
        $imgDir = dir(getcwd().'/images/eshop_news');
        while($soubor = $imgDir->read()) {
            if ($soubor == "." || $soubor ==".." || $soubor == "Thumbs.db") {continue;}            
            $image_select->addMultiOption($soubor, $soubor);
        }
        $imgDir->close();
    
        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "savebutton"));

        $this->addElements(array(
            $news_id, $select, $date, $title_cz, $text_cz, $image_upload, $image_select, $submit
        ));
    }
}
