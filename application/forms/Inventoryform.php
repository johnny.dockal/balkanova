<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Inventoryform extends Zend_Form {

    public function __construct($action = null, $productArray = null) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('enctype', 'multipart/form-data');

        $i = 1;
        $j = 0;
        foreach ($productArray as $value) {
            $j++;
            $$i = new Zend_Form_Element_Hidden('product_id_' . $j, array('class' => 'nodisplay', 'value' => $value['product_id']));
            $$i->setLabel('Počet objednaných kusů')->setRequired(true);
            $this->addElement($$i);
            $i++;
            $$i = new Zend_Dojo_Form_Element_TextBox('inv2_' . $j, array('class' => 'textboxtiny'));
            $$i->setLabel('Počet kusů fyzicky na krámě')->setRequired(true);
            $this->addElement($$i);
            $i++;
            $$i = new Zend_Dojo_Form_Element_TextBox('inv3_' . $j, array('class' => 'textboxtiny'));
            $$i->setLabel('Počet kusů fyzicky na skladě')->setRequired(true);
            $this->addElement($$i);
            $i++;
            $this->addDisplayGroup(array(
                'inv2_' . $j, 'inv3_' . $j,
                    ), 'group' . $j, array('legend' => $value['title']));
            $group = $this->getDisplayGroup('group' . $j);
            $group->setDecorators(array(
                'FormElements',
                'Fieldset',
                array('HtmlTag', array('tag' => 'div'))
            ));
        }

        $count = new Zend_Form_Element_Hidden('count', array('class' => 'nodisplay', 'value' => $j));
        $count->removeDecorator('Label');

        $this->addElement($count);

        $save = new Zend_Form_Element_Submit('save', array('label' => "Uložit", 'class' => "button bg-green border-style active", 'style' => 'width: 99%;'));

        $this->addElement($save);
    }

}
