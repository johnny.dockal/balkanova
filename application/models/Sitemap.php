<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of categories
 *
 * @author owner
 */
class Model_Sitemap extends Zend_Db_Table_Abstract {

	function getSitemap() {		
		$db = Zend_Db_Table::getDefaultAdapter();

		//na�teme kategorie
		$category_result = $db->fetchAll("SELECT category_url, category_full_title, category_ID FROM view_categories_$lang ORDER BY category_sequence ASC");
		$i = 1;
		
		foreach ($category_result as $category_row) {
			$sitemap[$i]["level"]=1;
			$sitemap[$i]["full_title"]=$category_row["category_full_title"];
			$sitemap[$i]["url"]=$category_row["category_url"];
			$parent_url=$category_row["category_url"];
			
			//na�teme subkategorie
			// POZOR - subcategory_full_title!='' pro skryt� pomocn�ch subkategori� (= t�ch, kter� prakticky nejsou pou��v�ny)
			$subcategory_result = $db->fetchAll("SELECT subcategory_url, subcategory_full_title FROM view_subcategories_$lang WHERE category_ID='".$category_row["category_ID"]."' AND subcategory_full_title!='' ORDER BY subcategory_sequence ASC");
			
			foreach ($subcategory_result as $subcategory_row) {
				$i++;
				$sitemap[$i]["level"]=2;
				$sitemap[$i]["full_title"]=$subcategory_row["subcategory_full_title"];
				$sitemap[$i]["url"]=$parent_url."/".$subcategory_row["subcategory_url"];
			}
			$i++;
		}
                
		return $sitemap;
	}
	
	
	function getXmlSitemap($lang)
	{
		
		if ($lang == 'de') {$lang = 'cz';}
		
		$db = Zend_Registry::get('db');

		//na�teme kategorie
		$category_result = $db->fetchAll("SELECT category_url, category_ID FROM view_categories_$lang ORDER BY category_sequence ASC");
		$i = 1;
		
		foreach ($category_result as $category_row) {
			$sitemap[$i]["level"]=1;
			$sitemap[$i]["url"]=$category_row["category_url"];
			$category_url=$category_row["category_url"];
			
			//na�teme subkategorie
			// POZOR - subcategory_full_title!='' pro skryt� pomocn�ch subkategori� (= t�ch, kter� prakticky nejsou pou��v�ny)
			$subcategory_result = $db->fetchAll("SELECT subcategory_ID, subcategory_url, subcategory_full_title FROM view_subcategories_$lang WHERE category_ID='".$category_row["category_ID"]."'  ORDER BY subcategory_sequence ASC");
			
			foreach ($subcategory_result as $subcategory_row) {
				
				if($subcategory_row["subcategory_full_title"]!='')
				{
				  $i++;
				  $sitemap[$i]["level"]=2;
				  $sitemap[$i]["full_title"]=$subcategory_row["subcategory_full_title"];
				  $sitemap[$i]["url"]=$category_url."/".$subcategory_row["subcategory_url"];
				  $subcategory_url=$category_url."/".$subcategory_row["subcategory_url"];
				  
				  $product_result = $db->fetchAll("SELECT product_url FROM view_products_specifics_$lang WHERE subcategory_ID='".$subcategory_row["subcategory_ID"]."' AND product_status != 'neaktivni' AND product_url!='' ORDER BY product_full_title ASC");
			  
				  foreach ($product_result as $product_row) {
				  $i++;
				  $sitemap[$i]["url"]=$subcategory_url."/".$product_row["product_url"];
				  }
				}
				
				else
				{
				  $product_result = $db->fetchAll("SELECT product_url FROM view_products_specifics_$lang WHERE subcategory_ID='".$subcategory_row["subcategory_ID"]."' AND product_status != 'neaktivni' AND product_url!='' ORDER BY product_full_title ASC");
			  
				  foreach ($product_result as $product_row) {
				  $i++;
				  $sitemap[$i]["url"]=$category_url."/".$product_row["product_url"];
				  }
				}
					
				
			}
			$i++;
		}
		

		return $sitemap;

	}
	

 }
?>
