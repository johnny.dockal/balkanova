<?php

class Admin_InventoryController extends Zend_Controller_Action {

    private $category_id = 'all';
    private $page = 1;
    private $order = 'product_id';
    private $sort = 'desc';
    private $limit = 50;
    private $productArray = null;

    public function init() {
        $this->view->headTitle('Inventář', 'POSTEND');
    }

    public function preDispatch() {
        $category_id = $this->getParam('category_id');
        $page = $this->getParam('page');
        $order = $this->getParam('order');
        $sort = $this->getParam('sort');

        $adminSession = new Zend_Session_Namespace('Admin');

        if (!empty($category_id)) {
            $this->category_id = $category_id;
            $adminSession->category_id = $category_id;
            //pokus se mění kategorie, musí se jít zpátky na stránku 1, aby nedošlo k out of bounds exception
            $adminSession->page = $this->page;
        } else {
            $this->category_id = $adminSession->category_id;
        }
        if (empty($adminSession->category_id)) {
            $adminSession->category_id = $this->category_id;
        }
        //nastavíme stránku
        if (!empty($page)) {
            $this->page = $page;
            $adminSession->page = $page;
        } else if (empty($adminSession->page)) {
            $adminSession->page = $this->page;
        } else {
            $this->page = $adminSession->page;
        }
        //nastavíme pořadí
        if (!empty($order)) {
            $this->order = $order;
            $adminSession->order = $order;
        } else {
            $this->order = $adminSession->order;
        }
        if (empty($adminSession->order)) {
            $adminSession->order = $this->order;
        }
        if (!empty($sort)) {
            $this->sort = $sort;
            $adminSession->sort = $sort;
        } else {
            $this->sort = $adminSession->sort;
        }
        if (empty($adminSession->sort)) {
            $adminSession->sort = $this->sort;
        }
        $this->view->category_id = $adminSession->category_id;
        $this->view->page = $adminSession->page;
        $this->view->order = $adminSession->order;
        $this->view->sort = $adminSession->sort;
    }

    public function indexAction() {        
        $search = $this->getParam('search');
        $order  = $this->getParam('order');
        if (!isset($order)) {$order = 'product_id';}
        $model  = new Model_DbTable_EshopProductHistory();
        if (!empty($search)) {
            $products = $model->searchInventory($search);
        } else {
            $products = $model->fetchInventory(null, $order);
        }
        $this->view->order = $order; 
        $this->view->products = $products;
    }

    public function selectAction() {
        $categories = new Model_DbTable_EshopCategories();
        $model = new Model_DbTable_EshopProducts();
        $products = $model->fetchProductsAdmin($this->category_id, $this->order);
        //$products = $model->fetchProductsByCat();
        $this->view->categories = $categories->fetchAll()->toArray();
        $this->view->itemlimit = $this->limit;
        $this->view->itemcount = count($products);
        $this->view->pagecount = ceil($this->view->itemcount / $this->limit);
        $this->view->products = array_chunk($products, $this->limit);
    }

    public function editAction() {
        $model = new Model_DbTable_EshopProducts();
        $products = $model->fetchProductsByIds($this->getRequest()->getPost());
        $form = new Form_Inventoryform('/admin/inventory/add/', $products);
        $this->view->form = $form;
    }

    public function addAction() {
        $model = new Model_DbTable_EshopProductHistory();
        $user = Zend_Auth::getInstance()->getIdentity();
        $j = 1;
        while ($j <= $this->getParam('count')) {
            $data = array(
                'product_id' => $this->getParam(('product_id_' . $j)),
                'user_id' => $user->user_id,
                'operation' => 'Přidání do inventáře',
                'inv2a' => $this->getParam('inv2_' . $j),                
                'inv2b' => $this->getParam('inv2_' . $j),
                'inv2r' => $this->getParam('inv2_' . $j),
                'inv3a' => $this->getParam('inv3_' . $j),
                'inv3b' => $this->getParam('inv3_' . $j),
                'inv3r' => $this->getParam('inv3_' . $j)
            );
            $model->insert($data);
            $j++;
        }
        $this->_redirect('/admin/inventory/');
    }
    
    public function historyAction() {
        $product_id = $this->getParam('product_id');
        $model = new Model_DbTable_EshopProductHistory();
        $this->view->history = $model->fetchHistory($product_id);
    }
    
    public function deleteAction() {
        $product_id = $this->getParam('product_id');
        $product_history_id = $this->getParam('product_history_id');
        $model = new Model_DbTable_EshopProductHistory();
        $where = $model->getAdapter()->quoteInto('product_history_id = ?', $product_history_id);
        $model->delete($where);
        $this->_redirect('/admin/inventory/history/?product_id='.$product_id);
    }

    public function updateAction() {
        $model = new Model_DbTable_EshopProductHistory();
        $product_id = $this->getParam('product_id');
        $quantity = $this->getParam('quantity');
        $operation = $this->getParam('operation');
        $redirect = $this->getParam('redirect');
        if ($model->updateInventory($product_id, $operation, $quantity)) {
            $this->_redirect($redirect);
        } else {
            echo "<p>".$model->getMessage()."</p>";
        } 
    }

    public function saveAction() {
        
    }

}
