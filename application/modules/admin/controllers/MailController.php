<?php

class Admin_MailController extends Zend_Controller_Action
{

    public function init() {
    }
    
    private function checkRelevance() {
        $hour       = date('H');
        $day        = date('w');
        $send       = false;
        //zkusíme, zda je správná hodina
        if ($hour >= 09 && $hour <= 13) {
            $send = true;
        } else {
            echo "<p>Jídelní lístky lze odesílat pouze dopoledne!</p>";
            $send = false;
        }
        //zkusíme, zda je správný den (Po až Pá)
        if ($day >= 1 && $day <= 5) {
            $send = true;
        } else {
            echo "<p>Jídelní lístky lze odesílat pouze v pracovní den!</p>";
            $send = false;
        }
        return $send;
    }
    
    public function indexAction() {
        $token      = $this->getParam('token');
        $today      = date('Y-m-d');
        
        //zjistíme, zda je vůbec co odeslat, pokud není, odešle se upozornění na uvedené adresy, že někdo zapoměl vyplnit lístek
        $mail       = new Zend_Mail('utf-8');
        $menumodel  = new Model_DbTable_Menus();
        $menu       = $menumodel->fetchRow("menu_id = '$today'");
        
        //pokud je vyplněná nabídka a souhlasí token od kronu, můžeme posílat maily
        if (!empty($menu->menu_cz) && $token == 'sdfvsdhgnbfghjnmghjknh') {             
            echo "<p>Token souhlasí!</p>";
            $send = true;  
        //pokud souhlasí token, tak je funkce spouštěna z cronu a měl by se odeslat varovný email    
        } else if (empty($menu->menu_cz) && $token == 'sdfvsdhgnbfghjnmghjknh') {           
            echo "<p>Token souhlasí, ale není vyplněna nabídka!</p>";      
            try {
                $mail->setSubject("Polední nabídka ke dni ".date('d. n. Y')." NENÍ ROZESLÁNA!");
                $mail->setBodyText('Pozor! Vzhledem k tomu, že není vyplněna polední nabídka na tento den se neodeslal automatický email. Je třeba nejdřív vyplnit nabídku a pak odeslat mail manuálně.');
                $mail->setFrom('info@pivovarskyklub.com', 'Pivovarský klub');  
                $mail->addTo('jan_dockal@seznam.cz');
                $mail->send();
            } catch (Exception $e) {
                $e;
            } 
            $send = false;
        //pokud je nabídka na den prázdná, ale chybí token (někdo se to snaží spouštět manuálně) tak se vypíše chyba!
        } else {        
            echo "<p>Všechno špatně!</p>";
            $send = false;
        }
        
        //pokud jsou splněny předchozí podmínky, jedeme dál
        if ($this->checkRelevance() && $send) {
            $mailmodel  = new Model_DbTable_Mailinglist();            
            $recipients = $mailmodel->fetchAllActive();
            
            //připojíme k emailu "akci", nebo doplňující informaci či co...
            $modelTexts = new Model_DbTable_Texts();
            $texts = $modelTexts->fetchTexts();
            $akce = $texts['akce']['text'];
            
            //$mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
            $mail->setSubject("Polední nabídka ke dni ".date('d. n. Y'));
            $mail->setFrom('info@pivovarskyklub.com', 'Pivovarský klub');     
            
            echo "<table class='admintable'>";
            echo "<th><tr>";
            echo "<td>email</td><td>naposledy odeslán</td><td>status</td>";
            echo "</tr></th>";
            foreach ($recipients as $value) {       
                echo "<tr>";
                echo "<td>".$value->mail."</td>";
                echo "<td>".$value->last_sent."</td>";
                
                if ($today == $value->last_sent) {
                    echo "<td>Email byl dnes již odeslán!</td>";
                } else {
                    try {
                        //pro každého uživatele se generuje link na odhlášení zvlášť
                        $link = "<br/><br/>Pokud již nechcete tyto polední nabídky přijímat, prosím klikněte na odkaz <a href='http://www.pivovarskyklub.com/mailinglist/delete?token=$value->token'>[ODHLÁSIT]</a>";
                        $mail->clearRecipients();
                        $mail->addTo($value->mail);
                        $mail->setBodyHtml($menu->menu_cz.$akce.$link);
                        $mail->send();
                        $mailmodel->emailSent($value->mail_id);
                        echo "<td>Zend: Email odeslán!</td>";
                    } catch (Exception $e) {
                        echo "<td>Zend: Něco selhalo!</td><pre>".$e."</pre>";
                    }
                }
                echo "</tr>";
            } 
            echo "</table>";
        } else {
            try {
                $mail->setSubject("Polední nabídka ke dni ".date('d. n. Y')." NENÍ ROZESLÁNA!");
                $mail->setBodyText('Lístek se neodeslal, nevím proč.');
                $mail->setFrom('info@pivovarskyklub.com', 'Pivovarský klub');  
                $mail->addTo('jan_dockal@seznam.cz');
                $mail->send();
            } catch (Exception $e) {
                echo "<td>Zend: Něco selhalo!</td><pre>".$e."</pre>";
            } 
        }  
    }
    
    public function confirmAction() {
        $order_id = $this->getParam('order_id');         
        $status_id = $this->getParam('status_id'); 
        
        $order = new Model_EshopOrder($order_id);
        $model = new Model_DbTable_Texts();        
        
        //tohle je ochcávka kvůli tomu aby šlo odeslat email i u ukončených objednávek
        //u ukončených objednávek se totiž vrací NULL jako next status
        /* $nextstatus = $order->getNextStatus(); */      
        if ($order->getShippingId() == '1') {
            $text = $model->fetchTextName('emailready');
        } else {
            $text = $model->fetchTextName('emailsent');
        }
        $data = array(
            'order_id' => $order_id,
            'status_id' => $status_id, 
            'email' => $order->getEmail(),
            'subject' => $text['title'],
            'text' => $text['text'].$order->getProductTable($order)
        );
        /*if ($order->checkConfirmationSent()) {
            echo "<h1>Email o zásilce byl již odeslán a nelze jej odeslat znovu.</h2>";
        } else {  */          
            $form = new Form_Mailform('/admin/mail/send/');
            $form->populate($data);
            $this->view->form = $form;
        //}
        //$data['status_id'] = $status_id;
        $skip = new Form_Skipform('/admin/orders/status/');
        $skip->populate($data);
        $this->view->skip = $skip;
    }

    public function sendAction() {
        $order_id = $this->getParam('order_id'); 
        $email = $this->getParam('email');
        $subject = $this->getParam('subject'); 
        $text = $this->getParam('text');         
        $status_id = $this->getParam('status_id');
                
        $mail = new Zend_Mail('utf-8');
        $mail->setFrom('info@balkanova.cz', 'Balkanova E-shop');
        $mail->addTo($email);
        $mail->setBodyHtml($text);    
        $mail->setSubject($subject);
        
        try {
            $mail->send();
        } catch (Zend_Exception $e) {
            echo "Caught exception ".__METHOD__.": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
        }
        if (!empty($status_id)) {
            $this->_redirect('/admin/orders/status/?order_id='.$order_id.'&status_id='.$status_id.'&mailaction=sent');
        }        
    }  
    
}


