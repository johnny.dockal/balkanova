<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopProductHistory extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_product_history';
    protected $_primary = 'product_history_id';
    protected $_limit = 30;
    protected $_productQuery_grouped = null;
    protected $_productQuery = null;
    protected $_productQueryEdit = null;
    protected $_message = null;
    private $sendmail = false;

    public function init() {
        $session = new Zend_Session_Namespace('Default');
        //v německém locale je správná informace uložena v kolonce pro češtinu
        if ($session->lang == 'de') {
            $this->lang_cat_subcat = 'cz';
        } else {
            $this->lang_cat_subcat = $session->lang;
        }
        $this->lang = $session->lang;
        $this->_productQuery = "SELECT "
                . "GROUP_CONCAT(c.category_id SEPARATOR ';') AS category_id, "
                . "GROUP_CONCAT(s.subcategory_id SEPARATOR ';') AS subcategory_id, "
                . "p.product_id, "
                . "p.status, "
                . "p.code, "
                . "p.sequence, "
                . "p.title_$this->lang AS title, "
                . "p.alias_$this->lang AS alias, "
                . "p.full_title_$this->lang AS full_title, "
                . "p.text_$this->lang AS text, "
                . "p.note_$this->lang AS note, "
                . "p.material_$this->lang AS material, "
                . "p.manufactured_$this->lang AS manufactured, "
                . "p.price_unit_" . APP_LOCALE . " AS price_unit, "
                . "p.price_" . APP_LOCALE . " AS price, "
                . "p.size, "
                . "p.weight, "
                . "p.weight_unit, "
                . "p.grams, "
                . "p.hair, "
                . "p.certified, "
                . "p.new "
                //. "(ph.inv2b + ph.inv3b) AS available, "
                //. "ph.timestamp, ph.inv1a, ph.inv1b, ph.inv2a, ph.inv2b, ph.inv3a, ph.inv3b, "
                //. "ph.timestamp "
                . "FROM eshop_products AS p "
                . "JOIN eshop_product_history AS ph ON (ph.product_id = p.product_id) "
                . "LEFT JOIN (eshop_subcat_products AS sp, eshop_subcategories AS s, eshop_categories AS c) "
                . "ON (sp.product_id = p.product_id AND s.subcategory_id = sp.subcategory_id AND c.category_id = s.category_id) ";
        //. "WHERE product_history_id = (SELECT MAX(product_history_id) FROM eshop_product_history AS h2 WHERE ph.product_id = h2.product_id) ";
    }

    public function fetchInventory($category_id = null, $order = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $this->_productQuery;
        if (isset($category_id) and ( $category_id != 'all')) {
            $sql .= "AND c.category_id = '$category_id' ";
        }
        $sql .= "GROUP BY ph.product_id ";
        if (isset($order)) {
            if ($order == 'title') {
                $order = "p.title_$this->lang";
            }
            $sql .= "ORDER BY '$order' ";
        }
        try {
            $result = $db->fetchAll($sql);
            foreach ($result as $data) {
                $products[$data['product_id']] = new Model_EshopProduct($data);
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $products;
    }

    public function fetchInventoryProduct($product_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT 
                        p.product_id, 
                        status,
                        code,
                        sequence,
                        alias_cz AS alias,                        
                        title_cz AS title,
                        text_cz AS text,
                        price_cz,
                        timestamp, inv1a, inv1b, inv2a, inv2b, inv3a, inv3b 
                    FROM eshop_product_history AS h1
                    JOIN eshop_products AS p ON (h1.product_id = p.product_id)
                    WHERE product_history_id = (SELECT MAX(product_history_id) FROM eshop_product_history AS h2 WHERE h1.product_id = h2.product_id) ";
        if (isset($product_id)) {
            $sql .= "AND h1.product_id = '$product_id' ";
        }
        $sql .= "GROUP BY h1.product_id ";
        try {
            if (isset($product_id)) {
                $products = $db->fetchRow($sql);
            } else {
                $products = $db->fetchAll($sql);
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $products;
    }

    public function searchInventory($search) {
        $db = Zend_Db_Table::getDefaultAdapter();
        //$sql = $this->_productQuery . " WHERE p.title_cz LIKE LOWER('%$search%') GROUP BY p.product_id ";
        $sql = "SELECT 
                        p.product_id, 
                        status,
                        code,
                        sequence,
                        alias_cz AS alias,                        
                        title_cz AS title,
                        text_cz AS text,
                        price_cz,
                        timestamp, inv1a, inv1b, inv2a, inv2b, inv3a, inv3b 
                    FROM eshop_product_history AS h1
                    JOIN eshop_products AS p ON (h1.product_id = p.product_id)
                    WHERE LOWER (title_cz) LIKE LOWER('%$search%')"
                . "GROUP BY h1.product_id ";
        //echo "<p>$sql</p>";
        try {
            $result = $db->fetchAll($sql);
            foreach ($result as $data) {
                $products[$data['product_id']] = new Model_EshopProduct($data);
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $products;
    }

    public function fetchHistory($product_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT h.product_history_id, p.product_id, u.user_id, order_id, p.title_cz AS title, operation, u.user_name, timestamp, 
                        inv1l, inv1a, inv1b, inv1r, inv2l, inv2a, inv2b, inv2r, inv3l, inv3a, inv3b, inv3r
                    FROM eshop_product_history AS h
                    JOIN eshop_products AS p ON (h.product_id = p.product_id)
                    LEFT JOIN users AS u ON (u.user_id = h.user_id)
                    WHERE p.product_id = '$product_id'";
        try {
            $history = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $history;
    }

    public function fetchInventoryNumbers($product_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT product_history_id, product_id, (inv2b + inv3b) AS available, inv1a, inv1b, inv2a, inv2b, inv3a, inv3b "
                . "FROM eshop_product_history AS h "
                . "WHERE h.product_id = '$product_id' "
                . "ORDER BY product_history_id DESC "
                . "LIMIT 1";
        try {
            $result = $db->fetchRow($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }

    public function fetchAvailable($product_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        if (is_array($product_id)) {
            $sql = "SELECT p.product_id, (inv2b + inv3b) AS available, inv1a, inv1b, inv2a, inv2b, inv3a, inv3b
                    FROM eshop_product_history AS h
                    JOIN eshop_products AS p ON (h.product_id = p.product_id)
                    WHERE p.product_id IN (" . implode(",", $product_id) . ")";
        } else {
            $sql = "SELECT product_history_id, inv1a, inv1b, inv2a, inv2b, inv3a, inv3b "
                    . "FROM eshop_product_history AS h "
                    . "JOIN eshop_products AS p ON (h.product_id = p.product_id) "
                    . "WHERE p.product_id = '$product_id' "
                    . "ORDER BY product_history_id DESC "
                    . "LIMIT 1";
        }
        try {
            if (is_array($product_id)) {
                $result = $db->fetchAssoc($sql);
            } else {
                $result = $db->fetchRow($sql);
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        if (empty($result)) {
            return null;
        } else if (is_array($product_id)) {
            return $result;
        } else {
            return ($result['inv2b'] + $result['inv3b']);
        }
    }

    public function updateInventory($product_id, $operation, $quantity, $order_id = null) {
        $prevData = $this->fetchInventoryProduct($product_id);
        if (empty($prevData)) {
            return 0;
        }
        $user = Zend_Auth::getInstance()->getIdentity();
        $cando = true;
        if (APP_ENV == 'production') {
            $debug = false;
        } else {
            $debug = true;
        }
        switch ($operation) {
            case 'order':
                $data['product_id'] = $product_id;
                $data['order_id'] = $order_id;
                $data['operation'] = 'Objednávka č. ' . $order_id;
                $data['inv1a'] = $prevData['inv1a'] + $quantity;
                $data['inv1b'] = $prevData['inv1b'];
                $data['inv1r'] = 0 + $quantity;
                $data['inv2l'] = 0 - $quantity;
                $data['inv2a'] = $prevData['inv2a'];
                $data['inv2b'] = $prevData['inv2b'] - $quantity;
                $data['inv3a'] = $prevData['inv3a'];
                $data['inv3b'] = $prevData['inv3b'];
                break;
            case 'cancel':
                $data['product_id'] = $product_id;
                $data['order_id'] = $order_id;
                $data['user_id'] = $user->user_id;
                $data['operation'] = 'Zrušení objednávky č. ' . $order_id;
                $data['inv1a'] = $prevData['inv1a'] - $quantity;
                $data['inv1b'] = $prevData['inv1b'];
                $data['inv1l'] = 0 - $quantity;
                $data['inv2l'] = 0 + $quantity;
                $data['inv2a'] = $prevData['inv2a'];
                $data['inv2b'] = $prevData['inv2b'] + $quantity;
                $data['inv3a'] = $prevData['inv3a'];
                $data['inv3b'] = $prevData['inv3b'];
                break;
            case 'cancelshipped':
                $data['product_id'] = $product_id;
                $data['order_id'] = $order_id;
                $data['user_id'] = $user->user_id;
                $data['operation'] = 'Zrušení expedované objednávky č. ' . $order_id;
                $data['inv1a'] = $prevData['inv1a'];
                $data['inv1b'] = $prevData['inv1b'];
                $data['inv2l'] = 0 + $quantity;
                $data['inv2a'] = $prevData['inv2a'] + $quantity;
                $data['inv2b'] = $prevData['inv2b'] + $quantity;
                $data['inv3a'] = $prevData['inv3a'];
                $data['inv3b'] = $prevData['inv3b'];
                break;
            case 'addstorage' :
                $data['product_id'] = $product_id;
                $data['order_id'] = null;
                $data['user_id'] = $user->user_id;
                $data['operation'] = 'Doplnění do skladu';
                $data['inv1a'] = $prevData['inv1a'];
                $data['inv1b'] = $prevData['inv1b'];
                $data['inv2a'] = $prevData['inv2a'];
                $data['inv2b'] = $prevData['inv2b'];
                $data['inv3r'] = 0 + $quantity;
                $data['inv3a'] = $prevData['inv3a'] + $quantity;
                $data['inv3b'] = $prevData['inv3b'] + $quantity;
                break;
            case 'addshop' :
                $data['product_id'] = $product_id;
                $data['order_id'] = null;
                $data['user_id'] = $user->user_id;
                $data['operation'] = 'Doplnění na obchod';
                $data['inv1a'] = $prevData['inv1a'];
                $data['inv1b'] = $prevData['inv1b'];
                $data['inv2r'] = 0 + $quantity;
                $data['inv2a'] = $prevData['inv2a'] + $quantity;
                $data['inv2b'] = $prevData['inv2b'] + $quantity;
                $data['inv3a'] = $prevData['inv3a'];
                $data['inv3b'] = $prevData['inv3b'];
                break;
            case 'move' :
                if ($quantity > $prevData['inv3a']) {
                    $cando = false;
                    $this->message = 'Nelze přemístit víc zboží, než máte fyzicky na skladě.';
                } else {
                    $data['product_id'] = $product_id;
                    $data['user_id'] = $user->user_id;
                    $data['order_id'] = null;
                    $data['operation'] = 'Přemístění ze skladu na krám';
                    $data['inv1a'] = $prevData['inv1a'];
                    $data['inv1b'] = $prevData['inv1b'];
                    $data['inv2a'] = $prevData['inv2a'] + $quantity;
                    $data['inv2b'] = $prevData['inv2b'] + $quantity;
                    $data['inv2r'] = 0 + $quantity;
                    $data['inv3l'] = 0 - $quantity;
                    $data['inv3a'] = $prevData['inv3a'] - $quantity;
                    $data['inv3b'] = $prevData['inv3b'] - $quantity;
                }
                break;
            case 'sale':
                $data['product_id'] = $product_id;
                $data['order_id'] = $order_id;
                $data['user_id'] = $user->user_id;
                $data['operation'] = 'Prodej na obchodě č. ' . $order_id;
                $data['inv1a'] = $prevData['inv1a'];
                $data['inv1b'] = $prevData['inv1b'];
                $data['inv2a'] = $prevData['inv2a'];
                $data['inv2b'] = $prevData['inv2b'];
                $data['inv3a'] = $prevData['inv3a'];
                $data['inv3b'] = $prevData['inv3b'];
                $shop = 0;
                $storage = 0;
                for ($i = 1; $i <= $quantity; $i++) {
                    //pokud je v krámě dostatek zboží, tak se objednávka odečte z krámu
                    if ((int) $data['inv2a'] >= 1) {
                        $data['inv2a'] --;
                        $data['inv2b'] --;
                        $shop++;
                        //pokud na krámu není dost zboží, tak se to odečte ze skladu a odešle se email    
                    } else {
                        $data['inv3a'] --;
                        $data['inv3b'] --;
                        $storage++;
                        $this->sendmail = true;
                    }
                }
                $data['inv2l'] = 0 - $shop;
                $data['inv3l'] = 0 - $storage;
                break;
            case 'dispatch':
                $data['product_id'] = $product_id;
                $data['order_id'] = $order_id;
                $data['user_id'] = $user->user_id;
                $data['operation'] = 'Expedování objednávky č. ' . $order_id;
                $data['inv1l'] = 0 - $quantity;
                $data['inv1a'] = $prevData['inv1a'] - $quantity;
                $data['inv1b'] = $prevData['inv1b'];
                $data['inv2a'] = $prevData['inv2a'];
                $data['inv2b'] = $prevData['inv2b'];
                $data['inv3a'] = $prevData['inv3a'];
                $data['inv3b'] = $prevData['inv3b'];
                $shop = 0;
                $storage = 0;
                for ($i = 1; $i <= $quantity; $i++) {
                    //pokud je v krámě dostatek zboží, tak se objednávka odečte z krámu
                    if ((int) $data['inv2a'] >= 1) {
                        $data['inv2a'] = $data['inv2a'] - 1;
                        $shop++;
                        //pokud na krámu není dost zboží, tak se to odečte ze skladu a odešle se email    
                    } else {
                        $data['inv2b'] = $data['inv2b'] + 1;
                        $data['inv3a'] = $data['inv3a'] - 1;
                        $data['inv3b'] = $data['inv3b'] - 1;
                        $storage++;
                        $this->sendmail = true;
                    }
                }
                $data['inv2l'] = 0 - $shop;
                $data['inv3l'] = 0 - $storage;
                break;
            case 'handover':
                $data['product_id'] = $product_id;
                $data['order_id'] = $order_id;
                $data['user_id'] = $user->user_id;
                $data['operation'] = 'Expedování objednávky č. ' . $order_id;
                $data['inv1l'] = 0 - $quantity;
                $data['inv1a'] = $prevData['inv1a'] - $quantity;
                $data['inv1b'] = $prevData['inv1b'];
                $data['inv2a'] = $prevData['inv2a'];
                $data['inv2b'] = $prevData['inv2b'];
                $data['inv3a'] = $prevData['inv3a'];
                $data['inv3b'] = $prevData['inv3b'];
                $shop = 0;
                $storage = 0;
                for ($i = 1; $i <= $quantity; $i++) {
                    //pokud je v krámě dostatek zboží, tak se objednávka odečte z krámu
                    if ((int) $data['inv2a'] >= 1) {
                        $data['inv2a'] = $data['inv2a'] - 1;
                        $shop++;
                        //pokud na krámu není dost zboží, tak se to odečte ze skladu a odešle se email    
                    } else {
                        $data['inv2b'] = $data['inv2b'] + 1;
                        $data['inv3a'] = $data['inv3a'] - 1;
                        $data['inv3b'] = $data['inv3b'] - 1;
                        $storage++;
                        $this->sendmail = true;
                    }
                }
                $data['inv2l'] = 0 - $shop;
                $data['inv3l'] = 0 - $storage;
                break;
            case 'writeoffshop':
                if ($quantity > $prevData['inv2a']) {
                    $cando = false;
                    $this->message = 'Nelze odepsat víc zboží, než máte fyzicky na obchodě.';
                } else {
                    $data['product_id'] = $product_id;
                    $data['order_id'] = $order_id;
                    $data['user_id'] = $user->user_id;
                    $data['operation'] = 'Odpis zboží';
                    $data['inv1a'] = $prevData['inv1a'];
                    $data['inv1b'] = $prevData['inv1b'];
                    $data['inv2l'] = 0 - $quantity;
                    $data['inv2a'] = $prevData['inv2a'] - $quantity;
                    $data['inv2b'] = $prevData['inv2b'] - $quantity;
                    $data['inv3a'] = $prevData['inv3a'];
                    $data['inv3b'] = $prevData['inv3b'];
                }
                break;
            case 'writeoffstore':
                if ($quantity > $prevData['inv3a']) {
                    $cando = false;
                    $this->message = 'Nelze odepsat víc zboží, než máte fyzicky na skladě.';
                } else {
                    $data['product_id'] = $product_id;
                    $data['order_id'] = $order_id;
                    $data['user_id'] = $user->user_id;
                    $data['operation'] = 'Odpis zboží ze skladu';
                    $data['inv1a'] = $prevData['inv1a'];
                    $data['inv1b'] = $prevData['inv1b'];
                    $data['inv2a'] = $prevData['inv2a'];
                    $data['inv2b'] = $prevData['inv2b'];
                    $data['inv3l'] = 0 - $quantity;
                    $data['inv3a'] = $prevData['inv3a'] - $quantity;
                    $data['inv3b'] = $prevData['inv3b'] - $quantity;
                }
                break;
        }
        if ($this->sendmail) {
            $mail = new Zend_Mail('utf-8');
            $model = new Model_DbTable_EshopProducts();
            $product = $model->fetchProduct($product_id);
            if ($debug) {
                $mail->setFrom('info@balkanova.cz', 'TEST Inventář');
                $mail->setSubject("TEST " . $product['title'] . " (ID $product_id) potřebuje přepočítat v inventáři");
                $mail->addTo("jan_dockal@seznam.cz");
            } else {
                $mail->setFrom('info@balkanova.cz', 'Inventář');
                $mail->setSubject($product['title'] . " (ID $product_id) potřebuje přepočítat v inventáři");
                $mail->addTo("info@balkanova.cz");
            }
            $mail->setBodyHtml("Produkt " . $product['title'] . " (ID $product_id) podle inventáře na obchodě není. Prosím doplňte správné počty zboží na skladě a v obchodě.");
            try {
                $mail->send();
            } catch (Exception $e) {
                echo "Caught exception: " . get_class($e) . "\n";
                echo "Message: " . $e->getMessage() . "\n";
            }
        }
        if ($cando) {
            try {
                $this->insert($data);
                return 1;
            } catch (Zend_Exception $e) {
                echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
                echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
                //echo "\n <br/>SQL: " . $sql . "\n <br/>";
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function getMessage() {
        return $this->message;
    }

}
