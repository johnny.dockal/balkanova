<?php

class Admin_ProfileController extends Zend_Controller_Action {

    public function init() {
        $this->view->headTitle('Profil', 'POSTEND');
    }

    public function indexAction() {
        $saved = $this->getParam('saved');
        $problem = $this->getParam('problem');
        //připravíme výtah z tabulky uživatelů pro tabulku
        $modelUsers = new Model_DbTable_Users();
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $user = get_object_vars(Zend_Auth::getInstance()->getStorage()->read());
            $userArray = $modelUsers->find($user['user_id'])->toArray();
        }
        //připravíme formulář pro přidání nového uživatele
        $form = new Form_Profileform();
        $form->populate($userArray[0]);
        $this->view->form = $form;  
        
        $this->view->saved = $saved;        
        $this->view->problem = $problem;
    }

    public function saveAction() {
        $user_id = $this->getParam('user_id');
        $table = new Model_DbTable_Users();
        if (empty($user_id)) {
            $this->_redirect('/admin/profile/?saved=false');
        } else {
            $pswd = $this->getParam('user_pswd');
            $check = $this->getParam('pswd_check');
            $data = array(
                'user_email' => $this->getParam('user_email'),
                'user_name' => $this->getParam('user_name')
            );
            if (!empty($pswd) && !empty($check) && $pswd == $check) {
                $data['user_pswd_hashed'] = md5($pswd);
                $where = $table->getAdapter()->quoteInto('user_id = ?', $user_id);
                $table->update($data, $where);
                $this->_redirect('/admin/profile/?saved=true');
            } else if (!empty($pswd) && !empty($check)) {
                $this->_redirect('/admin/profile/?saved=false&problem=nomatch');
            } else {
                $where = $table->getAdapter()->quoteInto('user_id = ?', $user_id);
                $table->update($data, $where);
                $this->_redirect('/admin/profile/?saved=true');
            }
        }
    }

}
