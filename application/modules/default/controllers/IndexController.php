<?php

class Default_IndexController extends Zend_Controller_Action {

    private $lang = null;
    private $display = null;

    function init() {
        $session = new Zend_Session_Namespace('Default');
        $this->lang = $session->lang;

        $this->view->title = $this->view->str_ourproducts;
        $this->display = $this->_getParam('display');
        $this->view->headimg = "/graphics/nase-vyrobky_$this->lang.gif";
    }

    function indexAction() {
        $categoryModel = new Model_DbTable_EshopCategories();
        $this->view->data = $categoryModel->fetchCategories();
        $textModel = new Model_DbTable_Texts();
        $podminky = $textModel->fetchTextName('podminky');
        $this->view->podminky_text = $podminky;


        if ($this->display == 'impressum' and $this->lang == "de") {
            $this->view->display = 'impressum';
        } elseif ($this->display == 'terms') {
            $this->view->display = 'terms';
        } elseif ($this->display == 'shops') {
            $shops = $this->getShops();
            $this->view->shops = $shops;
            $this->view->display = 'shops';
        } else {
            $this->view->display = 'index';
        }
    }

    function getShops() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT * FROM shops ORDER BY shop_city, shop_name DESC";
        $stmt = $db->query($sql);
        $shops = $stmt->fetchAll();

        return $shops;
    }

}

?>