<?php
/*
 * Synchronizováno 11.01.2016
 */
class Model_DbTable_EshopOrderStatus extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_order_status';
    protected $_primary = 'status_id';

    public function fetchTitle($status_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $session = new Zend_Session_Namespace('Default');
        $lang = $session->lang;
        $sql = "SELECT "
                . "title_$lang AS title "                
                . "FROM $this->_name AS o "                
                . "WHERE $this->_primary = '$status_id'";        
        try {
            $result = $db->fetchRow($sql);
            return $result['title'];
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
    }    

    public function changeStatus($order_id, $status_id) {
        $user = Zend_Auth::getInstance()->getIdentity();
        $data = array('status_id' => $status_id, 'user_id' => $user->user_id);
        $where = $this->getAdapter()->quoteInto('order_id = ?', $order_id);
        try {
            if ($status >= 0) {
                $this->update($data, $where);
            }
            $model = new Model_DbTable_EshopOrderHistory();
            $data2 = array('order_id' => $order_id, 'status_id' => $status_id, 'user_id' => $user->user_id);
            $model->insert($data2);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
    }

    public function saveOrder($data) {
        $order_id = null;
        try {
            $this->insert($data);
            $db = Zend_Db_Table::getDefaultAdapter();
            $order_id = $db->lastInsertId($this->_name, $this->_primary);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $order_id;
    }

    public function getOrderByToken($token) {
        $query = "SELECT * FROM $this->_name WHERE token = '$token'";
        try {
            $db = Zend_Db_Table::getDefaultAdapter();
            $result = $db->fetchAll($query);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }

}
