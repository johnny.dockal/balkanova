<?php

// Define path to application directory
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

//z nějakého neznámého důvodu mi nejde na ONEBITU nastavit v .htaccess APPLICATION_ENV na cokoliv jiného než production
//takže takhle to ochcávám, aby se mi tam nahodilo "testing" v případě, že to běží na demu
$url = $_SERVER['HTTP_HOST'];
$url_array = explode('.', $url);
if (($url_array[0] == 'detest') or ( $url_array[0] == 'test')) {
    $enviroment = 'testing';
} else if ($url_array[1] == 'local') {
    $enviroment = 'development';
} else {
    $enviroment = 'production';
}

// Define application environment
defined('APP_ENV') || define('APP_ENV', (getenv('APP_ENV') ? getenv('APP_ENV') : $enviroment));

// Define application name
defined('APP_NAME') || define('APP_NAME', 'balkanova');

// Define application name
defined('APP_NAME_FULL') || define('APP_NAME_FULL', 'Balkanova');

if (isset($url_array[2]) && $url_array[2] == 'de') {

// Define application ID 1=balkanova.cz, 2=balkanova.de, 3=bulharskavina.cz
    defined('APP_ID') || define('APP_ID', 2);

// Define application ID 1=balkanova.cz, 2=balkanova.de, 3=bulharskavina.cz
    defined('APP_LOCALE') || define('APP_LOCALE', 'de');
} else {

// Define application ID 1=balkanova.cz, 2=balkanova.de, 3=bulharskavina.cz
    defined('APP_ID') || define('APP_ID', 1);

// Define application ID 1=balkanova.cz, 2=balkanova.de, 3=bulharskavina.cz
    defined('APP_LOCALE') || define('APP_LOCALE', 'cz');
}

// Define application contact address
defined('APP_EMAIL') || define('APP_EMAIL', 'info@balkanova.cz');

// Define product url (in case images are taken from external source)
defined('IMG_URL') || define('IMG_URL', 'http://www.balkanova.cz/');

// Define application contact address for testing purposes
defined('APP_TESTMAIL') || define('APP_TESTMAIL', 'jan_dockal@seznam.cz');

// Define product url (in case images are taken from external source)
defined('APP_URL') || define('APP_URL', 'http://www.balkanova.cz/');

// Define product url (in case images are taken from external source)
defined('APP_URL_TEST') || define('APP_URL_TEST', 'http://test.balkanova.de/');

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
        APP_ENV, APPLICATION_PATH . '/configs/application.ini'
);

//Setup Ini Configuration, save to registry
$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'staging');
Zend_Registry::set('config', $config);

$front = Zend_Controller_Front::getInstance();
//Setup Router - SEO URL
$router = $front->getRouter();
//REGEXP PRO ROUTER
//pokud url nezačíná jedním ze slov v závorce, pošleme celý řetezec v parametru url na CatalogueController
$route = new Zend_Controller_Router_Route_Regex(
        '^((?!cart|sitemap|mailinglistlite|sitemap\.xml|admin|redirect|feed)\S+)', array(
    'module' => 'default',
    'controller' => 'catalogue'
        ), array(1 => 'url'), 'catalogue/%s'
);
//set seo-url router
$router->addRoute('seo-url', $route);

//nastavení sitemap.xml pro vyhledávače
$sitemap = new Zend_Controller_Router_Route_Regex(
        '^sitemap.xml', array(
    'controller' => 'sitemap',
    'action' => 'xml'
        )
);
$router->addRoute('sitemap-xml', $sitemap);
$application->bootstrap()
        ->run();

function makeAlias($text) {
    $prevodni_tabulka = Array(
        'ä' => 'a', 'Ä' => 'A', 'á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ã' => 'a', 'Ã' => 'A', 'â' => 'a', 'Â' => 'A',
        'č' => 'c', 'Č' => 'C', 'ć' => 'c', 'Ć' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ě' => 'e',
        'Ě' => 'E', 'é' => 'e', 'É' => 'E', 'ë' => 'e', 'Ë' => 'E', 'è' => 'e', 'È' => 'E', 'ê' => 'e', 'Ê' => 'E',
        'í' => 'i', 'Í' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ľ' => 'l',
        'Ľ' => 'L', 'ĺ' => 'l', 'Ĺ' => 'L',
        'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N',
        'ó' => 'o', 'Ó' => 'O', 'ö' => 'o', 'Ö' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ò' => 'o', 'Ò' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ő' => 'o', 'Ő' => 'O',
        'ř' => 'r', 'Ř' => 'R', 'ŕ' => 'r', 'Ŕ' => 'R',
        'š' => 's', 'Š' => 'S', 'ś' => 's', 'Ś' => 'S',
        'ť' => 't', 'Ť' => 'T',
        'ú' => 'u', 'Ú' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ü' => 'u', 'Ü' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'û' => 'u', 'Û' => 'U',
        'ý' => 'y', 'Ý' => 'Y',
        'ž' => 'z', 'Ž' => 'Z', 'ź' => 'z', 'Ź' => 'Z'
    );
    $temp1 = strtr($text, $prevodni_tabulka);
    $temp2 = strtolower($temp1);
    return str_replace(" ", "-", $temp2);
}
