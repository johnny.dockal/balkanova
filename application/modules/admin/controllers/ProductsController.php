<?php

class Admin_ProductsController extends Zend_Controller_Action {

    private $category_id = 'all';
    private $page = 1;
    private $order = 'product_id';
    private $sort = 'desc';
    private $limit = 50;
    private $productArray = null;

    public function init() {
        $this->productArray = array(
            'status' => $this->getParam('status'),
            'code' => $this->getParam('code'),
            'sequence' => $this->getParam('sequence'),
            'title_cz' => $this->getParam('title_cz'),
            'full_title_cz' => $this->getParam('full_title_cz'),
            'alias_cz' => $this->getParam('alias_cz'),
            'text_cz' => $this->getParam('text_cz'),
            'note_cz' => $this->getParam('note_cz'),
            'material_cz' => $this->getParam('material_cz'),
            'manufactured_cz' => $this->getParam('manufactured_cz'),
            'title_en' => $this->getParam('title_en'),
            'full_title_en' => $this->getParam('full_title_en'),
            'alias_en' => $this->getParam('alias_en'),
            'text_en' => $this->getParam('text_en'),
            'note_en' => $this->getParam('note_en'),
            'material_en' => $this->getParam('material_en'),
            'manufactured_en' => $this->getParam('manufactured_en'),
            'material_de' => $this->getParam('material_de'),
            'title_de' => $this->getParam('title_de'),
            'full_title_de' => $this->getParam('full_title_de'),
            'alias_de' => $this->getParam('alias_de'),
            'text_de' => $this->getParam('text_de'),
            'note_de' => $this->getParam('note_de'),
            'material_de' => $this->getParam('material_de'),
            'manufactured_de' => $this->getParam('manufactured_de'),
            'price_unit_cz' => $this->getParam('price_unit_cz'),
            'price_unit_de' => $this->getParam('price_unit_de'),
            'price_cz' => $this->getParam('price_cz'),
            'price_de' => $this->getParam('price_de'),
            'size' => $this->getParam('size'),
            'weight' => $this->getParam('weight'),
            'weight_unit' => $this->getParam('weight_unit'),
            'grams' => $this->getParam('grams'),
            'hair' => $this->getParam('hair'),
            'certified' => $this->getParam('certified'),
            'new' => $this->getParam('new')
        );
    }

    public function preDispatch() {
        $category_id = $this->getParam('category_id');
        $page = $this->getParam('page');
        $order = $this->getParam('order');
        $sort = $this->getParam('sort');  

        $adminSession = new Zend_Session_Namespace('Admin');
        
        //nastavíme kategorii, kterou si zrovna prohlížíme
        if (!empty($category_id)) {
            $this->category_id = $category_id;
            $adminSession->category_id = $category_id;
            //pokus se mění kategorie, musí se jít zpátky na stránku 1, aby nedošlo k out of bounds exception
            $adminSession->page = $this->page;
        } else {
            $this->category_id = $adminSession->category_id;
        }
        if (empty($adminSession->category_id)) {
            $adminSession->category_id = $this->category_id;
        }
        //nastavíme stránku
        if (!empty($page)) {
            $this->page = $page;
            $adminSession->page = $page;
        } else if (empty($adminSession->page)) {
            $adminSession->page = $this->page;
        } else {
            $this->page = $adminSession->page;
        }
        //nastavíme pořadí
        if (!empty($order)) {
            $this->order = $order;
            $adminSession->order = $order;
        } else {
            $this->order = $adminSession->order;
        }
        if (empty($adminSession->order)) {
            $adminSession->order = $this->order;
        }
        if (!empty($sort)) {
            $this->sort = $sort;
            $adminSession->sort = $sort;
        } else {
            $this->sort = $adminSession->sort;
        }
        if (empty($adminSession->sort)) {
            $adminSession->sort = $this->sort;
        }
        
        $this->view->category_id = $adminSession->category_id;
        $this->view->page = $adminSession->page;
        $this->view->order = $adminSession->order;
        $this->view->sort = $adminSession->sort;
    }

    public function indexAction() {
        $search = $this->getParam('search');        
        $categories = new Model_DbTable_EshopCategories();
        $model = new Model_DbTable_EshopProducts();
        if (!empty($search)) {
            $products = $model->searchProductsAdmin($search);   
            $this->view->search = true;
        } else {
            $products = $model->fetchProductsAdmin($this->category_id, $this->order);
        }        
        //$products = $model->fetchProductsByCat();
        $this->view->categories = $categories->fetchAll()->toArray();
        $this->view->itemlimit = $this->limit;
        $this->view->itemcount = count($products);
        $this->view->pagecount = ceil($this->view->itemcount / $this->limit);
        $this->view->products = array_chunk($products, $this->limit);
    }

    public function editAction() {
        $subcatmodel = new Model_DbTable_EshopSubCategories();
        $productmodel = new Model_DbTable_EshopProducts();
        $relationmodel = new Model_DbTable_SubcatProducts();

        //podle toho do jaké kategorie chceme produkt přidat tak zvolíme subkategorie pro formulář
        $product_id = $this->getParam('product_id');
        if (empty($product_id)) {
            
        } else {
            $product = $productmodel->fetchProductEdit($product_id);
        }
        $subcatoptions = $subcatmodel->fetchSubcategories();

        //uložení produktu
        if ($this->getRequest()->getPost('save') or $this->getRequest()->getPost('save2')) {
            $product_id = $this->saveProduct();
            //$this->_redirect('/admin/products/view/?product_id=' . $product_id);
        } else {
            //nasolíme text do připraveného formuláře včetně seznamu kategorií
            if (empty($product)) {
                $form = new Form_ProductsformCZENDE('/admin/products/edit/', $subcatoptions);
                $data = $this->productArray;
                $form->populate($data);
                $this->view->title = "nový";
            } else {
                $selected = $relationmodel->fetchProductSubcats($product_id);
                $form = new Form_ProductsformCZENDE('/admin/products/edit/', $subcatoptions, $selected, $product_id);
                //checkboxy se subkategoriema se zvolej pomocí $selected, tohle by to přepsalo
                unset($product['subcategory_id']);
                $form->populate($product);
                $this->view->title = $product['title_cz'];
                $this->view->selected = $selected;
            }
            $this->view->subcatoptions = $subcatoptions;
            $this->view->form = $form;
        }
    }

    public function viewAction() {
        $product_id = $this->getParam('product_id');
        $model = new Model_DbTable_EshopProducts();
        $this->view->product = $model->fetchProduct($product_id);
    }

    public function deleteAction() {
        $product_id = $this->getParam('product_id');
        if (!empty($product_id)) {
            $table = new Model_DbTable_EshopProducts();
            $where = $table->getAdapter()->quoteInto('product_id = ?', $product_id);
            $table->delete($where);
        }
        $this->_redirect('/admin/products/');
    }

    public function saveProduct() {
        $productmodel = new Model_DbTable_EshopProducts();
        $product_id = $this->getParam('product_id');
        $selected = $this->getParam('subcategory_id');
        if (empty($selected)) {
            $selected = array();
        }
        //aby se nemusel vyplňovat název piva dvakrát
        $title_en = $this->getParam('title_en');
        if (empty($title_en)) {
            $title_en = $this->getParam('title_cz');
        }
        $data = $this->productArray;
        if (empty($product_id)) {
            //insertujeme produkt
            $product_id = $productmodel->saveProduct($data, $selected);
        } else {
            //updatujeme produkt
            $productmodel->saveProduct($data, $selected, $product_id);
        }

        $form = new Form_ProductsformCZENDE();
        $images = array("product_image1" => "", "product_image2" => "_big", "product_image3" => "_detail");
        foreach ($images as $key => $value) {
            if ($form->$key->isUploaded()) {
                echo "<br/>obrázek vybrán<br/>";
                $path = getcwd() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'eshop_products' . DIRECTORY_SEPARATOR;
                $target_name = $product_id . $value . ".jpg";
                $target_path = $path . DIRECTORY_SEPARATOR . $target_name;

                $validator = new Zend_Validate_File_Exists();
                $validator->addDirectory(getcwd());

                if ($validator->isValid($target_path)) {
                    unlink($target_path);
                }

                $form->$key->setDestination($path);
                $form->$key->addFilter('Rename', $target_name);
                /*
                  echo "teď se bude přenastavovat velikost...";
                  $filterChain = new Zend_Filter();
                  $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
                  'directory' => $path . 'l',
                  'width' => 300,
                  'height' => 600,
                  'cropToFit' => true,
                  'keepRatio' => true,
                  'keepSmaller' => false,
                  )));

                  $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
                  'directory' => $path . 'm',
                  'width' => 100,
                  'height' => 200,
                  'cropToFit' => true,
                  'keepRatio' => true,
                  'keepSmaller' => false,
                  )));
                  $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
                  'directory' => $path . 's',
                  'width' => 50,
                  'height' => 50,
                  'cropToFit' => true,
                  'keepRatio' => true,
                  'keepSmaller' => false,
                  )));
                  $form->product_image->addFilter($filterChain);
                  echo ini_get('upload_max_filesize');
                  echo "<br/>teď se bude ukládat...";
                 * */
                try {
                    echo "zkusíme ho uložit ($target_name)<br/>";
                    if ($form->$key->receive()) {
                        echo "povedlo se <br/>";
                    } else {
                        echo "NEpovedlo se<br/>";
                    }
                } catch (Zend_File_Transfer_Exception $e) {
                    throw new Exception('Unable to recieve : ' . $e->getMessage() . " <br/>PATH: " . $path);
                }
            } else {
                echo "Obrázek nebyl vybrán";
            }
        }

        return $product_id;
    }

}
