<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Countriesform extends Zend_Form {

    public function __construct($countries = null) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction('/admin/shipping/save/');
        $this->setAttrib('class', 'admintable');
        
        if (!isset($countries)) {
            $countries = array('0');
            $i = 0;
        } else {
            $i = 1;
        }
        foreach ($countries as $value) {
            $setting_id = new Zend_Form_Element_Hidden($i.'shipping_id');
            $public = new Zend_Form_Element_Radio($i.'public');
            $public->addMultiOptions(array(
                           '0'    => 'Zákázáno',
                           '1'    => 'Umožněno', 
                ));  
            $public->setLabel('Status:')->setRequired(true);
            $text_cz = new Zend_Dojo_Form_Element_Textarea($i.'text_cz', array('class' => 'textbox'));
            $text_cz->setLabel('Doprovodný text česky')->setRequired(true);
            $text_en = new Zend_Dojo_Form_Element_Textarea($i.'text_en', array('class' => 'textbox'));
            $text_en->setLabel('Doprovodný text anglicky')->setRequired(true);
            $text_de = new Zend_Dojo_Form_Element_Textarea($i.'text_de', array('class' => 'textbox'));
            $text_de->setLabel('Doprovodný text německy')->setRequired(true);
            $price_cz = new Zend_Form_Element_Text($i.'package_price_cz', array('class' => 'textbox'));
            $price_cz->setLabel('Cena za balné (nikoliv za dopravu) v korunách:')->setRequired(true); 
            $price_de = new Zend_Form_Element_Text($i.'package_price_de', array('class' => 'textbox'));
            $price_de->setLabel('Cena za balné (nikoliv za dopravu) v eurech:')->setRequired(true); 
            $price_unit_cz = new Zend_Form_Element_Text($i.'package_price_unit_cz', array('class' => 'textbox'));
            $price_unit_cz->setLabel('Cena v korunách:')->setRequired(true); 
            $price_unit_de = new Zend_Form_Element_Text($i.'package_price_unit_de', array('class' => 'textbox'));
            $price_unit_de->setLabel('Cena v eurech:')->setRequired(true); 
            $this->addElements(array($setting_id, $public, $text_cz, $text_en, $text_de, $price_cz, $price_de));
            $this->addDisplayGroup(array(        
                    $i.'public',
                    $i.'text_cz',
                    $i.'text_en',
                    $i.'text_de',
                    $i.'package_price_cz',                
                    $i.'package_price_de',
                    $i.'package_price_unit_cz',                
                    $i.'package_price_unit_de'
            ),$i.'group',array('legend' => $value['title_cz']));
            $group = $this->getDisplayGroup($i.'group');
            $group->setDecorators(array(        
                    'FormElements',
                    'Fieldset',
                    array('HtmlTag',array('tag'=>'div'))
            ));            
            $i++;
        }
        
        $count = new Zend_Form_Element_Hidden('count');
        $count->setValue($i);
        $count->removeDecorator('label');
        $this->addElement($count);

        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "button bg-green border-style active"));
        $this->addElement($submit);
        
    }

}
