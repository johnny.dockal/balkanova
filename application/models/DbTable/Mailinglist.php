<?php

class Model_DbTable_Mailinglist extends Zend_Db_Table_Abstract {

    protected $_name = 'mailinglist';
    protected $_primary = 'mail_id';
    
    public function deleteAdress($id) {
        $where = $this->getAdapter()->quoteInto('user_id = ?', $user_id);
        $this->delete($where);
    }
    
    public function fetchAllActive() {
        $query = $this->select()->where('status > 0');
        $result = $this->fetchAll($query);
        
        return $result;
    }
    
    public function emailSent($id) {
        $data = array(
            'last_sent' => date('Y-m-d')
        );
        $where = $this->getAdapter()->quoteInto('mail_id = ?', $id);
        $this->update($data, $where);
    }
}

