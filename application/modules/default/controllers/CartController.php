<?php

class Default_CartController extends Zend_Controller_Action {

    private $step = null;
    private $order = null;
    private $accepted = false;
    private $cartProducts = null;
    private $defaultSession = null;
    private $cartSession = null;
    private $msg_available = null;

    public function init() {
        $this->cartSession      = new Zend_Session_Namespace('Cart');
        $this->defaultSession   = new Zend_Session_Namespace('Default');
        $this->order            = new Model_EshopOrder();
        $this->msg_available    = array();
        $this->view->header     = 'small';
    }

    public function preDispatch() {
        //nastavení jednotlivých kroků
        $this->step = $this->_getParam('step', 0);
        if ($this->getRequest()->isPost()) {
            //pokud chytíme parametr zpět, chceme se vrátit o jeden krok
            $back = $this->_getParam('cartbuttonprev');
            //pokud chytíme parametr update, tak se editují počty zboží v košíku
            $count = $this->_getParam('update');
            //zjistíme, zda zákazník odklikl základní podmínky
            //$this->accepted = $this->_getParam('accepted');

            if ($count) {
                $inventory = new Model_DbTable_EshopProductHistory();
                $product_ids = array();
                $i = 1;
                while ($i <= $count) {
                    $product_id = $this->_getParam('product_id-' . $i);
                    $product_ids[$product_id] = $product_id;
                    $i++;
                }
                $available = $inventory->fetchAvailable($product_ids);
                $j = 1;
                while ($j <= $count) {
                    $product_id = $this->_getParam('product_id-' . $j);
                    //chci jenom čísla
                    $quantity = filter_var($this->_getParam('quantity-' . $j), FILTER_SANITIZE_NUMBER_INT);
                    if (empty($quantity)) {
                        $quantity = 0;
                    }
                    $size = $this->_getParam('size-' . $j);
                    if (isset($available[$product_id]) and ( $quantity > $available[$product_id]['available'])) {
                        $quantity = $available[$product_id]['available'];
                        if ($quantity < 0) {
                            $quantity = 0;
                        }
                        $this->msg_available[$product_id] = $quantity;
                    } else if ($quantity > 50) {
                        $quantity = 50;
                        $this->msg_available[$product_id] = $quantity;
                    }
                    $this->order->updateProduct($product_id, $quantity, $size);
                    $j++;
                }
            }

            if ($back) {
                $this->step = $this->step - 2;
                if ($this->step < 1) {
                    $this->_redirect('/cart/');
                }
            }

            switch ($this->step) {
                case 2:
                case 3:
                    $deleteZero = true;
                    break;
                case 1:
                default:
                    $deleteZero = false;
                    break;
            }
            $this->cartProducts = $this->order->fetchCartProducts($deleteZero);
        }
        $this->defaultSession->sum = $this->order->getCartSum();
    }

    public function postDispatch() {
        $view = Zend_Layout::getMvcInstance()->getView();
        if (!isset($this->cartProducts)) {
            $this->cartProducts = $this->order->fetchCartProducts();
        }
        switch ($this->step) {
            case 1:
                $this->breadcrumbs[0]['alias'] = 'cart';
                $this->breadcrumbs[0]['title'] = $view->str_basket;
                $this->breadcrumbs[1]['alias'] = 'cart/order/step/1/';
                $this->breadcrumbs[1]['title'] = $view->str_address;
                $this->view->title = $view->str_address;
                break;
            case 2:
                $this->breadcrumbs[0]['alias'] = 'cart';
                $this->breadcrumbs[0]['title'] = $view->str_basket;
                $this->breadcrumbs[1]['alias'] = 'cart/order/step/2/';
                $this->breadcrumbs[1]['title'] = $view->str_shipping;
                $this->view->title = $view->str_shipping;
                break;
            case 3:
                $this->breadcrumbs[0]['alias'] = 'cart';
                $this->breadcrumbs[0]['title'] = $view->str_basket;
                $this->breadcrumbs[1]['alias'] = 'cart/order/step/3/';
                $this->breadcrumbs[1]['title'] = $view->str_payment;
                $this->view->title = $view->str_payment;
                break;
            case 4:
                $this->breadcrumbs[0]['alias'] = 'cart';
                $this->breadcrumbs[0]['title'] = $view->str_basket;
                $this->breadcrumbs[1]['alias'] = 'cart/order/step/4/';
                $this->breadcrumbs[1]['title'] = $view->str_summary;
                $this->view->title = $view->str_summary;
                break;
            default:
                $this->breadcrumbs[0]['alias'] = 'cart';
                $this->breadcrumbs[0]['title'] = $view->str_basket;
                $this->view->title = $view->str_basket;
                break;
        }
        $this->view->breadcrumbs = $this->breadcrumbs;
        $this->view->cartProducts = $this->cartProducts;
        $this->view->order = $this->order;
        $this->view->step = $this->step;
        $this->view->msg_available = $this->msg_available;
    }

    public function indexAction() {
        $view = Zend_Layout::getMvcInstance()->getView();
        $this->view->title = $view->str_basket;
    }

    //Přidáme do košíku výrobek podle ID, přesměrujeme na původní stránku
    public function addAction() {
        $product_id = $this->_getParam('product_id');
        //$quantity   = $this->_getParam('quantity'); 
        $redir = $this->_getParam('redirect');
        //$line       = $this->_getParam('line');    
        $size = $this->_getParam('size');

        $this->order->addProduct($product_id, '1', $size);
        $this->_redirect("/$redir");
    }

    public function getcategoryAction() {
        $group_id = $this->_getParam('group_id');
        $subcatId1 = $this->_getParam('subcatId1');
        $subcatId2 = $this->_getParam('subcatId2');

        $modelGroups = new Model_DbTable_EshopGroups();
        $data = $modelGroups->fetchGroupByIdAjax($group_id, $subcatId1, $subcatId2);

        $this->_helper->json($data);
    }

    public function emptyAction() {
        $product_id = $this->_getParam('product_id');

        if ($product_id == 'all') {
            Zend_Session::namespaceUnset('Cart');
            $this->defaultSession->sum = 0;
        } else {
            $this->defaultSession->sum--;
            unset($this->cartSession->$product_id);
        }
        $this->_redirect("/cart/");
    }

    //Vyplnění objednávky
    public function orderAction() {
        //parametry odeslané z kroku 2, 3 a 4 při pohybu "zpět"       
        $this->order->setFormParameters($this->getRequest()->getPost());
        $this->step = $this->order->checkFormParameters($this->step);
        $paramArray = $this->order->formParametersToArray();

        echo "<p style='color: #fff'>orderWeight: " . $this->order->getOrderWeight() . "</p>";

        switch ($this->step) {
            //KROK 1 - Výběr dopravy a platby
            case '1':
                $model = new Model_DbTable_EshopCountries();
                $form = new Form_OrderStep1form($model->fetchCountries());
                $form->populate($paramArray);
                //musíme zobrazit podmínky
                $texts = new Model_DbTable_Texts();
                $this->view->terms = $texts->fetchTextName('podminky');
                $this->view->form = $form;
                break;
            //KROK 2 - Doručovací údaje
            case '2':
                $model = new Model_DbTable_EshopShipping();
                $form = new Form_OrderStep2form($model->fetchShipping($this->order->getCountryId(), $this->order->getOrderWeight()));
                $form->populate($paramArray);
                $this->view->noedit = true;
                $this->view->form = $form;
                break;
            //KROK 2 - Doručovací údaje - zásilkovna
            case '2z':
                $this->view->noedit = true;
                $this->view->country_id = $paramArray['country_id'];
                $this->view->display = 'zasilkovna';
                break;
            //KROK 3 - Platební údaje
            case '3':
                $model = new Model_DbTable_EshopPayments();
                $form = new Form_OrderStep3form($model->fetchPayments($this->order->getShippingId(), $this->order->getCountryId()), 1, $this->order->getOrderTotal());
                $form->populate($paramArray);
                $this->view->noedit = true;
                $this->view->form = $form;
                break;
            //KROK 4 - Kontrola
            case '4':
                $this->view->formParameters = $paramArray;
                $this->view->noedit = true;
                $this->_helper->viewRenderer('overview');
                break;
        }
    }

    public function sendAction() {
        $view = Zend_Layout::getMvcInstance()->getView();
        $this->order->setFormParameters($this->getRequest()->getPost());
        $this->step = $this->order->checkFormParameters(4);
        $formParameters = $this->order->formParametersToArray();
        $failmessage = "";
        if (APP_ENV == 'production') {
            $debug = false;
        } else {
            $debug = true;
        }

        if ($this->step == 4) {
            $save = $this->order->saveOrder();
        } else {
            $failmessage .= "<h2>Chyba!</h2><p>Zdá se, že vypršela úschova vašich osobních údajů. Údaje z kontaktního formuláře uchováváme jen omezenou dobu 12 minut. "
                    . "Pokud jste po tuto dobu na eshopu nečinní, tak se automaticky smažou.</p>";
        }
        $token = $this->order->getToken();
        if (isset($save) && $token) {
            //Připravím link na objednávku
            if ($debug) {
                $link = "$view->str_order_check <a href='" . APP_URL_TEST . "/cart/payment?token=$token'>[$view->str_order_link]</a><br/><br/>";
            } else {
                $link = "$view->str_order_check <a href='" . APP_URL . "/cart/payment?token=$token'>[$view->str_order_link]</a><br/><br/>";
            }
            //tabulka se jménem, adresou atd...
            $clientTable = "<h2>$view->form_billing</h2>"
                    . "<table width='560'>";
            if ($this->order->getCompany()) {
                $clientTable .= "<tr><td>$view->str_company</td><td>" . $this->order->getCompanyName() . "</td><tr>"
                        . "<tr><td>$view->str_ico</td><td>" . $this->order->getCompanyId() . "</td><tr>";
            }
            $clientTable .= "<tr><td>$view->form_name</td><td>" . $this->order->getFullName() . "</td><tr>"
                    . "<tr><td>$view->form_address</td><td>" . $this->order->getAddress() . "</td><tr>"
                    . "<tr><td>$view->form_city</td><td>" . $this->order->getCity() . " " . $this->order->getZip() . "</td><tr>"
                    . "<tr><td>$view->form_country</td><td>" . $this->order->getCountryName() . "</td><tr>";
            if ($this->order->getDiff()) {
                $clientTable .= "<tr><td colspan='2'><strong>$view->str_delivery_data</strong></td></tr>"
                        . "<tr><td>$view->form_name</td><td>" . $this->order->getDiffFullName() . "</td><tr>"
                        . "<tr><td>$view->form_address</td><td>" . $this->order->getDiffAddress() . "</td><tr>"
                        . "<tr><td>$view->form_city</td><td>" . $this->order->getDiffCity() . " " . $this->order->getDiffZip() . "</td><tr>";
            }
            $clientTable .= "<tr><td>$view->form_phone</td><td>" . $this->order->getPhone() . "</td><tr>"
                    . "<tr><td>$view->form_email</td><td>" . $this->order->getEmail() . "</td><tr>"
                    . "<tr><td>$view->form_delivery</td><td>" . $this->order->getShippingTitle() . "</td><tr>"
                    . "<tr><td>$view->form_payment</td><td>" . $this->order->getPaymentTitle() . "</td><tr>"
                    . "<tr><td>$view->str_message</td><td>" . $this->order->getMessage() . "</td><tr>"
                    . "</table><br/>";
            /*
             *  tabulka s objednanými věcmi je umístěna v modelu, aby k ní byl jednoduchý přístup z obou mnodulů
             */
            $productTable = $this->order->getProductTable();
            /*
             * pokud se má objednávka zaplatit převodem, tak potřebujeme tabulku s platebními údaji
             */
            $payment_id = $this->order->getPaymentId();
            if (in_array($payment_id, array('7', '9', '10'))) {
                $settings = new Model_DbTable_Settings();
                switch ($payment_id) {
                    //7 = převod na český účet
                    case 7:
                        $account = $settings->getAccountCZ();
                        break;
                    //9 = převod na slovenský účet
                    case 9:
                        $account = $settings->getAccountSK();
                        break;
                    //10 = převod na SIPO účet
                    case 10:
                        $account = $settings->getAccountEU();
                        break;
                    //4 = dobírka
                    default:
                        break;
                }
                $paymentTable = "<h2>$view->str_order_payment</h2>"
                        . "<table width='560'>"
                        . "<tr><td>$view->str_account_sum</td><td>" . $view->currency->toCurrency($this->order->getOrderTotal()) . "</td><tr>";
                if ($payment_id != '7') {
                    $paymentTable .= "<tr><td>&nbsp;</td><td>" . $view->currency->exchangeToEuro($this->order->getOrderTotal()) . "</td><tr>";
                    $paymentTable .= "<tr><td>$view->str_account_rate</td><td>" . $view->currency->getExchangeRate() . "</td><tr>";
                }
                $paymentTable .= "<tr><td>$view->str_account_no</td><td>$account->setting_value1</td><tr>";
                if ($payment_id == '10') {
                    $paymentTable .= "<tr><td>$view->str_account_swift</td><td>$account->setting_value2</td><tr>";
                } else {
                    $paymentTable .= "<tr><td>$view->str_account_bankId</td><td>$account->setting_value2</td><tr>";
                }
                $paymentTable .= "<tr><td>$view->str_account_var</td><td>" . $this->order->getInvoiceNumber() . "</td><tr>";
                $paymentTable .= "</table>";
            } else {
                $paymentTable = "";
            }
            /*
             * Seženeme si text mailu pro zákazníka
             */
            $model = new Model_DbTable_Texts();
            $text = $model->getEmailText($this->defaultSession->lang, $this->order->getShippingId(), $this->order->getPaymentId());
            /*
             * Začneme stavět mail pro zákazníka
             */
            $mail = new Zend_Mail('utf-8');
            if ($debug) {
                $mail->setFrom(APP_EMAIL, 'TEST ' . APP_NAME_FULL);
                $mail->setSubject("TEST $view->str_order_subject " . date('d. n. Y'));
            } else {
                $mail->setFrom(APP_EMAIL, 'E-shop ' . APP_NAME_FULL);
                $mail->setSubject("$view->str_order_subject " . date('d. n. Y'));
            }
            $mail->addTo($formParameters['order_email']);
            $mail->setBodyHtml($text['text'] . " <br/>$link $clientTable $paymentTable $productTable");
            try {
                if ($mail->send()) {
                    $historyModel = new Model_DbTable_EshopOrderHistory();
                    $historyModel->saveHistory($this->order->getOrderId(), '-1');
                }
            } catch (Exception $e) {
                echo "Caught exception: " . get_class($e) . "\n";
                echo "Message: " . $e->getMessage() . "\n";
            }
            $mail->clearRecipients();
            $mail->clearSubject();
            $mail->clearFrom();
            //Začnem stavět mail pro provozovatele
            if ($debug) {
                $mail->setFrom($formParameters['order_email'], 'TEST ' . APP_NAME_FULL . ' E-shop - ' . $formParameters['order_name'] . " " . $formParameters['order_surname']);
                $mail->addTo('jan_dockal@seznam.cz');
                $mail->setSubject("TESTOVACÍ objednávka od " . $formParameters['order_name'] . " " . $formParameters['order_surname']);
            } else {
                $mail->setFrom($formParameters['order_email'], APP_NAME_FULL . ' E-shop - ' . $formParameters['order_name'] . " " . $formParameters['order_surname']);
                $mail->addTo('info@balkanova.cz');
                $mail->setSubject("Nová objednávka od " . $formParameters['order_name'] . " " . $formParameters['order_surname']);
            }
            $mail->setBodyHtml("$clientTable $link $productTable <h2>Text odeslán zákazníkovi</h2> " . $text['text']);
            try {
                $mail->send();
            } catch (Exception $e) {
                echo "Caught exception: " . get_class($e) . "\n";
                echo "Message: " . $e->getMessage() . "\n";
            }
            $this->view->token = $token;
            $this->view->total = $this->order->getOrderTotal();
            $this->destroySession();
            //redirect je zrušen kvůli sklik, přesměrování probíhá na view
            //$this->_redirect("/cart/payment/?token=$token");
        } else {
            $failmessage .= "<p>Objednávku nelze uložit</p><p>token: $token</p>";
        }
        $this->view->failmessage = $failmessage;
    }

    public function paymentAction() {
        $token = $this->getParam('token');
        if (!empty($token)) {
            $this->order->fetchOrderByToken($token);
            switch ($this->order->getPaymentId()) {
                //7 = převod na český účet
                case 7:
                    $settings = new Model_DbTable_Settings();
                    $this->view->account = $settings->getAccountCZ();
                    $this->view->invoice_number = $this->order->getInvoiceNumber();
                    break;
                //9 = převod na slovenský účet
                case 9:
                    $settings = new Model_DbTable_Settings();
                    $this->view->account = $settings->getAccountSK();
                    $this->view->invoice_number = $this->order->getInvoiceNumber();
                    break;
                //10 = převod na SIPO účet
                case 10:
                    $settings = new Model_DbTable_Settings();
                    $this->view->account = $settings->getAccountEU();
                    $this->view->invoice_number = $this->order->getInvoiceNumber();
                    break;
                //4 = dobírka
                default:
                    break;
            }
            $this->cartProducts = $this->order->getOrderProducts();

            $texts = new Model_DbTable_Texts();

            $this->view->notice = $texts->fetchTextName('potvrzeni');
            $this->view->noedit = true;
            $this->view->noimage = true;
        } else {
            
        }
    }

    public function obchodnipodminkyAction() {
        $texts = new Model_DbTable_EshopTexts();
        $terms = $texts->find('podminky')->toArray();
        $this->view->terms = $terms[0];
    }

    private function destroySession() {
        Zend_Session::namespaceUnset('Order');
        Zend_Session::namespaceUnset('Cart');
        $defaultSession = new Zend_Session_Namespace('Default');
        $defaultSession->sum = 0;
    }

}
