<?php

class Model_DbTable_Users extends Zend_Db_Table_Abstract {

    protected $_name = 'users';
    protected $_primary = 'user_id';
    
    /* vrátí všechny texty v tabulce texts v daném jazyce */
    public function fetchUsers() {
        $users = $this->select()->from($this->_name, array('user_id', 'user_enabled', 'user_login', 'user_name', 'user_email', 'user_role', 'user_registered'));
        $result = $this->fetchAll($users);
        return $result;
    }
    
    public function disableUser($user_id) {
        $where = $this->getAdapter()->quoteInto('user_id = ?', $user_id);
        $data = array('user_enabled' => '0');
        $this->update($data, $where);
    }
    
    public function enableUser($user_id) {
        $where = $this->getAdapter()->quoteInto('user_id = ?', $user_id);
        $data = array('user_enabled' => '1');
        $this->update($data, $where);
    }
}

