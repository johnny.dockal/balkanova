<?php

/**
 * Plugin zjištuje požadavky na změnu jazyka webu v parametru.
 * Pokud odpovídá českému nebo anglickému jazyku, nastaví ji do Zend session.
 * @author Daniel Vála
 */
class Plugin_LangSelector extends Zend_Controller_Action_Helper_Abstract {

    private $langArray = null;
    private $session = null;

    public function init() {
        $this->session = new Zend_Session_Namespace('Default');
        $this->langArray = array('cz', 'de', 'en');
        // pokud ještě není nastaven žádný jazyk, nastaví se defaultně prvně uvedený jazyk v configu
        $module = $this->getRequest()->getModuleName();
        if ($module == 'admin') {
            $this->session->lang = 'cz';
        } else if (isset($this->session->lang)) {
            //nic
        } else if (APP_ID == 2) {
            $this->session->lang = 'de';
        } else {
            $this->session->lang = 'en';
        }
        // pokud je zjištěn požadavek na změnu jazyka, kontroluje se, zda je jazyk v požadavku jeden z uvedených v konfigu a nastaví se
        // pokud nevyhovuje nebo není vůbec nastaven, zachová se stávající jazyk
        $lang = $this->getRequest()->getParam('lang');
        if (isset($lang)) {
            if (in_array($lang, $this->langArray)) {
                $this->session->lang = $lang;
            } else {
                array_push($errormessage, "Chyba v požadavku na změnu jazyka, jazyk nenalezen v config.ini");
            }
        }  
        //nahodíme view, abychom do něj mohli posílat případné errormessage a stringtabley
        $view = Zend_Layout::getMvcInstance()->getView();
        if (isset($errormessage)) {
            $view->errormessage = $errormessage;
        }
        
        $view->lang = $this->session->lang;
        $this->setStrings();
    }
    
    public function setStrings($newlang = null) {
        if (isset($newlang)) {
            $lang = $newlang;
        } else {
            $lang = $this->session->lang;
        }
        $view = Zend_Layout::getMvcInstance()->getView();
        //stringtable
        if ($lang == 'cz') {
            //hlavní stránka
            $view->str_ourproducts = "Naše výrobky";
            $view->str_about = "O nás";
            $view->str_aboutourproducts = "Vlněné deky a koberce";
            $view->str_basket = "Košík";
            $view->str_balkanova = "Balkanova - originální vlněné bulharské deky";
            $view->str_news = "Aktuálně";
            $view->str_contact = "Kontaktní údaje";
            $view->str_contact_de = "Kontaktní údaje pro Německo: ";
            $view->str_contact_cz = "Kontaktní údaje: ";
            $view->str_shop = "Kamenný obchod";
            $view->str_resellers = "Obchody nabízející naše výrobky"; // chybí překlad
            $view->str_facebook_claim = "Můžete nás sledovat také na Facebooku";
            $view->str_sitemap = "Mapa stránek";
            $view->str_terms = "Obchodní podmínky";
            $view->str_noproducts = "V této kategorii nejsou momentálně žádné produkty.";
            $view->str_carpets_ready = "Nyní vám nabízíme tyto hotové kilimy:";
            //produkty
            $view->str_ask = "Dotaz na výrobek";
            $view->str_custom = "Objednat vlastní rozměr";
            $view->str_available = "ks na skladě:";
            $view->str_pile = "délka vlasu: ";
            $view->str_pile1 = "cm";
            $view->str_grams = "gramáž: ";
            $view->str_grams1 = " g/m&#178;";
            $view->str_size = "velikost: ";
            $view->str_material = "materiál:";
            $view->str_production = "výroba:";
            $view->str_size = "velikost:";
            $view->str_weight = "hmotnost:";
            $view->str_stock = "počet kusů na skladě:";
            $view->str_pcs = "ks";
            $view->str_add = "Do košíku";
            $view->str_empty = "Pro tuto kategorii momentálně není dostupné zboží.";
            $view->str_question = "Dotaz na výrobek";
            $view->str_care = "Údržba";
            $view->str_coarse1 = "Mountain Man (course) – one feather - Rhodopa Blankets";
            $view->str_coarse2 = "Rustic (slightly course) – two feathers - Perelika Blankets";
            $view->str_coarse3 = "Gentle (soft) – three feathers - Karandilla Blankets";
            $view->str_coarse4 = "Downy (ultra soft) – four feathers - Merino Blankets";
            $view->str_bysoftness = "Dle jemnosti";
            $view->str_softness_text = "Jemnost";
            $view->str_softness_desc = "Většina dek v této kategorii má níže zvýrazněnou jemnost";
            //cena
            $view->str_price0 = "cena: ";
            $view->str_price1 = "cena bez DPH: ";
            $view->str_price2 = "cena včetně DPH: ";
            $view->str_price3 = "cena (za jednotku): ";
            $view->str_price4 = "cena (za bm): ";
            $view->str_price5 = "0,5 bm: ";
            $view->str_price6 = "cena za m&sup2;: ";
            //objednávka  
            $view->str_order = "Objednávka";
            $view->str_order_id = "Identifikační číslo:";
            $view->str_order_status = "Stav objednávky:";
            $view->str_order_timestamp = "Datum přijetí:";
            $view->str_order_shipping_address = "Doručovací adresa:";
            $view->str_order_step = "krok";
            $view->str_order_continue = "Pokračovat";
            $view->str_order_back = "Zpět";
            $view->str_order_empty_basket = "Vyprázdnit košík";
            $view->str_order_fill_order = "Vyplnit objednávku";
            $view->str_order_send = "Odeslat objednávku";
            $view->str_order_select = "Vyberte místo odběru";
            $view->str_order_select_shipping = "Vyberte způsob dodání:";
            $view->str_order_select_payment = "Vyberte způsob platby:";
            $view->str_order_message = "Zpráva pro příjemce:";
            $view->str_order_product = "Název zboží";
            $view->str_order_size = "Velikost ";
            $view->str_order_quantity = "Množství";
            $view->str_order_unit_price = "Cena za ks";
            $view->str_order_unit_price_noVAT = "Cena (ks) <br/>(bez DPH)";
            $view->str_order_total_price = "Cena celkem ";
            $view->str_order_total_price_noVAT = "Cena celkem <br/>(bez DPH)";           
            $view->str_order_percent_of = "z částky";
            $view->str_order_total = "Celkem ";            
            $view->str_order_details = "Podrobnosti ";
            $view->str_order_payment = "Platba objednávky ";
            $view->str_order_VATincluded = "Všechny ceny jsou uvedené včetně DPH.";
            $view->str_order_check = "Stav objednávky můžete zkontrolovat zde:";
            $view->str_order_link = "Zobrazit objednávku";
            $view->str_order_ready = "Objednané zboží je připraveno k vyzvednutí";
            $view->str_order_sent = "Vaše objednávka byla předána přepravci";
            $view->str_order_subject = "Vaše objednávka byla uložena"; 
            $view->str_order_packing = "balíček"; 
            $view->str_order_paypal = "Zaplatit přes PayPal"; 
            $view->str_order_session = "Osobní informace byly vymazány z mezipaměti (session).";
            $view->str_order_free = 'ZDARMA';
            
            $view->str_picture = "obrázek";
            $view->str_product = "výrobek";
            $view->str_items = "počet";   
            $view->str_price = "cena";    
            $view->str_sum = "celkem";         
            $view->str_code = "kód produktu: ";
            $view->str_note = "poznámka: ";
            $view->str_size = "velikost: ";
            $view->str_size2 = "šířka: ";
            $view->str_weight = "hmotnost: ";
            $view->str_weight2 = "min. objednávka: ";
            $view->str_shipping1    = "Doprava: ";
            $view->str_shipping2    = "Způsob přepravy: ";            
            $view->str_payment1     = "Platba: ";            
            $view->str_payment2     = "Způsob platby: "; 
            $view->str_total = "Celkem: ";
            $view->str_empty = "Vysypat košík";
            $view->str_fill = "Vyplit údaje &#187;";
            $view->str_incomplete = "Formulář je nekompletní!";
            $view->str_required = "Než budeme moct objednávku přijmout, tak je třeba se přesvědčit zda jsou všechny údaje v pořádku.";
            $view->str_sofar = "Zatím máte košík prázdný!";
            $view->str_address = "Adresa";
            $view->str_shipping = "Doprava";
            $view->str_payment = "Platba";
            
            //formuláře
            $view->form_billing             = "Fakturační údaje";
            $view->form_name                = "Vaše jméno: ";
            $view->form_surname             = "Příjmení: ";
            $view->form_phone               = "Telefon: ";
            $view->form_email               = "E-mail: ";
            $view->form_address             = "Ulice a č. p.: ";
            $view->form_city                = "Město: ";
            $view->form_zip                 = "PSČ: ";
            $view->form_country             = "Země: ";
            $view->form_delivery            = 'Způsob doručení:';
            $view->form_payment             = 'Způsob platby:';            
            $view->form_available1          = 'Bohužel máme jen';
            $view->form_available2          = 'výrobků k dispozici.';
            $view->form_shipping_address    = "Doručovací adresa:";
            $view->form_fill_shipping       = "Vyplňte prosím jinou dodací adresu.";
            $view->form_order_company       = "Faktura bude vystavena na firmu";
            $view->form_order_company_name  = "Jméno firmy:";
            $view->form_order_company_id    = "IČO:";
            $view->form_order_diff          = "Dodací údaje se liší od fakturačních údajů";
            $view->form_back                = "Zpět";
            $view->form_select_shipping     = "Vybrat dopravu";
            $view->form_select_payment      = "Vybrat platbu";
            $view->form_select_summary      = "Shrnutí";
            $view->form_order_sendmail      = "Přeji si zasílat novinky na email.";
            $view->form_order_adult         = "Prohlašuji, že mi již bylo 18 let.";
            $view->form_summary             = "Závěrečná kontrola údajů"; 
            $view->form_send                = "Odeslat objednávku"; 
            
            $view->str_billing_info = "Fakturační údaje";
            $view->str_delivery_data = "Dodací údaje";            
            $view->str_czpost = "Českou poštou";
            $view->str_depost = "Deutsche Post";
            $view->str_payment = "Způsob platby:";
            $view->str_method1 = "Hotově při předání. (dobírka)";
            $view->str_method2 = "Kreditní kartou či paypal účtem. (předem)";
            $view->str_message = "Zpráva pro nás (volitelně):";
            $view->str_agree = "Souhlasím s <a onclick='' href='#podminky'>prodejními podmínkami</a>";
            $view->str_agree2 = "Souhlasím s obchodními podmínkami (viz. níže).";
            $view->str_mailing = "Přeji si zasílat informace o novinkách:";
            $view->str_confirm = "Potvrdit objednávku &#187;";
            $view->str_back = "&#171; Zpět na formulář";
            $view->str_ordered = "Objednali jste následující zboží:";
            $view->str_pay = "Zaplatit &#187;";
            $view->str_print = "Vytisknout";
            $view->str_check = "Nyní prosím klikněte na tlačítko zaplatit pro zaplacení Vaší objednávky.";
            $view->str_thanks = "Děkujeme!";
            $view->str_thanks2 = "Děkujeme za váš nákup!";
            $view->str_conditions = "Prodejní podmínky";
            $view->str_received = "Objednávka přijata";
            $view->str_sent = "Objednávka expedována";
            //faktura
            $view->str_invoice = "Faktura č. ";
            $view->str_supplier = "Dodavatel";
            $view->str_buyer = "Odběratel";
            $view->str_company = "Firma:";
            $view->str_ico = "IČ:";
            $view->str_dic = "DIČ";
            $view->str_listed = "zapsaná v obchodním rejstříku vedeném Městským soudem v Praze, oddíl C, vložka 148378";
            $view->str_date1 = "Datum vystavení: ";
            $view->str_date2 = "Datum zdanit. plnění: ";
            $view->str_date3 = "Splatnost: ";
            $view->str_paytype = "Způsob platby: ";
            $view->str_transfer1 = "před dodáním bezhotovostně";
            $view->str_transfer2 = "převodem";
            $view->str_transport = "Doprava: ";
            $view->str_paypal = "Paypal účet: ";
            $view->str_orderID = "Číslo objednávky.:";
            $view->str_shipping = "Doprava ";
            $view->str_vat1 = "DPH";
            $view->str_vat2 = "DPH %";
            $view->str_store = "Adresa prodejny: ";
            $view->str_final = "Celkem uhrazeno: ";
            $view->str_final_cashondelivery = "Hrazeno dobírkou: ";
            $view->str_account_sum = "Částka: ";
            $view->str_account_no = "Číslo účtu: ";
            $view->str_account_iban = "IBAN: ";
            $view->str_account_bankId = "Kód banky: ";
            $view->str_account_var = "Variabilní symbol: ";
            $view->str_account_swift = "SWIFT: ";
            $view->str_account_rate = "Kurz: ";
            //chyby v objednávacím formuláři
            $view->err_fill_order = "Prosím vyplňte způsob platby za objednávku.";
            $view->err_fill_address1 = "Musíte vybrat doručovací adresu.";
            $view->err_fill_shipping = "Prosím vyplňte způsob doručení zboží.";
            $view->err_fill_name = "Prosím vyplňte své jméno.";
            $view->err_fill_diff_name = "Prosím vyplňte své jméno.";
            $view->err_fill_surname = "Prosím vyplňte své přijmení.";
            $view->err_fill_diff_surname = "Prosím vyplňte své přijmení.";
            $view->err_fill_phone = "Prosím vyplňte své telefonní číslo.";
            $view->err_fill_email = "Prosím vyplňte validní email adresu.";
            $view->err_fill_address2 = "Prosím vyplňte svou adresu.";
            $view->err_fill_diff_address = "Prosím vyplňte svou adresu.";
            $view->err_fill_city = "Prosím vyplňte město.";
            $view->err_fill_diff_city = "Prosím vyplňte město.";
            $view->err_fill_zip = "Prosím vyplňte PSČ (pouze číselné hodnoty).";
            $view->err_fill_diff_zip = "Prosím vyplňte PSČ (pouze číselné hodnoty).";
            $view->err_fill_company_name = "Prosím vyplňte jméno firmy.";
            $view->err_fill_company_id = "Prosím vyplňte IČO firmy.";
            $view->err_fill_country = "Prosím vyplňte Zemi doručení.";
            $view->err_fill_agree = "Musíte souhlasit s obchodními podmínkami.";

            $view->week = $array = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            //$view->month = $array = array("", "January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $view->month = $array = array("", "JANUARY", "FEBUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER");
        } else if ($lang == 'de') {
            //hlavní stránka
            $view->str_ourproducts = "Unsere Produkte";
            $view->str_about = "Unser Unternehmen";
            $view->str_aboutourproducts = "Schafwolldecken und Wollteppiche";
            $view->str_basket = "Warenkorb";
            $view->str_balkanova = "Balkanova - Original bulgarische Wolldecken und Wollteppiche";
            $view->str_news = "Aktuelles";
            $view->str_contact = "Kontaktdaten";
            $view->str_contact_de = "Kontaktdaten für Deutschland: ";
            $view->str_contact_cz = "Kontaktdaten: ";
            $view->str_shop = "Geschäft";
            $view->str_resellers = ""; // chybí překlad
            $view->str_facebook_claim = "Sie können uns auch auf Facebook folgen";
            $view->str_sitemap = "Seitemap";
            $view->str_terms = "AGB";
            $view->str_noproducts = "In dieser Kategorie sind keine Artikel.";
            $view->str_carpets_ready = "Jetzt bieten wir diese bereit Kelims";
            //produkty
            $view->str_ask = "Frage zum Produkt";
            $view->str_custom = "Order custom size";
            $view->str_available = "Stück verfügbar:";
            $view->str_pile = "Florhöhe: ";
            $view->str_pile1 = "cm";
            $view->str_grams = "Gewicht / m&#178;: ";
            $view->str_grams1 = " g/m&#178;";
            $view->str_size = "Größe: ";
            $view->str_material = "Material: ";
            $view->str_production = "Herstellung: ";
            $view->str_size = "Größe: ";
            $view->str_weight = "Gewicht: ";
            $view->str_stock = "Auf Lager: ";
            $view->str_pcs = "St.";
            $view->str_add = "In den Warenkorb";
            $view->str_empty = "Diese Kategorie ist leer.";
            $view->str_question = "DE - Dotaz na výrobek";
            $view->str_care = "Pflege";
            $view->str_coarse1 = "Eine Feder - männlich-herb (rau) - unsere&nbsp;Rhodopa-Decken";
            $view->str_coarse2 = "Zwei Federn- rustikal (etwas rau) - unsere&nbsp;Perelika-Decken";
            $view->str_coarse3 = "Drei Federn- flauschig (weich) - unsere&nbsp;Karandilla-Decken";
            $view->str_coarse4 = "Vier Federn- daunenweich (sehr weich) - unsere&nbsp;Merino-Decken";
            $view->str_bysoftness = "Dle jemnosti";
            $view->str_softness_text = "Weichheitsgrad (unsere Federskala)";
            $view->str_softness_desc = "";
            //cena
            $view->str_price0 = "Preis: ";
            $view->str_price1 = "Preis zzgl. MwSt. ";
            $view->str_price2 = "Preis inkl. MwSt. ";
            $view->str_price3 = "Preis (pro Einheit): ";
            $view->str_price4 = "Cena (za bm):  ";
            $view->str_price5 = "0,5 bm: ";
            $view->str_price6 = "Preis für m&sup2;: ";
            //objednávka
            $view->str_order = "Bestellung";
            $view->str_order_id = "Bestellung ID:";
            $view->str_order_status = "Bestellung status:";
            $view->str_order_timestamp = "Datum des Eingangs:";
            $view->str_order_shipping_address = "Postanschrift:";
            $view->str_order_step = "Schritt";
            $view->str_order_continue = "Weitermachen";
            $view->str_order_back = "Zurück";
            $view->str_order_empty_basket = "Warenkorb ausleeren";
            $view->str_order_fill_order = "Formular anzeigen &#187;";
            $view->str_order_send = "Bestellung abschicken";
            $view->str_order_select = "Wählen Sie eine Websitesammlung";
            $view->str_order_select_shipping = "Zustellungsart wählen:";
            $view->str_order_select_payment = "Zahlungsart wählen:";
            $view->str_order_message = "Nachricht für den Empfänger:";
            $view->str_order_product = "Produktbezeichnung";
            $view->str_order_size = "Größe";  
            $view->str_order_quantity = "Menge";
            $view->str_order_unit_price = "Preis pro Stück";
            $view->str_order_total_price = "Gesamtpreis";
            $view->str_order_total = "Gesamt ";        
            $view->str_order_percent_of = "der Menge";
            $view->str_order_details = "Einzelheiten ";
            $view->str_order_payment = "Zahlungsaufträge ";
            $view->str_order_VATincluded = "Alle Preisangaben inkl. MwSt.";
            $view->str_order_check = "Sie können den Status Ihrer Bestellung zu überprüfen hier:";
            $view->str_order_link = "Anzeigereihenfolge";
            $view->str_order_ready = "Die bestellte Ware zur Abholung bereit stehen";
            $view->str_order_sent = "Ihre Bestellung wurde dem Frachtführer übergeben";
            $view->str_order_subject = "Wir haben Ihren Auftrag empfangen"; 
            $view->str_order_packing = "Paket"; 
            $view->str_order_paypal = "Bezahlen durch PayPal"; 
            $view->str_order_session = "Personenbezogene Daten werden aus dem Cache (Session) gelöscht.";
            
            $view->str_picture = "Bild";
            $view->str_product = "Produkt";
            $view->str_items = "Anzahl";   
            $view->str_price = "Preis";    
            $view->str_sum = "Gesamtpreis";
            $view->str_code = "Produktcode: ";
            $view->str_note = "Anmerkungen: ";
            $view->str_size = "Größe: ";
            $view->str_shipping1    = "Versandkosten: ";
            $view->str_shipping2    = "Versandart:";            
            $view->str_payment1     = "Gebührenzahlung: ";            
            $view->str_payment2     = "Zahlungsmethode:"; 
            $view->str_total = "Gesamtbetrag: ";
            $view->str_empty_bin = "Warenkorb ausleeren";
            $view->str_to_order = "Formular anzeigen &#187;";
            //objednávání           
            $view->str_size2 = "Width: ";
            $view->str_weight = "Gewicht:";
            $view->str_weight2 = "min. objednávka: ";
            $view->str_incomplete = "Es fehlen einige Pflichtdaten im Formular!";
            $view->str_required = "Alle Pflichtfelder müssen ausgefällt werden, bevor wir die Bestellung annehmen kšnnen. Bitte źberprźfen Sie Ihre Eingaben.";
            $view->str_sofar = "Es befinden sich derzeit keine Artikel im Warenkorb!";
            $view->str_address = "Adresse";
            $view->str_shipping = "Versand";
            $view->str_payment = "Zahlung";
            $view->str_summary = "Zusammenfassung";
            //formulář
            $view->form_billing             = "Abrechnungsdaten";
            $view->form_name                = 'Vorname:';
            $view->form_surname             = 'Nachname:';
            $view->form_phone               = 'Telefon:';
            $view->form_email               = 'E-Mail-Adresse:';
            $view->form_address             = 'Adresse:';
            $view->form_city                = 'Stadt:';
            $view->form_zip                 = 'PLZ:';
            $view->form_country             = 'Land:';
            $view->form_delivery            = 'Verschiffung:';
            $view->form_payment             = 'Zahlungsoptionen:';   
            $view->form_available1          = 'Leider haben wir nur';
            $view->form_available2          = 'x Produkte.';
            $view->form_shipping_address    = "Postanschrift:";   
            $view->form_fill_shipping       = "Bitte geben Sie die Lieferadresse.";
            $view->form_order_company       = "Die Rechnung wird an die Firma ausgestelltEine Rechnung wird an die Firma ausgestellt";
            $view->form_order_company_name  = "Firmenname:";
            $view->form_order_company_id    = "Id. Nr.:";
            $view->form_order_diff          = "Lieferdaten unterscheidet sich von der Rechnungsdaten";
            $view->form_back                = "Zurück";
            $view->form_select_shipping     = "Service auswählen";
            $view->form_select_payment      = "Zahlungsmethode";
            $view->form_select_summary      = "Shrnutí";
            $view->form_order_sendmail      = "Ich möchte über Neuigkeiten informiert werden per E-Mail.";
            $view->form_order_adult         = "Ich erkläre, dass ich 18 Jahre alt bin."; 
            $view->form_summary             = "Endprüfung Daten."; 
            $view->form_send                = "Bestellung abschicken."; 
            
            $view->str_billing_info = "Zahlungsinformationen";
            $view->str_delivery_data = "Zustellinformationen";            
            $view->str_czpost = "Tschechische Post";
            $view->str_depost = "Deutsche Post";
            $view->str_payment = "Zahlungsoptionen:";
            $view->str_method1 = "Bar bei Übergabe (Nachnahme)";
            $view->str_method2 = "Kreditkarte oder PayPal (Vorauszahlung)";
            $view->str_message = "Anmerkungen (fakultativ):";
            $view->str_agree = "Ich stimme den <a onclick='' href='#podminky'>Verkaufsbedingungen</a> zu.";
            $view->str_agree2 = "Ich stimme mit Nutzungsbedingungen (siehe Unten).";
            $view->str_mailing = "Ich möchte den Newsletter erhalten.";
            $view->str_confirm = "Bestellung bestätigen &#187;";
            $view->str_back = "&#171; Zurück";
            $view->str_ordered = "Sie haben folgende Produkte bestellt:";
            $view->str_print = "Drucken";
            $view->str_pay = "Bezahlen &#187;";
            $view->str_check = "Klicken Sie bitte jetzt auf die Schaltfläche „Bezahlen“, um Ihre Bestellung zu bezahlen.";
            $view->str_thanks = "Danke schön!";
            $view->str_thanks2 = "Vielen Dank für Ihren Einkauf!";
            $view->str_conditions = "VERKAUFSBEDINGUNGEN";
            $view->str_received = "Auftrag angenommen";
            $view->str_sent = "Auftrag ausgeliefert";
            
            //Faktura
            $view->str_invoice = "Rechnung Nr. ";
            $view->str_supplier = "Verkäufer";
            $view->str_buyer = "Empfänger";
            $view->str_company = "Firma:";
            $view->str_ico = "IČ:";
            $view->str_dic = "DIČ";
            $view->str_listed = "Tschechien, ist im Handelsregister, geführt am Stadtgericht Prag, Abteilung C, Einlage 148378, eingetragen.";
            $view->str_date1 = "Rechnungsdatum: ";
            $view->str_date2 = "Leistungsdatum: ";
            $view->str_date3 = "Fälligkeitsdatum: ";
            $view->str_paytype = "Zahlungsart: ";
            $view->str_paypal = "Paypal account: ";
            $view->str_transfer1 = "Vor Lieferung, bargeldlos";
            $view->str_transfer2 = "Überweisung";
            $view->str_transport = "Versand: ";
            $view->str_orderID = "Bestellnr.: ";
            $view->str_phone = "Tel.: ";
            $view->str_shipping = "Versandkosten: ";
            $view->str_total = "Gesamtbetrag: ";
            $view->str_vat1 = "MwSt.";
            $view->str_vat2 = "MwSt. %";
            $view->str_store = "Adresse des Shops: ";
            $view->str_final = "Gesamtbetrag: ";
            $view->str_final_cashondelivery = "Bezahlt Nachnahme: ";
            $view->str_account_sum = "Summe: ";
            $view->str_account_no = "Kontonummer: ";
            $view->str_account_iban = "IBAN: ";
            $view->str_account_bankId = "Bank ID: ";
            $view->str_account_var = "Variables Symbol: ";
            $view->str_account_swift = "SWIFT: ";
            $view->str_account_rate = "Exchange rate: ";
            //chyby v objednávacím formuláři
            $view->err_fill_order = "Füllen Sie bitte Ihre Zahlungsanweisung.";
            $view->err_fill_address1 = "Sie müssen eine Versandadresse auswählen.";
            $view->err_fill_shipping = "Bitte füllen Sie die Methode der Übergabe der Ware.";
            $view->err_fill_name = "Bitte geben Sie Ihren Namen.";
            $view->err_fill_surname = "Bitte füllen Sie Ihren Nachnamen an.";
            $view->err_fill_phone = "Bitte geben Sie Ihre Telefonnummer an.";
            $view->err_fill_email = "Bitte füllen Sie eine gültige E-Mail-Adresse ein.";
            $view->err_fill_address2 = "Bitte geben Sie Ihre Adresse.";
            $view->err_fill_city = "Bitte füllen Sie die Stadt.";
            $view->err_fill_zip = "Bitte füllen Sie das Kennzeichen (nur numerische Werte).";
            $view->err_fill_country = "Bitte füllen Sie das Lieferland.";
            $view->err_fill_agree = "Sie müssen sich mit unseren Bedingungen einverstanden sind, um Ihre Bestellung abzuschließen.";

            $view->week = $array = array("neděle", "pondělí", "úterý", "středa", "čtvrtek", "pátek", "sobota");
            //$view->month = $array = array ("", "leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec");
            $view->month = $array = array("", "LEDEN", "ÚNOR", "BŘEZEN", "DUBEN", "KVĚTEN", "ČERVEN", "ČERVENEV", "SRPEN", "ZÁŘÍ", "ŘÍJEN", "LISTOPAD", "PROSINEC");
        } else {
            //hlavní stránka
            $view->str_ourproducts = "Our Products";
            $view->str_about = "About us";
            $view->str_aboutourproducts = "Woolen blankets and carpets";
            $view->str_basket = "Basket";
            $view->str_balkanova = "Balkanova - original Bulgarian carpets and blankets";
            $view->str_news = "News";
            $view->str_contact = "Contact";            
            $view->str_contact_de = "Contact for Germany: ";
            $view->str_contact_cz = "Contact info: ";
            $view->str_shop = "Balkanova shop";
            $view->str_resellers = "Shops carrying Balkanova products";
            $view->str_facebook_claim = "You can follow us also on Facebook";
            $view->str_sitemap = "Sitemap";
            $view->str_terms = "Terms and Conditions";
            $view->str_noproducts = "Unfortunately, there are no products in this category at the time.";
            $view->str_carpets_ready = "These carpets are for sale immediately:";            
            //cena            
            $view->str_price0 = "Price: ";
            $view->str_price1 = "price without VAT: ";
            $view->str_price2 = "price including VAT: ";
            $view->str_price3 = "price (per item): ";
            $view->str_price4 = "Price per meter: ";
            $view->str_price5 = "Price per 0,5 meter: ";
            $view->str_price6 = "Price per m&sup2;: ";
            //objednávka
            $view->str_ask = "Ask about this product";
            $view->str_custom = "Order custom size";
            $view->str_available = "Pcs available:";
            $view->str_pile = "Pile: ";
            $view->str_pile1 = "cm";
            $view->str_grams = "Weight/m&#178;: : ";
            $view->str_grams1 = " g/m&#178;";
            $view->str_size = "Size: ";
            $view->str_material = "Material:";
            $view->str_production = "Production:";
            $view->str_size = "Size:";
            $view->str_weight = "Weight:";
            $view->str_stock = "In stock:";
            $view->str_pcs = "pcs";
            $view->str_add = "Add to basket";
            $view->str_empty = "This category is empty";
            $view->str_question = "Ask about this product";
            $view->str_care = "Care";
            $view->str_coarse1 = "Mountain Man (course) – one feather - the&nbsp;Rhodopa blankets";
            $view->str_coarse2 = "Rustic (slightly course) – two feathers - the&nbsp;Perelika blankets";
            $view->str_coarse3 = "Gentle (soft) – three feathers - the&nbsp;Karandilla blankets";
            $view->str_coarse4 = "Downy (ultra soft) – four feathers - the Merino&nbsp;blankets";
            $view->str_bysoftness = "By softness";
            $view->str_softness_text = "Softness";
            $view->str_softness_desc = "The most of the blankets in this category have the softness marked below";
            $view->str_softness_1 = "Mouintain man";
            $view->str_softness_2 = "Rustic";
            $view->str_softness_3 = "Gentle";
            $view->str_softness_4 = "Downy";
            //objednávání
            $view->str_order = "Order";
            $view->str_order_id = "Order ID:";
            $view->str_order_status = "Order status:";
            $view->str_order_timestamp = "Date of receipt:";
            $view->str_order_shipping_address = "Mailing address:";
            $view->str_order_step = "step";
            $view->str_order_continue = "Continue";
            $view->str_order_back = "Back";
            $view->str_order_empty_basket = "Empty basket";
            $view->str_order_fill_order = "Fill in form &#187;";
            $view->str_order_send = "Send form";
            $view->str_order_select = "Choose your pick-up location";
            $view->str_order_select_shipping = "Choose preferred shipping:";
            $view->str_order_select_payment = "Choose preferred payment:";
            $view->str_order_message = "Message for us (optional)";
            $view->str_order_product = "Product name";
            $view->str_order_size = "Size";            
            $view->str_order_quantity = "Quantity";
            $view->str_order_unit_price = "Price per piece";
            $view->str_order_total_price = "Total price";
            $view->str_order_total = "Total ";        
            $view->str_order_percent_of = "of";
            $view->str_order_details = "Order details ";
            $view->str_order_payment = "Payment details ";
            $view->str_order_VATincluded = "All prices include VAT.";
            $view->str_order_check = "You can check the status of your order here:";
            $view->str_order_link = "Show order status";
            $view->str_order_ready = "The order is ready for pickup";
            $view->str_order_sent = "Your order has been shipped";
            $view->str_order_subject = "We have recieved your order";
            $view->str_order_packing = "package"; 
            $view->str_order_paypal = "Pay via PayPal"; 
            $view->str_order_session = "Personal information is deleted from the cache (session).";
            
            $view->str_picture = "picture";
            $view->str_product = "product";
            $view->str_items = "number of items";              
            $view->str_price = "price";    
            $view->str_sum = "total price";  
            $view->str_code = "product code: ";
            $view->str_note = "note: ";
            $view->str_size = "Size: ";
            $view->str_size2 = "Width: ";
            $view->str_weight = "Weight:";
            $view->str_weight2 = "min. order: ";
            $view->str_shipping1    = "Shipping: ";
            $view->str_shipping2    = "Shipping method: ";            
            $view->str_payment1     = "Payment: ";            
            $view->str_payment2     = "Payment method: "; 
            $view->str_total = "total: ";
            $view->str_empty = "Empty basket";
            $view->str_fill = "Fill in form &#187;";
            $view->str_incomplete = "The form is missing some necessary information!";
            $view->str_required = "Before we can accept your order, please make sure you have filled in all the required information.";
            $view->str_sofar = "So far your basket is empty!";
            $view->str_address = "Address";
            $view->str_shipping = "Shipping";
            $view->str_payment = "Payment";
            $view->str_summary = "Summary";
            
            $view->form_billing             = "Billing information";
            $view->form_name                = 'Name:';
            $view->form_surname             = 'Surname:';
            $view->form_phone               = 'Phone number:';
            $view->form_email               = 'E-mail address:';
            $view->form_address             = 'Address:';
            $view->form_city                = 'City:';
            $view->form_zip                 = 'Area code:';
            $view->form_country             = 'Country:';
            $view->form_delivery            = 'Delivery:';
            $view->form_payment             = 'Payment method:';       
            $view->form_available1          = 'Unfortunately we have only';
            $view->form_available2          = 'products to offer.';
            $view->form_shipping_address    = "Mailing address:";  
            $view->form_fill_shipping       = "Please fill in a different delivery address.";
            $view->form_order_company       = "The invoice will be issued to a company";
            $view->form_order_company_name  = "The name of the company:";
            $view->form_order_company_id    = "Company registration number:";
            $view->form_order_diff          = "Delivery data deffers from billing information";
            $view->form_back                = "Back";
            $view->form_select_shipping     = "Select shipping";
            $view->form_select_payment      = "Select payment";
            $view->form_select_summary      = "Summary";
            $view->form_order_sendmail      = "I wish to receive news to email.";
            $view->form_order_adult         = "I declare I am 18 years of age or older.";
            $view->form_summary             = "Final summary"; 
            $view->form_send                = "Send order"; 
            
            $view->str_billing_info = "Billing information";
            $view->str_delivery_data = "Delivery address";            
            $view->str_czpost = "Czech Post";
            $view->str_depost = "Deutsche Post";
            $view->str_payment = "Payment method:";
            $view->str_method1 = "Cash at delivery";
            $view->str_method2 = "Credit Card or Paypal (now)";
            $view->str_message = "Message for us (optional):";
            $view->str_agree = "I agree with the <a onclick='' href='#podminky'>terms and conditions</a>";
            $view->str_agree2 = "I agree with terms and conditions (see below).";
            $view->str_mailing = "Add me to your mailing list:";
            $view->str_confirm = "Confirm order &#187;";
            $view->str_back = "&#171; Back";
            $view->str_ordered = "You have ordered the following products:";
            $view->str_print = "Print";
            $view->str_pay = "Pay &#187;";
            $view->str_check = "Please check your order and select the payment button.";
            $view->str_thanks = "Thank you!";
            $view->str_thanks2 = "Thank you for your purchase";
            $view->str_conditions = "Terms and Conditions";
            $view->str_received = "Order received";
            $view->str_sent = "Order shipped";
            //Faktura
            $view->str_invoice = "Invoice no. ";
            $view->str_supplier = "Supplier";
            $view->str_buyer = "Buyer";
            $view->str_company = "Company:";
            $view->str_ico = "ID No. (IČ): ";
            $view->str_dic = "Tax No.: ";
            $view->str_listed = "Czech Republic, entered into the Commercial Register of the Municipal Court in Prague, Section C, insert 148378.";
            $view->str_date1 = "Day of Issue: ";
            $view->str_date2 = "Subject of taxation day: ";
            $view->str_date3 = "Due day: ";
            $view->str_paytype = "Method of payment: ";
            $view->str_paypal = "Paypal account: ";
            $view->str_transfer1 = "In advance";
            $view->str_transfer2 = "Transfer";
            $view->str_transport = "Transport: ";
            $view->str_orderID = "Order ID: ";
            $view->str_phone = "Phone: ";
            $view->str_product = "product";
            $view->str_shipping = "Shipping ";
            $view->str_code = "product code: ";
            $view->str_total = "total: ";
            $view->str_vat1 = "VAT";
            $view->str_vat2 = "VAT %";
            $view->str_store = "Shop adress: ";
            $view->str_final = "Total: ";
            $view->str_final_cashondelivery = "Paid on delivery: ";
            $view->str_account_sum = "Sum: ";
            $view->str_account_no = "Account number: ";
            $view->str_account_iban = "IBAN: ";
            $view->str_account_bankId = "Bank ID: ";
            $view->str_account_var = "Variable symbol: ";
            $view->str_account_swift = "SWIFT: ";
            $view->str_account_rate = "Exchange rate: ";
            //chyby v objednávacím formuláři
            $view->err_fill_order = "Please fill in your method of payment.";
            $view->err_fill_address1 = "You must select a shipping address.";
            $view->err_fill_shipping = "Please fill out your method of shipping.";
            $view->err_fill_name = "Please fill in your name.";
            $view->err_fill_surname = "Please fill in your surname.";
            $view->err_fill_phone = "Please fill in your phone number.";
            $view->err_fill_email = "Please fill in a valid email address.";
            $view->err_fill_address2 = "Please fill in your address.";
            $view->err_fill_city = "Please fill in your city.";
            $view->err_fill_zip = "Please fill in your zip code (numbers only).";
            $view->err_fill_country = "Please fill in your country.";
            $view->err_fill_agree = "You must agree with our terms and conditions to finalise your order.";
        }
        
        //načteme si všechny důležité texty na všechny stránky z databáze
        $modelTexts = new Model_DbTable_Texts();
        $texts = $modelTexts->fetchTexts();
        $view->texts = $texts;
    }

}
