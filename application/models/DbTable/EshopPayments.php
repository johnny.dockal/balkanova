<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopPayments extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_payments';
    protected $_primary = 'payment_id';

    public function fetchPayments($shipping_id = null, $country_id = null) {
        $session = new Zend_Session_Namespace('Default');
        $db = Zend_Db_Table::getDefaultAdapter();
        if (isset($shipping_id) and isset($country_id)) {            
            $sql = "SELECT 
                        eshop_payments.payment_id, 
                        eshop_payments.public, 
                        eshop_payments.title_$session->lang AS title,      
                        eshop_payments.text_$session->lang AS text, 
                        eshop_payments.price_cz AS price,
                        eshop_payments.price_percent AS percentage
                    FROM eshop_payments 
                    JOIN eshop_country_delivery_payment 
                        ON eshop_payments.payment_id = eshop_country_delivery_payment.payment_id
                    WHERE eshop_country_delivery_payment.country_id = '$country_id'
                        AND eshop_country_delivery_payment.shipping_id = '$shipping_id'
                        AND eshop_country_delivery_payment.public = '1' 
                        AND eshop_payments.public = '1' ";            
        } else {
            $sql = "SELECT 
                        eshop_payments.payment_id, 
                        eshop_payments.public, 
                        eshop_payments.title_$session->lang AS title,      
                        eshop_payments.text_$session->lang AS text, 
                        eshop_payments.price_cz AS price,
                        eshop_payments.price_percent AS percentage
                    FROM eshop_payments";            
        }
        try {
                $result = $db->fetchAll($sql);
            } catch (Zend_Exception $e) {
                echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
                echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
                echo "\n <br/>SQL: " . $sql . "\n <br/>";
            }
            return $result;
    }

    public function fetchSalePayments() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT 
                        eshop_payments.payment_id, 
                        eshop_payments.public, 
                        eshop_payments.title_cz AS title,      
                        eshop_payments.text_cz AS text, 
                        eshop_payments.price_cz AS price                        
                    FROM eshop_payments                     
                    WHERE eshop_payments.payment_id IN ('15', '16')";
        try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }

}
