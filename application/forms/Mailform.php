<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Mailform extends Zend_Form {

    public function __construct($action) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('class', 'admintable');
        $this->setAttrib('enctype', 'multipart/form-data');
        
        $recipient = new Zend_Form_Element_Text("email", array("readonly" => "readonly", "class" => "textboxwide"));
        $recipient->setLabel('Adresát:')->setRequired(true);
        
        $status_id = new Zend_Form_Element_Hidden('status_id');
        //zabrání zobrazení labelu
        $status_id->setDecorators(array('ViewHelper'));
        
        $order_id = new Zend_Form_Element_Hidden('order_id');
        //zabrání zobrazení labelu
        $order_id->setDecorators(array('ViewHelper'));
        
        $subject = new Zend_Form_Element_Text("subject", array("class" => "textboxwide"));
        $subject->setLabel('Předmět zprávy:')->setRequired(true);
        
        $text = new Zend_Dojo_Form_Element_Textarea('text', array('class' => "textboxextrabig"));
        $text->setLabel('Text:')->setRequired(true);
        
        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Odeslat", 'class' => "savebutton"));

        $this->addElements(array(
            $order_id, $status_id, $recipient, $subject, $text, $submit
        ));
        
    }
}
