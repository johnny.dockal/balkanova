$(function(){

	$('a.popup').click(function(){
		if($(this).attr('href').substr(-3) == 'jpg'){
			popupImg($(this).attr('href'),$(this));
		}else{


			popupPage($(this));
		}
		return false;
	});
});

popupPage = function(elem){
	height=500;
	href=elem.attr('href');
	title=elem.attr('title');
	if(elem.is('.wide')){
		width=845;
	}else{
		if(elem.is('.narrow')){
			width=620;
		}else{
			width=720;
		}
	}

	subwin = window.open(href,title,"height="+height+",width="+width+",toolbar=no, menubar=no, scrollbars=no, resizable=yes, location=no, directories=no, status=no");
	if(!subwin) {return false;}

	subwin.focus();
	return true;
}


popupImg = function(href,elem){
	var title="Koberec"

	if (elem.is('.wide')) {
		var height=270;
		var width=610;
	} else {
		var height=400;
		var width=295;
	}

	subwin = window.open("","","height="+height+",width="+width+", toolbar=no, menubar=no, scrollbars=no, resizable=yes, location=no, directories=no, status=no");
		if (!subwin) {return false;}

     	subwin.document.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml">');
     	subwin.document.write('<head><title>'+title+'</title><style type="text/css">body{margin:0;padding:0;background:#F9F9F9 no-repeat center;}</style></head>\n');
     	subwin.document.write('<body><p><img width="'+width+'" height="'+height+'" src="'+href+'" alt="'+title+'" title="" onclick="window.close();" /></p></body>\n');
     	subwin.document.write('</html>\n');
     	subwin.document.close();

		subwin.focus();
		return true;
}
