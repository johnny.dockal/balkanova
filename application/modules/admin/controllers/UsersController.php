<?php

class Admin_UsersController extends Zend_Controller_Action
{

    public function init() {
        $this->view->headTitle('Uživatelé', 'POSTEND');
    }

    public function indexAction() {
        //připravíme formulář pro přidání nového uživatele
        $form = new Form_Userform();
        $this->view->form = $form;
        //připravíme výtah z tabulky uživatelů pro tabulku
        $modelUsers = new Model_DbTable_Users();
        $users = $modelUsers->fetchUsers();
        $this->view->users = $users;
    }
    
    public function saveAction() {
        $user_id = $this->getParam('user_id');
        $table = new Model_DbTable_Users();
        if (empty($user_id)) {
            $data = array( 
                'user_login'        => $this->getParam('user_login'),
                'user_email'        => $this->getParam('user_email'),
                'user_name'         => $this->getParam('user_name'),
                'user_role'         => $this->getParam('user_role'),
                'user_login_hashed' => md5($this->getParam('user_login')),
                'user_pswd_hashed'  => md5($this->getParam('user_pswd'))
            );
            $table->insert($data);
            $this->_redirect('/admin/users/');
        } else {
            $pswd = $this->getParam('user_pswd');
            $check = $this->getParam('pswd_check');
            $data = array( 
                'user_login'        => $this->getParam('user_login'),
                'user_email'        => $this->getParam('user_email'),
                'user_name'         => $this->getParam('user_name'),
                'user_role'         => $this->getParam('user_role'),
                'user_login_hashed' => md5($this->getParam('user_login'))
            );
            if (!empty($pswd) && !empty($check) && $pswd == $check) {
                $data['user_pswd_hashed'] = md5($pswd);
                $where = $table->getAdapter()->quoteInto('user_id = ?', $user_id);
                $table->update($data, $where);
                $this->_redirect('/admin/users/');
            } else if (!empty($pswd) && !empty($check)) {
                $this->_redirect('/admin/users/edit/?user_id='.$user_id.'&nomatch=true');
            } else {
                $where = $table->getAdapter()->quoteInto('user_id = ?', $user_id);
                $table->update($data, $where);
                $this->_redirect('/admin/users/');
            }
            
        }
        
    }
    
    public function editAction() {
        $user_id = $this->getParam('user_id');
        $nomatch = $this->getParam('nomatch');
        $table = new Model_DbTable_Users();
        $form = new Form_Userform();   
        $userdata = $table->find($user_id)->toArray();
        $form->populate($userdata[0]);
        $this->view->name = $userdata[0]['user_name'];
        $this->view->nomatch = $nomatch;
        $this->view->form = $form;
    }
    
    public function disableAction() {
        $user_id = $this->getParam('user_id');
        if (isset($user_id)) {
            $table = new Model_DbTable_Users();
            $table->disableUser($user_id);
        } 
        $this->_redirect('/admin/users/');
    }
    
    public function enableAction() {
        $user_id = $this->getParam('user_id');
        if (isset($user_id)) {
            $table = new Model_DbTable_Users();
            $table->enableUser($user_id);
        } 
        $this->_redirect('/admin/users/');
    }


}

