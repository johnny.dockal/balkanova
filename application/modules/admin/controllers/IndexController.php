<?php

class Admin_IndexController extends Zend_Controller_Action
{

    public function init() {
        $this->view->headTitle('Úvodní text', 'POSTEND');
    }

    public function indexAction() {
        $this->_redirect('/admin/orders');
    }

    public function saveAction() {
        $table = new Model_DbTable_Texts();
        $table->updateText();
        $this->_redirect('/admin/');
    }
}

