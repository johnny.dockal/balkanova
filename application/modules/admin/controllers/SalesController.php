<?php

class Admin_SalesController extends Zend_Controller_Action {

    private $category_id = 'all';
    private $page = 1;
    private $orderBy = 'product_id';
    private $sort = 'desc';
    private $search = null;
    private $limit = 50;
    private $productArray = null;
    private $adminSession = null;
    private $order = null;
    private $cartProducts = null;
    private $msg_available = null;

    public function init() {
        $this->adminSession = new Zend_Session_Namespace('Admin');
        $this->order = new Model_EshopOrder();
        $this->error_msg = array();
    }

    public function preDispatch() {
        $category_id = $this->getParam('category_id');
        $this->search = $this->getParam('search');
        $page = $this->getParam('page');
        $order = $this->getParam('order');
        $sort = $this->getParam('sort');
        //pokud chytíme parametr update, tak se editují počty zboží v košíku
        $count = $this->_getParam('update');
        
        if ($count) {
            $inventory = new Model_DbTable_EshopProductHistory();
            $product_ids = array();
            $i = 1;
            while ($i <= $count) {
                $product_id = $this->_getParam('product_id-' . $i);
                $product_ids[$product_id] = $product_id;
                $i++;
            }
            $available = $inventory->fetchAvailable($product_ids);
            $j = 1;
            while ($j <= $count) {
                $product_id = $this->_getParam('product_id-' . $j);
                //chci jenom čísla
                $quantity = filter_var($this->_getParam('quantity-' . $j), FILTER_SANITIZE_NUMBER_INT);
                if (empty($quantity)) {$quantity = 0;}
                $size = $this->_getParam('size-' . $j);
                $price = $this->_getParam('price-' . $j);
                if (isset($available[$product_id]) and ( $quantity > $available[$product_id]['available'])) {
                    //$quantity = $available[$product_id]['available'];
                    if ($quantity < 0) {$quantity = 0;}
                    $this->msg_available[$product_id] = $available[$product_id]['available'];
                } else if ($quantity > 50) {
                    $quantity = 50;
                    $this->msg_available[$product_id] = $quantity;
                }
                $this->order->updateProduct($product_id, $quantity, $size, $price);
                $j++;
            }
        } 

        if (!empty($category_id)) {
            $this->category_id = $category_id;
            $this->adminSession->category_id = $category_id;
            //pokus se mění kategorie, musí se jít zpátky na stránku 1, aby nedošlo k out of bounds exception
            $this->adminSession->page = $this->page;
            //zároveň smažeme keyword pro hledání
            $this->adminSession->search = '';
        } else {
            $this->category_id = $this->adminSession->category_id;
        }
        if (empty($this->adminSession->category_id)) {
            $this->adminSession->category_id = $this->category_id;
        }
        if (!empty($this->search)) {
            $this->adminSession->search = $this->search;
        }
        //nastavíme stránku
        if (!empty($page)) {
            $this->page = $page;
            $this->adminSession->page = $page;
        } else if (empty($this->adminSession->page)) {
            $this->adminSession->page = $this->page;
        } else {
            $this->page = $this->adminSession->page;
        }
        //nastavíme pořadí
        if (!empty($order)) {
            $this->orderBy = $order;
            $this->adminSession->order = $order;
        } else {
            $this->orderBy = $this->adminSession->order;
        }
        if (empty($this->adminSession->order)) {
            $this->adminSession->order = $this->orderBy;
        }
        if (!empty($sort)) {
            $this->sort = $sort;
            $this->adminSession->sort = $sort;
        } else {
            $this->sort = $this->adminSession->sort;
        }
        if (empty($this->adminSession->sort)) {
            $this->adminSession->sort = $this->sort;
        }

        $this->view->category_id = $this->adminSession->category_id;
        $this->view->search = $this->adminSession->search;
        $this->view->page = $this->adminSession->page;
        $this->view->orderBy = $this->adminSession->order;
        $this->view->sort = $this->adminSession->sort;
    }

    public function indexAction() {
        $adminSession = new Zend_Session_Namespace('Admin');

        $status = new Model_DbTable_OrderStatus();
        $statusArray = $status->fetchAll('status_id >= 0')->toArray();
        if (!isset($adminSession->year)) {
            $adminSession->year = date('Y');
        }
        $filter = array(
            '0' => null,
            '1' => null,
            '2' => null,
            '3' => null,
            '4' => null,
            '5' => null,
            '6' => null,
            '7' => null,
            '8' => '8',
            '9' => '9'
        );

        $model = new Model_DbTable_EshopOrders();
        $this->view->orders = $model->fetchOrders($adminSession->year, $filter, APP_ID);

        $this->view->status = $statusArray;
        $this->view->year = $adminSession->year;
        $this->view->filter = $adminSession->filter;
    }

    public function postDispatch() {  
        if (!isset($this->cartProducts)) {
            $this->cartProducts = $this->order->fetchCartProducts();
        }
        $this->view->cartProducts = $this->cartProducts;
        $this->view->msg_available = $this->msg_available;
    }

    public function editAction() {
        $order_id = $this->getParam('order_id');
        $model = new Model_EshopOrder();
        $model->fetchOrder($order_id);
        $this->view->order = $model;
        $history = new Model_DbTable_EshopOrderHistory();
        $this->view->orderHistory = $history->fetchOrderHistory($order_id);
    }

    public function newAction() { 
        $categories = new Model_DbTable_EshopCategories();
        $model = new Model_DbTable_EshopProductHistory();
        if (!empty($this->adminSession->search)) {
            $products = $model->searchInventory($this->adminSession->search);
        } else {
            $products = $model->fetchInventory($this->category_id);
        }
        $this->view->categories = $categories->fetchAll()->toArray();
        $this->view->itemlimit = 60;
        $this->view->itemcount = count($products);
        $this->view->pagecount = ceil($this->view->itemcount / 60);
        $this->view->products = array_chunk($products, 60);     
    }

    public function paymentAction() {
        $model = new Model_DbTable_EshopPayments();
        $this->view->paymentOptions = $model->fetchSalePayments();
        $this->view->noedit = true;
    }

    public function saveAction() {
        $user = Zend_Auth::getInstance()->getIdentity();
        $payment_id = $this->getParam('payment_id');
        if (empty($payment_id)) {
            $this->_redirect('/admin/sales/payment/');
        } else {
            $formParams = array();
            $formParams['order_name'] = $user->user_name;
            $formParams['order_surname'] = $user->user_role;
            $formParams['order_phone'] = '776332522';
            $formParams['order_email'] = $user->user_email;
            $formParams['order_address'] = 'Karoliny Světlé 23';
            $formParams['order_city'] = 'Praha 1';
            $formParams['order_diff'] = '0';
            $formParams['order_company'] = '0';
            $formParams['order_zip'] = '1100';
            $formParams['country_id'] = '1'; //ČR
            $formParams['shipping_id'] = '1'; //Osobní odběr
            $formParams['payment_id'] = $payment_id;
            $formParams['order_message'] = '';
            $formParams['order_agree'] = '1';
            $formParams['order_adult'] = '1';
            $formParams['user_id'] = $user->user_id;
        }
        $this->order->setFormParameters($formParams);
        $this->order->saveOrder('8'); //status 8 = prodej na krámě
        $this->destroySession();
        $this->_redirect("/admin/sales/");
    }

    public function addAction() {
        $count = $this->getParam('count');
        for ($i = 1; $i <= $count; $i++) {
            $product_id = $this->getParam($i . '-product_id');
            $quantity = $this->getParam($i . '-quantity');
            $price = $this->getParam($i . '-price_cz');
            if ($quantity > 0) {
                $this->order->addProduct($product_id, $quantity, null, $price);
            }
        }
        $this->_redirect('/admin/sales/new/');
    }

    public function emptyAction() {
        $product_id = $this->_getParam('product_id');

        if ($product_id == 'all') {
            Zend_Session::namespaceUnset('Cart');
        } else {
            unset($this->cart->$product_id);
        }
        $this->_redirect("/admin/sales/new/");
    }

    public function saleAction() {
        $model = new Model_DbTable_EshopProducts();
        $products = $model->fetchProductsByIds($productIds);
        //$products = $model->fetchProductsByCat();
        $this->view->categories = $categories->fetchAll()->toArray();
        $this->view->itemlimit = 60;
        $this->view->itemcount = count($products);
        $this->view->pagecount = ceil($this->view->itemcount / 60);
        $this->view->products = array_chunk($products, 60);
    }

    private function destroySession() {
        Zend_Session::namespaceUnset('Order');
        Zend_Session::namespaceUnset('Cart');
        $defaultSession = new Zend_Session_Namespace('Default');
        $defaultSession->sum = 0;
    }

    public function stornoAction() {
        $order_id = $this->getParam('order_id');
        $redirect = $this->getParam('redirect');
        $model = new Model_DbTable_EshopOrders();
        $login = Zend_Auth::getInstance()->getIdentity()->user_login_hashed;
        $role = Zend_Auth::getInstance()->getIdentity()->user_role;
        //$login je vyjimka pro marketu
        if ($login == '81d6f316d169150d0e8733866c38684d' or $role == 'admin' or $role == 'superadmin') {
            $model->changeStatus($order_id, 9);
        }
        if (!empty($redirect)) {
            $this->_redirect('/admin/' . $redirect . '?order_id=' . $order_id);
        } else {
            $this->_redirect('/admin/sales/');
        }
    }

}
