<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Settingsform extends Zend_Form {

    public function __construct($setting, $i) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction('/admin/settings/save/setting_number/' . $i . '/');
        $this->setAttrib('class', 'orderform');

        $view = Zend_Layout::getMvcInstance()->getView();

        $groupElements = array();
        
        $setting_id = new Zend_Form_Element_Hidden('setting_id_' . $i);
        $setting_id->setValue($setting['setting_id']);
        $setting_id->setDecorators(array('ViewHelper'));
        $this->addElement($setting_id);
        array_push($groupElements, $setting_id);

        $setting_value1 = new Zend_Dojo_Form_Element_TextBox('setting_value1_' . $i, array('class' => 'textboxwide'));
        $setting_value1->setLabel($setting['setting_description1'])->setValue($setting['setting_value1'])->setRequired(true);
        $this->addElement($setting_value1);
        array_push($groupElements, $setting_value1);

        if ($setting['setting_value2']) {
            $setting_value2 = new Zend_Dojo_Form_Element_TextBox('setting_value2_' . $i, array('class' => 'textboxwide'));
            $setting_value2->setLabel($setting['setting_description2'])->setValue($setting['setting_value2'])->setRequired(true);
            $this->addElement($setting_value2);
            array_push($groupElements, $setting_value2);
        }

        if ($setting['setting_text_cz']) {
            $setting_text_cz = new Zend_Form_Element_Textarea('setting_text_cz_' . $i, array('class' => 'textbox'));
            $setting_text_cz->setLabel('Doprovodný text v češtině')->setValue($setting['setting_text_cz'])->setRequired(true);
            $this->addElement($setting_text_cz);
            array_push($groupElements, $setting_text_cz);
        }

        if ($setting['setting_text_en']) {
            $setting_text_en = new Zend_Form_Element_Textarea('setting_text_en_' . $i, array('class' => 'textbox'));
            $setting_text_en->setLabel('Doprovodný text v angličtině')->setValue($setting['setting_text_en'])->setRequired(true);
            $this->addElement($setting_text_en);
            array_push($groupElements, $setting_text_en);
        }

        if ($setting['setting_text_de']) {
            $setting_text_de = new Zend_Form_Element_Textarea('setting_text_de_' . $i, array('class' => 'textbox'));
            $setting_text_de->setLabel('Doprovodný text v němčině')->setValue($setting['setting_text_de'])->setRequired(true);
            $this->addElement($setting_text_de);
            array_push($groupElements, $setting_text_de);
        }
        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit nastavení", 'class' => "button bg-green border-style active"));
        $this->addElement($submit);
        array_push($groupElements, $submit);

        $this->addDisplayGroup($groupElements, 'settingGroup', array('legend' => 'My legend'));
        $group = $this->getDisplayGroup('settingGroup');
        $group->setDescription($setting['setting_id']);
        $group->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'div', 'style' => 'border: 1px solid black')),
            array('Description', array('tag' => 'h2', 'placement' => 'prepend'))
        ));        
    }
}
