<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopProducts extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_products';
    protected $_primary = 'product_id';
    protected $_limit = 30;
    protected $_productQuery_grouped = null;
    protected $_productQuery = null;
    protected $_productQueryEdit = null;
    private $lang = null;
    private $lang_cat_subcat = null;

    public function init() {
        $session = new Zend_Session_Namespace('Default');
        $adminSession = new Zend_Session_Namespace('Admin');
        //v německém locale je správná informace uložena v kolonce pro češtinu
        if ($session->lang == 'de') {
            $this->lang_cat_subcat = 'cz';
        } else {
            $this->lang_cat_subcat = $session->lang;
        }
        //na balkanově je možno v administraci kouknout na německé názvy produktů
        $this->lang = $session->lang;
        $module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
        if (($module == 'admin') && isset($adminSession->viewlocale)) {
            $this->lang = $adminSession->viewlocale;
        }
        $this->_productQuery = "SELECT "
                . "GROUP_CONCAT(DISTINCT c.category_id SEPARATOR ';') AS category_id, "
                . "GROUP_CONCAT(DISTINCT c.eshop_id SEPARATOR ';') AS esop_id, "
                . "GROUP_CONCAT(DISTINCT c.title_$this->lang_cat_subcat SEPARATOR ';') AS category_title, "
                . "GROUP_CONCAT(DISTINCT c.url_cz SEPARATOR ';') AS category_alias, "
                . "GROUP_CONCAT(DISTINCT s.subcategory_id SEPARATOR ';') AS subcategory_id, "
                . "GROUP_CONCAT(DISTINCT s.title_$this->lang_cat_subcat SEPARATOR ';') AS subcategory_title, "
                . "GROUP_CONCAT(DISTINCT s.url_cz SEPARATOR ';') AS subcategory_alias, "               
                . "p.product_id, "
                . "p.status, "
                . "p.code, "
                . "p.sequence, "
                . "p.title_$this->lang AS title, "
                . "p.alias_$this->lang AS alias, "
                . "p.full_title_$this->lang AS full_title, "
                . "p.text_$this->lang AS text, "
                . "p.note_$this->lang AS note, "
                . "p.material_$this->lang AS material, "
                . "p.manufactured_$this->lang AS manufactured, "
                . "p.price_unit_" . APP_LOCALE . " AS price_unit, "
                . "p.price_" . APP_LOCALE . " AS price, "
                . "p.size, "
                . "p.weight, "
                . "p.weight_unit, "
                . "p.grams, "
                . "p.hair, "
                . "p.certified, "
                . "p.new "
                //. "ph1.available "
                . "FROM $this->_name AS p "
                . "LEFT JOIN (eshop_subcat_products AS sp, eshop_subcategories AS s, eshop_categories AS c) "
                . "ON (sp.product_id = p.product_id AND s.subcategory_id = sp.subcategory_id AND c.category_id = s.category_id) ";
                //. "LEFT JOIN (SELECT product_id, timestamp, (inv2b + inv3b) AS available FROM eshop_product_history AS ph1 WHERE timestamp = (SELECT MAX(timestamp) FROM eshop_product_history AS ph2 WHERE ph1.product_id = ph2.product_id)) AS ph1 "
                //. "ON p.product_id = ph1.product_id ";  
        $this->_productQueryAdmin = "SELECT "
                . "GROUP_CONCAT(c.category_id SEPARATOR ';') AS category_id, "                
                . "GROUP_CONCAT(DISTINCT c.eshop_id ORDER BY c.eshop_id ASC SEPARATOR ';') AS eshop_id, "  
                //. "c.title_$this->lang AS cat_title, "
                . "GROUP_CONCAT(DISTINCT s.subcategory_id SEPARATOR ';') AS subcategory_id, "
                . "GROUP_CONCAT(DISTINCT s.title_$this->lang_cat_subcat SEPARATOR ';') AS subcat_title, "
                . "GROUP_CONCAT(DISTINCT s.url_$this->lang_cat_subcat SEPARATOR ';') AS subcat_alias, "
                //. "GROUP_CONCAT(s.text_$this->lang AS subcat_text, "
                //. "GROUP_CONCAT(s.maintainance_$this->lang AS maintainance, "
                . "p.product_id, "
                . "p.status, "
                . "p.code, "
                . "p.sequence, "
                . "p.title_$this->lang AS title, "
                . "p.alias_$this->lang AS alias, "
                . "p.full_title_$this->lang AS full_title, "
                . "p.text_$this->lang AS text, "
                . "p.note_$this->lang AS note, "
                . "p.material_$this->lang AS material, "
                . "p.manufactured_$this->lang AS manufactured, "
                . "p.price_unit_cz, "
                . "p.price_unit_de, "
                . "p.price_cz, "
                . "p.price_de, "
                . "p.size, "
                . "p.weight, "
                . "p.weight_unit, "
                . "p.grams, "
                . "p.hair, "
                . "p.certified, "
                . "p.new "
                . "FROM $this->_name AS p "
                . "JOIN (eshop_subcat_products AS sp1, eshop_subcategories AS s1, eshop_categories AS c1)  "
                . "ON (sp1.product_id = p.product_id AND s1.subcategory_id = sp1.subcategory_id AND c1.category_id = s1.category_id) "
                . "LEFT JOIN (eshop_subcat_products AS sp, eshop_subcategories AS s, eshop_categories AS c) "
                . "ON (sp.product_id = p.product_id AND s.subcategory_id = sp.subcategory_id AND c.category_id = s.category_id) ";
        $this->_productQueryEdit = "SELECT "
                . "GROUP_CONCAT(c.category_id SEPARATOR ';') AS category_id, "
                //. "c.title_$this->lang AS cat_title, "
                . "GROUP_CONCAT(s.subcategory_id SEPARATOR ';') AS subcategory_id, "
                //. "s.title_$this->lang AS subcat_title, "
                //. "s.text_$this->lang AS subcat_text, "
                //. "s.maintainance_$this->lang AS maintainance, "
                . "p.product_id, "                
                . "p.status, "
                . "p.code, "
                . "p.sequence, "
                . "p.title_cz, "
                . "p.title_en, "
                . "p.title_de, "
                . "p.full_title_cz, "
                . "p.full_title_en, "
                . "p.full_title_de, "
                . "p.alias_cz, "
                . "p.alias_en, "
                . "p.alias_de, "
                . "p.text_cz, "
                . "p.text_en, "
                . "p.text_de, "
                . "p.note_cz, "
                . "p.note_en, "
                . "p.note_de, "
                . "p.material_cz, "
                . "p.material_en, "
                . "p.material_de, "
                . "p.manufactured_cz, "
                . "p.manufactured_en, "
                . "p.manufactured_de, "
                . "p.price_unit_cz, "
                . "p.price_unit_de, "
                . "p.price_cz, "
                . "p.price_de, "
                . "p.size, "
                . "p.weight, "
                . "p.weight_unit, "
                . "p.grams , "
                . "p.hair, "
                . "p.certified, "
                . "p.new "
                . "FROM $this->_name AS p LEFT JOIN (eshop_subcat_products AS sp, eshop_subcategories AS s, eshop_categories AS c) "
                . "ON (sp.product_id = p.product_id AND s.subcategory_id = sp.subcategory_id AND c.category_id = s.category_id) ";
    }

    public function fetchProduct($product_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $this->_productQuery . "WHERE p.product_id = $product_id GROUP BY p.product_id";
        try {
            $product = $db->fetchRow($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        //Pokud napoprvé není žádný výsledek, může se jednat o produkt, který není zařazen do žádné subkategorie
        if (empty($product)) {
            $sql = $this->_productQuery . "WHERE p.product_id = $product_id";
            try {
                $product = $db->fetchRow($sql);
            } catch (Zend_Exception $e) {
                echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
                echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
                echo "\n <br/>SQL: " . $sql . "\n <br/>";
            }
        }
        return $product;
    }

    public function fetchProductByAlias($alias) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $this->_productQuery;
        try {
            $sql .= "WHERE c.eshop_id = " . APP_ID . " AND (p.alias_$this->lang = '$alias') GROUP BY p.product_id ";
            $product_data = $db->fetchRow($sql);
            if ($product_data) {
                return new Model_EshopProduct($product_data);
            }    
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
    }   

    public function fetchProductsForXML($category_id_array = null) {
        //Tato metoda získá všechny produkty a jejich varianty dané subkategorie
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $this->_productQuery;
        if ($category_id_array) {
            $sql .= "WHERE (c.category_id IN (" . implode(", ", $category_id_array) . ")) AND (status = '1') ";
        }
        $sql .= "GROUP BY p.product_ID "
                . "ORDER BY sequence, title ";
        try {
            $data = $db->fetchAll($sql);
            return $data;
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
    }

    public function fetchProductsSearch($search) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $products = array();
        //$keyword = "%".preg_replace(' ', '[%]', $search)."%";
        $sql = $this->_productQuery . "WHERE c.eshop_id = " . APP_ID . " AND LOWER (p.title_cz) LIKE LOWER('%$search%') AND (status != '0') AND (status != '3') GROUP BY p.product_id LIMIT 30";
        //pošlu query
        try {
            $result = $db->fetchAll($sql);
            foreach ($result as $data) {
                $products[$data['product_id']] = new Model_EshopProduct($data);
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $products;
    }

    public function fetchProductsByCat($category_id = null, $order = null, $limit = null, $exclude_subcat_id = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $products = array();
        $sql = $this->_productQuery;
        $sql .= "WHERE (c.category_id = $category_id) AND (c.eshop_id = " . APP_ID . ") AND (status != '0') AND (status != '3') ";
        if ($exclude_subcat_id) {
            $sql .= "AND (sp.subcategory_id != $exclude_subcat_id) ";
        }
        $sql .= "GROUP BY p.product_ID ORDER BY sequence, title ";
        //zjistím podle čeho to seřadit
        if ($order) {
            $sql .= "ORDER BY $order";
        }
        if ($limit) {
            $sql .= "LIMIT $limit ";
        }
        try {
            $result = $db->fetchAll($sql);
            foreach ($result as $data) {
                $products[$data['product_id']] = new Model_EshopProduct($data);
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $products;
    }    
    
    public function fetchProductsBySubcat($subcategory_id, $order = null, $limit = null, $exclude_product_id = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $products = array();
        $sql = $this->_productQuery;
        $sql .= "WHERE (sp.subcategory_id = $subcategory_id) AND (c.eshop_id = " . APP_ID . ") AND (status != '0') AND (status != '3') ";
        if ($exclude_product_id) {
            $sql .= "AND (p.product_id != '$exclude_product_id') ";
        }        
        $sql .= "GROUP BY p.product_ID ";
        if ($order) {
            $sql .= "ORDER BY $order ";
        } else {
            $sql .= "ORDER BY sequence, title ";
        }
        if (isset($limit)) {
            $sql .= "LIMIT $limit ";
        }
        try {
            $result = $db->fetchAll($sql);
            foreach ($result as $data) {
                $products[$data['product_id']] = new Model_EshopProduct($data);
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $products;
    }

    public function fetchProductsOfSubcategories($aParams) {
        //Tato metoda získá všechny produkty a jejich varianty dané subkategorie
        $db = Zend_Db_Table::getDefaultAdapter();
        $products = array();
        $sql = $this->_productQuery;
        $sql .= "WHERE (c.eshop_id = " . APP_ID . ") AND (status != '0') AND (status != '3') ";
        if (!empty($aParams['amount_upper']) && !empty($aParams['amount_lower'])) {
            $sql .= "AND p.price_" . APP_LOCALE . " BETWEEN '" . $aParams['amount_lower'] . "' AND '" . $aParams['amount_upper'] . "' ";
        }
        if (empty($aParams)) {
            //nic
        } else {
            foreach ($aParams['subcatIds'] as $subcategory_id) {
                if ($subcategory_id != 'default') {
                    $sql .= "AND p.product_id IN "
                        . "(SELECT esp.product_id FROM eshop_subcat_products AS esp WHERE esp.subcategory_id = '$subcategory_id') ";
                }
            }
        }
        $sql .= "GROUP BY p.product_ID ORDER BY sequence, title ";
        try {
            $result = $db->fetchAll($sql);
            foreach ($result as $data) {
                $products[$data['product_id']] = new Model_EshopProduct($data);
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $products;
    }

    public function saveProduct($data, $subcats, $product_id = null) {
        $relationmodel = new Model_DbTable_SubcatProducts();
        $subcatmodel = new Model_DbTable_EshopSubCategories();
        if (isset($product_id)) {
            $where = $this->getAdapter()->quoteInto('product_id = ?', $product_id);
            $this->update($data, $where);
            //vezmeme celý seznam subkategorií
            $subcategories = $subcatmodel->fetchAll()->toArray();
            //zjistíme, které subkategorie byli již k výrobku přiřazeny
            $prevselected = $relationmodel->fetchProductSubcats($product_id);
            foreach ($subcategories as $value) {
                if ((!in_array($value['subcategory_id'], $prevselected)) && (in_array($value['subcategory_id'], $subcats))) {
                    //subkategorie z postu není na seznamu z databáze, musí se tam přidat
                    $relationmodel->insert(array('subcategory_id' => $value['subcategory_id'], 'product_id' => $product_id));
                } else if ((in_array($value['subcategory_id'], $prevselected)) && (!in_array($value['subcategory_id'], $subcats))) {
                    $where = array();
                    $where[] = $relationmodel->getAdapter()->quoteInto('subcategory_id = ?', $value['subcategory_id']);
                    $where[] = $relationmodel->getAdapter()->quoteInto('product_id = ?', $product_id);
                    $relationmodel->delete($where);
                }
            }
        } else {
            $this->insert($data);
            $db = Zend_Db_Table::getDefaultAdapter();
            $product_id = $db->lastInsertId($this->_name, $this->_primary);
            foreach ($subcats as $value) {
                $relationmodel->insert(array('subcategory_id' => $value, 'product_id' => $product_id));
            }
        }
        return $product_id;
    }
    
    public function fetchProductSales($category_id = null, $date_from = null, $date_to = null, $year = null, $order = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT t1.order_detail_id, t1.product_id, t1.status, t1.title_cz AS title, t1.price_cz AS price, "
                . "SUM(t1.product_price) AS product_price, SUM(t1.product_quantity) AS product_quantity "
                . "FROM ("
                . "SELECT DISTINCT eod.order_detail_id, ep.product_id, ep.status, ep.title_cz, ep.alias_cz, ep.size, ep.price_cz, "
                . "eod.product_price, eod.product_quantity "
                . "FROM eshop_products AS ep "
                . "LEFT JOIN eshop_order_details AS eod ON ep.product_id = eod.product_id "
                . "JOIN eshop_orders AS eo ON eo.order_id = eod.order_id "
                . "LEFT JOIN (eshop_subcat_products AS esp, eshop_subcategories AS es, eshop_categories AS ec) 
                            ON (esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id AND ec.category_id = es.category_id) "
                . "WHERE eo.status_id != 0 ";
        if (isset($category_id) and $category_id != 'all') {
            $sql .= "AND ec.category_id = $category_id ";
        }
        if (isset($year) && $year != 'all') {
            $sql .= "AND eo.order_timestamp >= '$year-01-01 00:00:00' AND eo.order_timestamp <= '$year-12-31 23:59:59' ";
        }
        if (isset($date_from) && isset($date_to)) {
            $sql .= "AND eo.order_timestamp >= '$date_from 00:00:00' AND eo.order_timestamp <= '$date_to 23:59:59' ";
        }
        $sql .= ")AS t1 GROUP BY t1.product_id ";
        if (isset($order)) {
            $sql .= "ORDER BY $order ";
            if ($order == 'product_price' or $order == 'product_quantity') {
                $sql .= "DESC ";
            }
        } else {
            $sql .= "ORDER BY product_price DESC ";
        }
        try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "<p>SQL: $sql </p>";
            echo "<p>Caught exception during saving order details: " . get_class($e) . "</p>";
            echo "<p>Message: " . $e->getMessage() . "</p>";
        }
        return $result;
    }
    
    public function fetchProductsAdmin($category_id = null, $order = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        if (!empty($category_id)) {
            if ($category_id == 'unsorted') {
                $sql = $this->_productQueryAdmin . "WHERE c.category_id IS NULL ";
            } else if ($category_id == 'all') {
                $sql = $this->_productQueryAdmin . "GROUP BY p.product_id ";
            } else {
                $sql = $this->_productQueryAdmin . "WHERE c.category_id = $category_id GROUP BY p.product_id ";
            }
        } else {
            $sql = $this->_productQueryAdmin . "GROUP BY p.product_id ";
        }
        //zjistĂ­m podle ÄŤeho to seĹ™adit
        if (!empty($order)) {
            $sql .= "ORDER BY $order";
        }
        //poĹˇlu query
        try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }
    
    public function fetchProductEdit($product_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $this->_productQueryEdit . "WHERE p.product_id = $product_id GROUP BY p.product_id ";
        try {
            $product = $db->fetchRow($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $product;
    }
       
    public function searchProductsAdmin($search) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $this->_productQueryAdmin . "WHERE LOWER (p.title_cz) LIKE LOWER('%$search%') GROUP BY p.product_id ";
        //pošlu query
        try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }
    
    public function fetchProductsByIds($idArray) {
        $sql = $this->_productQuery . "WHERE p.product_id IN (";
        $i = 0;
        foreach ($idArray as $value) {
            if ($i > 0) {
                $sql .= ", ";
            }
            $sql .= "$value";
            $i++;
        }
        $sql .= ") GROUP BY p.product_id ";
        try {
            $db = Zend_Db_Table::getDefaultAdapter();
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }

}
