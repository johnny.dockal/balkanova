<?php

/**
 * Produkt reprezentuje jakékoli zboží, které se prodává v obchodě.
 * Má své jméno, cenu, kód, poznámku, množství a obrázek.
 * Všechny ceny se zaokrouhlují na celé koruny dolů (halíře se oříznou).
 */
class Model_EshopProduct {

    private $product_id = null;
    private $status = null;
    private $category_id = null;
    private $category_title = null;
    private $category_text = null;
    private $subcategory_id = null;
    private $subcategory_title = null;
    private $subcategory_text = null;
    private $type = null;
    private $code = null;
    private $title = null;
    private $full_title = null;
    private $alias = null;
    private $text = null;
    private $producer = null;
    private $material = null;
    private $manufactured = null;
    private $hair = null;
    private $size = null;
    private $weight = null;
    private $weight_unit = null;
    private $grams = null;
    private $note = null;
    private $new = null;
    private $certified = null;
    private $price = null;
    private $price_unit = null;
    private $quantity = null;
    private $available = null;
    private $inventory = array();
    private $VAT = null;

    public function __construct($data) {
        if (isset($data['product_id'])) {
            $this->product_id = $data['product_id'];
        }
        if (isset($data['status'])) {
            $this->status = $data['status'];
        }
        if (isset($data['category_id'])) {
            $this->category_id = $data['category_id'];
        }
        if (isset($data['category_title'])) {
            $this->category_title = $data['category_title'];
        }
        if (isset($data['category_text'])) {
            $this->category_text = $data['category_text'];
        }
        if (isset($data['subcategory_id'])) {
            $this->subcategory_id = $data['subcategory_id'];
        }
        if (isset($data['subcategory_title'])) {
            $this->subcategory_title = $data['subcategory_title'];
        }
        if (isset($data['subcategory_text'])) {
            $this->subcategory_text = $data['subcategory_text'];
        }
        if (isset($data['type'])) {
            $this->type = $data['type'];
        }
        if (isset($data['note'])) {
            $this->note = $data['note'];
        }
        if (isset($data['new'])) {
            $this->new = $data['new'];
        }
        if (isset($data['code'])) {
            $this->code = $data['code'];
        }
        if (isset($data['title'])) {
            $this->title = $data['title'];
        }
        if (isset($data['alias'])) {
            $this->alias = $data['alias'];
        }
        if (isset($data['full_title'])) {
            $this->full_title = $data['full_title'];
        }
        if (isset($data['text'])) {
            $this->text = $data['text'];
        }
        if (isset($data['producer'])) {
            $this->producer = $data['producer'];
        }
        if (isset($data['size'])) {
            $this->size = $data['size'];
        }
        if (isset($data['weight'])) {
            $this->weight = $data['weight'];
        }
        if (isset($data['weight_unit'])) {
            $this->weight_unit = $data['weight_unit'];
        }
        if (isset($data['grams'])) {
            $this->grams = $data['grams'];
        }
        if (isset($data['material'])) {
            $this->material = $data['material'];
        }
        if (isset($data['manufactured'])) {
            $this->manufactured = $data['manufactured'];
        }
        if (isset($data['hair'])) {
            $this->hair = $data['hair'];
        }
        if (isset($data['price'])) {
            $this->price = $data['price'];
        }
        if (isset($data['price_unit'])) {
            $this->price_unit = $data['price_unit'];
        }
        if (isset($data['quantity'])) {
            $this->quantity = $data['quantity'];
        }
        if (isset($data['available'])) {
            $this->available = $data['available'];
        }
        if (isset($data['VAT'])) {
            $this->VAT = $data['VAT'];
        }
    }

    public function getVAT() {
        if (empty($this->VAT)) {
            $settings = new Model_DbTable_Settings();
            $this->VAT = $settings->getVAT();
        }
        return $this->VAT;
    }

    public function getProductId() {
        return $this->product_id;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getStatusId() {
        return $this->status;
    }

    public function getType() {
        return $this->type;
    }

    public function getCode() {
        return $this->code;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getAlias() {
        return $this->alias;
    }

    public function getFullTitle() {
        if (empty($this->full_title)) {
            return $this->title;
        } else {
            return $this->full_title;
        }
    }

    public function getText() {
        return $this->text;
    }

    public function getNote() {
        return $this->note;
    }

    public function getSize() {
        return $this->size;
    }

    public function getWeight() {
        //průměrná váha lahve vína
        if (empty($this->weight) && APP_ID == 3) {
            $this->weight = 1260;
        }
        return $this->weight;
    }

    public function getWeightUnit() {
        return $this->weight_unit;
    }

    public function getGrams() {
        return $this->grams;
    }

    public function getProducer() {
        return $this->producer;
    }

    public function getMaterial() {
        return $this->material;
    }

    public function getManufactured() {
        return $this->manufactured;
    }

    public function getHair() {
        return $this->hair;
    }
    
    public function getNew() {
        return $this->new;
    }
    
    public function getCertified() {
        return $this->certified;
    }

    public function getViticulture() {
        //id 3 je skupina subkategorií vinařství
        $group_id = 3;
        $subcatids = explode(";", $this->subcategory_id);
        $model = new Model_DbTable_EshopSubCategories();
        $subcats = $model->fetchSubcategoriesOfGroup($group_id, $subcatids);
        $planB = $this->getManufactured();
        if (!empty($subcats)) {
            return $subcats[0];            
        } else if (!empty($planB)) {
            return array('title' => $planB);
        } else {
            return null;
        }
    }
    
    public function getWineType() {
        //id 1 je skupina subkategorií typ vína
        $group_id = 1;
        $subcatids = explode(";", $this->subcategory_id);
        $model = new Model_DbTable_EshopSubCategories();
        $subcats = $model->fetchSubcategoriesOfGroup($group_id, $subcatids);
        if (!empty($subcats)) {
            return $subcats[0];            
        } else {
            return null;
        }
    }

    public function getPrice() {
        //v adminu se při vyplňování prodeje může cena měnit podle zadání prodejce 
        $front = Zend_Controller_Front::getInstance();
        if ($front->getRequest()->getModuleName() == 'admin' && $front->getRequest()->getControllerName() == 'sales') {
            $cart = new Zend_Session_Namespace('Cart');
            $id = $this->getProductId();
            //if (isset($cart->$id->price)) { // WTF ZEND? tímto se nastavila do session NULL!!! Fuck OFF!
            //if (empty($cart->$id->price)) {
            if (empty($_SESSION['Cart'][$id])) {//Pouze toto spolehlivě funguje, aby to nezdělalo session práznými hodnotami
                return $this->price;
            } else { 
                return $cart->$id->price;                
            }
        }
        if (empty($this->price) or $this->price == '0.00') {
            return null;
        } else {
            return $this->price;
        }
    }

    public function getPriceUnit() {
        if (empty($this->price_unit) or $this->price_unit == '0.00') {
            return null;
        } else {
            return $this->price_unit;
        }
    }

    public function getPriceUnitNoVAT() {
        $price = round($this->getPrice() / ($this->VAT / 100 + 1), 2);
        return $price;
    }

    /**
     * Vrátí celkovou cenu za všechny kusy produktu s DPH.
     * @return int
     */
    public function getTotalPrice() {
        return $this->getPrice() * $this->getQuantityInCart();
    }

    /**
     * Vrátí celkovou cenu za všechny kusy produktu bez DPH.
     * @return int
     */
    public function getTotalPriceNoVAT() {
        $price = round($this->getPrice() * $this->getQuantityInCart() / (($this->VAT / 100) + 1), 2);
        return $price;
    }

    /**
     * Vrátí samotné DPH (cena s DPH mínus cena bez DPH).
     * @return int
     */
    public function getDPH() {
        return $this->getPriceUnit() - $this->getPriceBezDPHForUnit();
    }

    /**
     * Vrátí samotné DPH (cena s DPH mínus cena bez DPH) za všechny kusy produktu.
     * @return int
     */
    public function getTotalDPH() {
        return $this->getDPH() * $this->getQuantityInCart();
    }

    public function getQuantityInCart() {
        //pokud je hodnota prázdná, podíváme se do košíku
        if (empty($this->quantity)) {
            $cart = new Zend_Session_Namespace('Cart');
            $id = $this->getProductId();
            //if (isset($cart->$id)) { // Zasírá session null hodnotami
            if (empty($_SESSION['Cart'][$id])) {//Pouze toto spolehlivě funguje, aby to nezdělalo session práznými hodnotami                
                $this->quantity = 0;
            } else {
                $this->quantity = $cart->$id->quantity;
            }
        }
        return $this->quantity;
    }

    public function getImage() {
        $validator = new Zend_Validate_File_Exists();
        $validator->addDirectory('images/eshop_products/');
        if ($validator->isValid($this->product_id . '.jpg')) {
            return "/images/eshop_products/$this->product_id.jpg";
        } else if (!empty($this->subcategory_id)) {
            $subcatidarray = explode(";", $this->subcategory_id);
            return "/images/eshop_subcategories/$subcatidarray[0]-s.jpg";
        } else if (!empty($this->category_id)) {
            return "/images/eshop_products/default-$this->category_id.jpg";
        } else {
            return null;
        }
    }

    public function getImageBig() {
        $validator = new Zend_Validate_File_Exists();
        $validator->addDirectory('images/eshop_products/');
        if ($validator->isValid($this->product_id . '_big.jpg')) {
            return "/images/eshop_products/" . $this->product_id . "_big.jpg";
        } else {
            return null;
        }
    }

    public function getImageDetail() {
        $validator = new Zend_Validate_File_Exists();
        $validator->addDirectory('images/eshop_products/');
        if ($validator->isValid($this->product_id . '_detail.jpg')) {
            return "/images/eshop_products/" . $this->product_id . "_detail.jpg";
        } else {
            return null;
        }
    }

    public function getAllowsale() {
        //nejdřív zkontroluji, zda je správně nastaven status a zda je u toho cena
        if ($this->getStatusId() == '1') {
            $allowSale = true;
        } else {
            $allowSale = false;
        }
        if ($this->getPrice()) {
            //
        } else {
            $allowSale = false;
        }
        //pak se podívám, jestli je k dispozici nenulový počet
        if ($this->getAvailable() > 0) {
            //
        } else {
            $allowSale = false;
        }
        return $allowSale;
    }

    public function getAvailable() {
        $id = $this->getProductId();
        if (isset($this->available)) {
            //
        } else {
            $model = new Model_DbTable_EshopProductHistory();
            $this->available = $model->fetchAvailable($id);
        }
        $available = $this->available - $this->getQuantityInCart();
        //aby se na stránkách nezobrazoval záporný počet produktů
        //k záporné hodnotě může dojít jen při nedokonalém používání inventáře
        if ($available < 0) {
            $available = 0;
        }
        return $available;
    }
    
    public function getInventory() {
        $id = $this->getProductId();
        if (empty($this->inventory)) {
            $model = new Model_DbTable_EshopProductHistory();
            $this->inventory = $model->fetchInventoryNumbers($id);
        }         
        return $this->inventory;
    }

    public function getSizeValue() {
        $temp = str_replace(',', '.', $this->size);
        $temp2 = preg_replace('/[^0-9.]*/', '', $temp);
        return round($temp2, 2);
    }
    
    public function getSubcategoryIds() {
        return $this->subcategory_id;
    }
    
    /* deprecated */
    public function getQuantity() {
        return $this->getQuantityInCart();
    }
    

}

?>
