<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    /**
     * Access control list
     * @var Zend_Auth
     */
    private $_acl = null;

    protected function _initAutoload() {
        /**
         * Nastavení modulu
         * */
        $model_loader = new Zend_Application_Module_Autoloader(array(
                    'namespace' => '', 'basePath' => APPLICATION_PATH . '/'
                ));

        /**
         * Pokud uživatel není přihlášen, nastaví se jako guest.
        */ 
        if (Zend_Auth::getInstance()->hasIdentity()) {
            Zend_Registry::set('user_role', Zend_Auth::getInstance()->getStorage()->read()->user_role);
        } else {
            Zend_Registry::set('user_role', 'guest');
        }

        /**
         * Nastavení přihlašování a práv přístupu.
         */
        $this->_acl = new Model_UserAcl();
        $this->_auth = Zend_Auth::getInstance();

        /**
         * Zaregistrování pluginu, který kontroluje, zda uživatel může na danou stránku dříve, než se stránka načte.
         */
        $fc = Zend_Controller_Front::getInstance();
        $fc->registerPlugin(new Plugin_AccessCheck($this->_acl));
        //$fc->registerPlugin(new Plugin_AdminLoader());

        return $model_loader;
    }

    protected function _initViewHelpers() {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();

        /**
         * Nastavení doctypu
         */
        $view->doctype("XHTML1_STRICT");

        /**
         * Nastavení Meta-tagu
         */
        $view->headMeta()->appendHttpEquiv('Content-type', 'text/html;charset=utf-8')
                ->appendName('description', 'Balkanova')
                ->appendName('keywords', 'balkanova, vlněné deky, bulharské deky')
                ->appendName('author', 'Jan Dočkal');
        
        /**
         * Nastavení hlavičky a oddělovače
         */
        $view->headTitle()->setSeparator(' » ')->headTitle('Balkanova');

        /**
         * Načtení pluginů
         */
        //Zend_Controller_Action_HelperBroker::addHelper(new Plugin_Menu());
        Zend_Controller_Action_HelperBroker::addHelper(new Plugin_LayoutLoader()); 
        //Zend_Controller_Action_HelperBroker::addHelper(new Plugin_AdminLoader()); 
        Zend_Controller_Action_HelperBroker::addHelper(new Plugin_LangSelector()); 
        Zend_Controller_Action_HelperBroker::addHelper(new Plugin_LocaleLoader());
        //Zend_Controller_Action_HelperBroker::addHelper(new Plugin_MenuLoader());
        
        /**
         * Umožníme Zend Dojo zasahovat do view (umožní např. selekci v kalendáři)
         */
        Zend_Dojo::enableView($view);
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        $viewRenderer->setView($view);
        
        /**
         * Zjistíme si, zda je v configu povoleno zobrazování debugu a jazyků a pošleme do view
         */
        $config = Zend_Registry::get('config');
        $view->debugbox     = $config->settings->debug->debugbox;
        $view->languagebox  = $config->settings->debug->languagebox;        
    }
    
}