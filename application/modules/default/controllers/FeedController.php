<?php
class FeedController extends Zend_Controller_Action 
{
	
    function init() {
		$constants=new GetConstants();
		$this->config = $constants->getConfig();
		
		$this->view->config = $this->config;
		$this->view->str = $constants->getLangStr($this->lang);
			
		$this->view->title = $this->view->str->sitemap;
		$this->view->layout()->disableLayout();
    }
    
    function indexAction() 
    {	
		$feed = new Feed();
		$this->view->feed = $feed->productFeed($this->lang);
	}
	
	function seznamAction() 
    {	
		$feed = new Feed();
		$this->view->feed = $feed->productFeed($this->lang);
	}
	
	
	function heurekaAction() 
    {	
		$feed = new Feed();
		$feed_array = $feed->productFeed($this->lang);
		$heureka_feed = $feed_array;
		foreach($feed_array as $key => $tag)
		{		
			if	(preg_match("/Vlněné deky/", $tag["category"])) { $heureka_feed[$key]["category"]="Heureka.cz | Dům a zahrada | Bydlení a doplňky | Doplňky do ložnice | Přikrývky"; }
			if	(preg_match("/Chalište|Kilimy/", $tag["category"])) { $heureka_feed[$key]["category"]="Heureka.cz | Dům a zahrada | Bydlení a doplňky | Koberce a koberečky"; }
		}

		
		$this->view->feed = $heureka_feed;
	}
	
	function googlemerchantsAction() 
    {	
		$feed = new Feed();
		$this->view->feed = $feed->productFeed($this->lang);
	}
	
	function checkworldpriceAction() 
    {	
		$feed = new Feed();
		$this->view->feed = $feed->productFeed($this->lang, $separator=";");
	}

}
?>