<?php
class Admin_PaymentsController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
    }

    function indexAction() {  
        $model                  = new Model_DbTable_EshopPayments();
        $settings               = $model->fetchAll()->toArray();
        $form                   = new Form_Paymentform($settings);
        $this->view->form       = $form;
    }
    
    function saveAction() {
        $model                  = new Model_DbTable_EshopPayments();
        $count                  = $this->getParam('count');
        for ($i = 1; $i <= $count; $i++) {
            $payment_id = $this->getParam($i.'payment_id');
            $data = array(
                'text_cz'     => trim($this->getParam($i.'text_cz')),
                'text_en'     => trim($this->getParam($i.'text_en')),
                'text_de'     => trim($this->getParam($i.'text_de')),
                'public'      => $this->getParam($i.'public'),
                'wait'        => $this->getParam($i.'wait'),
                'price_cz'    => $this->getParam($i.'price_cz'),
                'price_de'    => $this->getParam($i.'price_de'),
                'price_percent' => $this->getParam($i.'price_percent')
            );   
            $where = $model->getAdapter()->quoteInto('payment_id = ?', $payment_id);
            $model->update($data, $where);
        } 
        $this->_redirect('/admin/shipping/payments/');
    }
}
?>
