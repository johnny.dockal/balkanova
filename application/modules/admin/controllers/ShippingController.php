<?php

class Admin_ShippingController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $this->shippingAction();
        $this->render('shipping');
    }
    
    //shipping
    public function shippingAction() {
        $model = new Model_DbTable_EshopShipping();
        $this->view->shipping = $model->fetchShipping();
        $this->view->form = new Form_Shippingform();
    }

    public function shippingeditAction() {
        $shipping_id = $this->getParam('shipping_id');        
        $form = new Form_Shippingform();
        $model = new Model_DbTable_EshopShipping();        
        $values = $model->fetchRow("shipping_id = '$shipping_id'")->toArray();
        $form->populate($values);
        $this->view->form = $form;
    }

    public function shippingsaveAction() {
        $model = new Model_DbTable_EshopShipping();
        $shipping_id = $this->getParam('shipping_id');

        $data = array(
            'title_cz' => $this->getParam('title_cz'),
            'text_cz' => $this->getParam('text_cz'),
            'title_en' => $this->getParam('title_en'),
            'text_en' => $this->getParam('text_en'),
            'title_de' => $this->getParam('title_de'),
            'text_de' => $this->getParam('text_de'),
            'public' => $this->getParam('public')
        );
        if (!empty($shipping_id)) {
            $where = $model->getAdapter()->quoteInto('shipping_id = ?', $shipping_id);
            $model->update($data, $where);
        } else {
            $model->insert($data);
        }
        $this->_redirect("/admin/shipping/");
    }

    //payments
    function paymentsAction() {
        $model = new Model_DbTable_EshopPayments();
        $this->view->payments = $model->fetchAll()->toArray();
        $this->view->form = new Form_Paymentsform();
    }
    
    public function paymentseditAction() {
        $payment_id = $this->getParam('payment_id');        
        $form = new Form_Paymentsform();
        $model = new Model_DbTable_EshopPayments();        
        $values = $model->fetchRow("payment_id = '$payment_id'")->toArray();
        $form->populate($values);
        $this->view->form = $form;
    }

    public function paymentssaveAction() {
        $model = new Model_DbTable_EshopPayments();
        $payment_id = $this->getParam('payment_id');

        $data = array(
            'title_cz' => $this->getParam('title_cz'),
            'text_cz' => $this->getParam('text_cz'),
            'title_en' => $this->getParam('title_en'),
            'text_en' => $this->getParam('text_en'),
            'title_de' => $this->getParam('title_de'),
            'text_de' => $this->getParam('text_de'),            
            'price_cz' => $this->getParam('price_cz'),
            'price_de' => $this->getParam('price_de'),
            'price_percent' => $this->getParam('price_percent'),
            'public' => $this->getParam('public')
        );
        if (!empty($payment_id)) {
            $where = $model->getAdapter()->quoteInto('payment_id = ?', $payment_id);
            $model->update($data, $where);
        } else {
            $model->insert($data);
        }
        $this->_redirect("/admin/shipping/payments");
    }

    function countriesAction() {
        $paymentsModel = new Model_DbTable_EshopPayments();
        $shippingModel = new Model_DbTable_EshopShipping();
        $ratesModel = new Model_DbTable_EshopShippingRates();
        $db = Zend_Db_Table::getDefaultAdapter();
        $addedId = $this->_getParam('addedId');
        $sql = "SELECT  eshop_countries.country_id, 
                        eshop_countries.title_cz, 
                        
                        eshop_deliveries.title_cz AS delivery, 
                        
                        eshop_country_delivery_payment.id,
                        eshop_country_delivery_payment.price_set_id,
                        eshop_country_delivery_payment.public,
                        
                        eshop_payments.title_cz AS payment,
                        eshop_payments.price_cz,
                        eshop_payments.price_de
                        
                FROM eshop_countries
                JOIN eshop_country_delivery_payment 
                    ON eshop_countries.country_id = eshop_country_delivery_payment.country_id
                JOIN eshop_deliveries
                    ON eshop_deliveries.shipping_id = eshop_country_delivery_payment.shipping_id      
                JOIN eshop_payments 
                    ON eshop_payments.payment_id = eshop_country_delivery_payment.payment_id
                ORDER BY eshop_countries.country_id, eshop_deliveries.title_cz";
        try {
            $stmt = $db->query($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception during saving order details: " . get_class($e) . "\n <br/>";
            echo "Message: " . $e->getMessage() . "\n <br/>";
            echo "SQL: " . $sql . "\n <br/>";
        }
        $countries_data = $stmt->fetchAll();
        //tohle je pro země, které se v prvním výběru nezobrazují, protože nemají zadané kombinace plateb a doručování
        $sql = "SELECT  eshop_countries.country_id, 
                        eshop_countries.title_cz, 
                        eshop_country_delivery_payment.shipping_id
                FROM eshop_countries
                LEFT JOIN eshop_country_delivery_payment ON eshop_countries.country_id = eshop_country_delivery_payment.country_id
                WHERE eshop_country_delivery_payment.shipping_id IS NULL
                ORDER BY eshop_countries.country_id";
        try {
            $stmt = $db->query($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception during saving order details: " . get_class($e) . "\n <br/>";
            echo "Message: " . $e->getMessage() . "\n <br/>";
            echo "SQL: " . $sql . "\n <br/>";
        }
        $countries_extra_data = $stmt->fetchAll();

        $this->view->addedId = $addedId;
        $this->view->countries_data = $countries_data;
        $this->view->countries_extra_data = $countries_extra_data;
        $this->view->payments_data = $paymentsModel->fetchPayments();
        $this->view->shipping_data = $shippingModel->fetchShipping();
        $this->view->rates_data = $ratesModel->fetchTarifs();
    }

    public function deactivateAction() {
        $id = $this->_getParam('combinationId');
        $sql = "UPDATE eshop_country_delivery_payment SET public = '0' WHERE id = '$id'";
        $db = Zend_Db_Table::getDefaultAdapter();
        $stmt = $db->query($sql);
        $this->_redirect("/admin/shipping/countries/");
    }

    public function ratesAction() {
        $ratesModel = new Model_DbTable_EshopShippingRates();
        $tarifs = $ratesModel->fetchTarifs();
        $db = Zend_Db_Table::getDefaultAdapter();
        $i = 0;
        $delivery_data = array();
        $payment_data = array();
        $tarif_data = array();
        $countries_data = array();
        foreach ($tarifs as $value) {
            $countries_data[$value['price_set_id']]['price_set_id'] = $value['price_set_id'];
            $sql = "SELECT eshop_countries.title_cz
                        FROM eshop_countries
                        JOIN eshop_country_delivery_payment 
                            ON eshop_countries.country_id = eshop_country_delivery_payment.country_id
                        WHERE eshop_country_delivery_payment.price_set_id = " . $value['price_set_id'] . "
                            GROUP BY eshop_countries.country_id";
            try {
                $values = $db->fetchAll($sql);
            } catch (Zend_Exception $e) {
                echo "Caught exception during saving order details: " . get_class($e) . "\n <br/>";
                echo "Message: " . $e->getMessage() . "\n <br/>";
                echo "SQL: " . $sql . "\n <br/>";
            }

            $countries = array();
            foreach ($values as $v) {
                array_push($countries, $v['title_cz']);
            }
            $countries_data[$value['price_set_id']]['countries'] = $countries;

            $sql = "SELECT 
                        eshop_deliveries.shipping_id,                     
                        eshop_deliveries.title_cz,            
                        eshop_deliveries_prices_sets.id AS id, 
                        eshop_deliveries_prices_sets.price_set_id, 
                        eshop_deliveries_prices_sets.weight_from, 
                        eshop_deliveries_prices_sets.weight_till, 
                        eshop_deliveries_prices_sets.price_cz, 
                        eshop_deliveries_prices_sets.price_de
                    FROM eshop_deliveries 
                    JOIN eshop_deliveries_prices_sets 
                        ON eshop_deliveries.shipping_id = eshop_deliveries_prices_sets.shipping_id
                    WHERE eshop_deliveries_prices_sets.price_set_id = " . $value['price_set_id'];
            try {
                $values = $db->fetchAll($sql);
            } catch (Zend_Exception $e) {
                echo "Caught exception during saving order details: " . get_class($e) . "\n <br/>";
                echo "Message: " . $e->getMessage() . "\n <br/>";
                echo "SQL: " . $sql . "\n <br/>";
            }
            $countries_data[$value['price_set_id']]['prices'] = $values;
        }
        $shippingModel = new Model_DbTable_EshopShipping();
        $this->view->countries_data = $countries_data;
        $this->view->delivery_data = $shippingModel->fetchAll();
    }

    public function saveAction() {
        $id = $this->_getParam('id');
        $price_cz = $this->_getParam('price_cz');
        $price_de = $this->_getParam('price_de');
        $title = $this->_getParam('title');
        $description = $this->_getParam('description');
        $redirect = $this->_getParam('redirect');
        $save = $this->_getParam('save');

        $db = Zend_Db_Table::getDefaultAdapter();
        if ($save == 'rate') {
            $sql = "UPDATE eshop_deliveries_prices_sets SET price_cz = '$price_cz', price_de= '$price_de' WHERE id = '$id'";
            $stmt = $db->query($sql);
        } else if ($save == 'delivery') {
            $sql = "UPDATE eshop_deliveries_text_values SET title = '$title', description = '$description' WHERE id = '$id'";
            $stmt = $db->query($sql);
        }
        $stmt = $db->query($sql);
        $this->_redirect("/admin/shipping/$redirect");
    }

    public function savecountriesAction() {
        $country_id = $this->_getParam('country_id');
        $payment_id = $this->_getParam('payment_id');
        $shipping_id = $this->_getParam('shipping_id');
        $price_set_id = $this->_getParam('price_set_id');
        $db = Zend_Db_Table::getDefaultAdapter();
        if (isset($country_id) && isset($payment_id) && isset($shipping_id) && isset($price_set_id)) {
            $id = $this->_getParam('combinationId');
            $sql = "INSERT INTO eshop_country_delivery_payment VALUES ('', '$country_id', '$shipping_id', '$price_set_id', '$payment_id', '1')";
            $stmt = $db->query($sql);
            $result = $db->fetchRow("SELECT MAX(id) FROM eshop_country_delivery_payment");
            $this->_redirect("/admin/shipping/countries/?addedId=" . $result['MAX(id)']);
        } else {
            echo "Některý z parametrů nebyl nastaven.";
        }
    }

    public function deleterateAction() {
        $model = new Model_DbTable_EshopShippingRates();
        $rate_id = $this->getParam('id');
        $model->delete("id = '$rate_id'");
        $this->_redirect("/admin/shipping/rates/");
    }

    public function saverateAction() {
        $model = new Model_DbTable_EshopShippingRates();
        $data = array(
            'price_set_id' => $this->getParam('price_set_id'),
            'shipping_id' => $this->getParam('shipping_id'),
            'weight_from' => $this->getParam('weight_from'),
            'weight_till' => $this->getParam('weight_till'),
            'price_cz' => $this->getParam('price_cz'),
            'price_de' => $this->getParam('price_de')
        );
        $model->insert($data);
        $this->_redirect("/admin/shipping/rates/");
    }
}
?>
