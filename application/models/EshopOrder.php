<?php

class Model_EshopOrder {

    private $session = null;
    private $cart = null;
    private $lang = null;
    private $token = null;
    private $cartSum = null;
    private $cartProducts = null;
    private $orderProducts = null;
    private $eshop_id = null;
    private $order_id = null;
    private $invoice_no = null;
    private $status_id = null;
    private $country_id = null;
    private $country_name = null;
    private $payment_id = null;
    private $payment_price = null;
    private $payment_count = null;
    private $payment_title = null;
    private $payment_wait = null;
    private $shipping_id = null;
    private $shipping_title = null;
    private $shipping_count = null;
    private $shipping_price = null;
    private $shipping_address = null;
    private $order_timestamp = null;
    private $order_lang = null;
    private $order_diff = 0;
    private $order_name = null;
    private $order_diff_name = null;
    private $order_surname = null;
    private $order_diff_surname = null;
    private $order_address = null;
    private $order_diff_address = null;
    private $order_city = null;
    private $order_diff_city = null;
    private $order_zip = null;
    private $order_diff_zip = null;
    private $order_company = 0;
    private $order_company_name = null;
    private $order_company_id = null;
    private $order_phone = null;
    private $order_email = null;
    private $order_message = '';
    private $order_VAT = null;
    private $order_agree = 0;
    private $order_adult = 0;
    private $user_id = 0;
    private $order_weight = 0;
    private $order_sendmail = 0;
    public $payment_break_point = 800;

    function __construct($order_id = null) {
        $this->cart = new Zend_Session_Namespace('Cart');
        $this->session = new Zend_Session_Namespace('Default');
        $this->lang = $this->session->lang;
        if (isset($order_id)) {
            $this->fetchOrder($order_id);
        }
    }

    public function checkNullProducts() {
        if (!empty($_SESSION['Cart'])) {
            foreach ($_SESSION['Cart'] as $key => $product) {
                if (isset($product) && $product->quantity > 0) {

                } else {
                    unset($this->cart->$key);
                    $this->session->sum = $this->getCartSum();
                }
            }
        }    
    }

    public function getCartSum() {
        $count = 0;
        if (!empty($_SESSION['Cart'])) {
            foreach ($_SESSION['Cart'] as $product) {
                if (isset($product)) {
                    $count = $count + $product->quantity;
                } 
            }
        }
        return $count;
    }

    public function formParametersToArray() {
        $orderArray = array(
            'country_id' => $this->country_id,
            'country_name' => $this->country_name,
            'payment_id' => $this->payment_id,
            'payment_title' => $this->payment_title,
            'payment_count' => $this->payment_count,
            'payment_price' => $this->payment_price,
            'shipping_id' => $this->shipping_id,
            'shipping_title' => $this->shipping_title,
            'shipping_count' => $this->shipping_count,
            'shipping_price' => $this->shipping_price,
            'shipping_address' => $this->shipping_address,
            'order_diff' => $this->order_diff,
            'order_name' => $this->order_name,
            'order_diff_name' => $this->order_diff_name,
            'order_surname' => $this->order_surname,
            'order_diff_surname' => $this->order_diff_surname,
            'order_phone' => $this->order_phone,
            'order_email' => $this->order_email,
            'order_address' => $this->order_address,
            'order_diff_address' => $this->order_diff_address,
            'order_city' => $this->order_city,
            'order_diff_city' => $this->order_diff_city,
            'order_zip' => $this->order_zip,
            'order_diff_zip' => $this->order_diff_zip,
            'order_company' => $this->order_company,
            'order_company_name' => $this->order_company_name,
            'order_company_id' => $this->order_company_id,
            'order_message' => $this->order_message,
            'order_vat' => $this->getVAT(),
            'order_sendmail' => $this->order_sendmail,
            'order_agree' => $this->order_agree
        );
        return $orderArray;
    }

    public function checkConfirmationSent() {
        $history = new Model_DbTable_EshopOrderHistory();
        $sent = false;
        foreach ($history->fetchOrderHistory($this->getOrderId()) as $value) {
            if ($value['status_id'] < -1) {
                $sent = true;
            }
        }
        return $sent;
    }

    public function checkFormParameters($step = 1) {
        $view = Zend_Layout::getMvcInstance()->getView();
        $errors = array();
        $return_step = $step;
        switch ($step) {
            case '4':
                if (empty($this->payment_id)) {
                    $return_step = 3;
                    $errors['err_fill_order'] = $view->err_fill_order;
                }
            case '3':
                if (($this->shipping_id == '17') and empty($this->shipping_address)) {
                    $errors['err_fill_address1'] = $view->err_fill_address1;
                    $return_step = '2z';
                }
                if (empty($this->shipping_id)) {
                    $return_step = 2;
                    $errors['err_fill_shipping'] = $view->err_fill_shipping;
                }
            case '2':
                if (empty($this->order_name)) {
                    $return_step = 1;
                    $errors['err_fill_name'] = $view->err_fill_name;
                }
                if (empty($this->order_surname)) {
                    $return_step = 1;
                    $errors['err_fill_surname'] = $view->err_fill_surname;
                }
                if (empty($this->order_address)) {
                    $return_step = 1;
                    $errors['err_fill_address2'] = $view->err_fill_address2;
                }
                if (empty($this->order_city)) {
                    $return_step = 1;
                    $errors['err_fill_city'] = $view->err_fill_city;
                }
                if ((empty($this->order_zip) or ! is_numeric($this->order_zip))) {
                    $return_step = 1;
                    $errors['err_fill_zip'] = $view->err_fill_zip;
                }
                if ($this->order_diff && empty($this->order_diff_name)) {
                    $return_step = 1;
                    $errors['err_fill_diff_name'] = $view->err_fill_name;
                }
                if ($this->order_diff && empty($this->order_diff_surname)) {
                    $return_step = 1;
                    $errors['err_fill_diff_surname'] = $view->err_fill_surname;
                }
                if ($this->order_diff && empty($this->order_diff_address)) {
                    $return_step = 1;
                    $errors['err_fill_diff_address'] = $view->err_fill_address2;
                }
                if ($this->order_diff && empty($this->order_diff_city)) {
                    $return_step = 1;
                    $errors['err_fill_diff_city'] = $view->err_fill_city;
                }
                if ($this->order_diff && (empty($this->order_diff_zip) or ! is_numeric($this->order_zip))) {
                    $return_step = 1;
                    $errors['err_fill_diff_zip'] = $view->err_fill_zip;
                }
                if ($this->order_company && empty($this->order_company_name)) {
                    $return_step = 1;
                    $errors['err_fill_company_name'] = $view->err_fill_company_name;
                }
                if ($this->order_company && (empty($this->order_company_id) or ! is_numeric($this->order_company_id))) {
                    $return_step = 1;
                    $errors['err_fill_company_id'] = $view->err_fill_company_id;
                }
                if ((empty($this->order_phone) or ! is_numeric($this->order_phone))) {
                    $return_step = 1;
                    $errors['err_fill_phone'] = $view->err_fill_phone;
                }
                if (!filter_var($this->order_email, FILTER_VALIDATE_EMAIL)) {
                    $return_step = 1;
                    $errors['err_fill_email'] = $view->err_fill_email;
                }
                if (empty($this->country_id)) {
                    $return_step = 1;
                    $errors['err_fill_country'] = $view->err_fill_country;
                }
                if (empty($this->order_agree)) {
                    $return_step = 1;
                    $errors['err_fill_agree'] = $view->err_fill_agree;
                }
                if (APP_NAME == 'pivoklub') {
                    if (empty($this->order_adult)) {
                        $return_step = 1;
                        $errors['err_fill_adult'] = "Pokud nepotvrdíte, že jste již plnoletí, nemůžeme vám nic prodat.";
                    }
                }
                break;
            case '2z':
                if ($this->shipping_id == '17') {
                    $return_step = '3z';
                } else {
                    $return_step = 3;
                }
                break;
            case '1':
                break;
            default :
                $return_step = 1;
                break;
        }
        $view->validation_errors = $errors;
        return $return_step;
    }

    public function fetchOrderByToken($token) {
        if (empty($token)) {
            return null;
        } else {
            $model = new Model_DbTable_EshopOrders();
            $result = $model->getOrderByToken($token); //->toArray();            
            $order_id = $result[0]['order_id'];
            //orderProducts musí být dříve než setOrderParameters, protože se tam již propočítávají ceny
            $this->orderProducts = $this->fetchOrderProducts($order_id);
            $this->setOrderParameters($result);
            return 1;
        }
    }

    public function fetchOrder($order_id) {
        if (empty($order_id)) {
            return null;
        }
        try {
            //tady musí být produkty první, dřív než se v setOrderparameters počítá celková cena objednávky
            $this->orderProducts = $this->fetchOrderProducts($order_id);
            $model = new Model_DbTable_EshopOrders();
            $result = $model->find($order_id)->toArray();
            $this->setOrderParameters($result);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
        }
        return 1;
    }

    private function fetchOrderProducts($order_id) {
        if (empty($order_id)) {
            return null;
        }
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT od.order_detail_id,
                        o.order_id,
                        od.product_id,
                        od.product_title AS title, "
                //. "od.product_producer AS producer, "
                . "od.product_code AS code, "
                . "od.product_size AS size, "
                . "od.product_price AS price, "
                . "od.product_quantity AS quantity                        
                FROM eshop_order_details AS od 
                JOIN eshop_orders AS o ON o.order_id = od.order_id
                WHERE o.order_id = '$order_id'";
        try {
            $result = $db->fetchAll($sql);
            $orderProducts = array();
            foreach ($result as $value) {
                $data = array(
                    'product_id' => $value['product_id'],
                    'title' => $value['title'],
                    //'producer' => $value['producer'],
                    'code' => $value['code'],
                    'price' => $value['price'],
                    'quantity' => $value['quantity'],
                    'size' => $value['size'],
                    'VAT' => $this->getVAT());
                array_push($orderProducts, new Model_EshopProduct($data));
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $orderProducts;
    }

    public function fetchCartProducts($clean = false) {
        $session = new Zend_Session_Namespace('Default');
        $lang = $session->lang;
        $subcat_lang = $session->lang;
        if ($lang == 'de') {
            $subcat_lang = 'cz';
        }
        $i = 0;
        $sql = null;
        if ($clean) {
            $this->checkNullProducts();
        }
        if (empty($_SESSION['Cart'])) {
            return null;
        }         
        foreach ($_SESSION['Cart'] as $key => $value) {
            if ($i > 0) {
                $sql .= "UNION ";
            } else {
                $i++;
            }
            $sql .= "SELECT   p.product_id, "
                    . "p.status, "
                    . "p.code, "
                    . "p.sequence,                                                           
                                p.alias_$lang AS alias,
                                p.title_$lang AS title,                                    
                                p.full_title_$lang AS full_title,
                                p.text_$lang AS text,                                
                                p.note_$lang AS note,                                 
                                p.material_$lang AS material,                           
                                p.manufactured_$lang AS manufactured,                   
                                p.price_unit_" . APP_LOCALE . " AS price_unit,                     
                                p.price_" . APP_LOCALE . " AS price, 
                                p.size, 
                                p.weight,
                                p.weight_unit,
                                p.grams,
                                GROUP_CONCAT(s.subcategory_id SEPARATOR ';') AS subcategory_id,
                                GROUP_CONCAT(s.title_$subcat_lang SEPARATOR ';') AS subcategory_title,                                    
                                c.category_id AS category_id,
                                c.title_$subcat_lang AS category_title,
                                c.text_$subcat_lang AS category_text
                    FROM eshop_products AS p JOIN (eshop_subcat_products AS esp, eshop_subcategories AS s, eshop_categories AS c) 
                        ON (esp.product_id = p.product_id AND s.subcategory_id = esp.subcategory_id AND c.category_id = s.category_id) 
                    WHERE p.product_id = '$key' ";
        }
        if (empty($sql)) {
            return null;
        }
        try {
            $db = Zend_Db_Table::getDefaultAdapter();
            $result = $db->fetchAssoc($sql);
            //musíme array ze sessiony projet znovu a do výsledku z databáze přidat kvantitu 
            $cartProducts = array();
            foreach ($result as $key => $value) {
                if (isset($this->cart->$key->size)) {
                    $size = $this->cart->$key->size;
                } else {
                    $size = $value['size'];
                }
                if (isset($this->cart->$key->quantity)) {
                    $quantity = $this->cart->$key->quantity;
                } else {
                    $quantity = 0;
                }
                $data = array(
                    'product_id' => $value['product_id'],
                    'status' => $value['status'],
                    'code' => $value['code'],
                    'sequence' => $value['sequence'],
                    'alias' => $value['alias'],
                    'title' => $value['title'],
                    'full_title' => $value['full_title'],
                    'text' => $value['text'],
                    'note' => $value['note'],
                    'material' => $value['material'],
                    'manufactured' => $value['manufactured'],
                    'price_unit' => $value['price_unit'],
                    'price' => $value['price'],
                    'size' => $value['size'],
                    'weight' => $value['weight'],
                    'weight_unit' => $value['weight_unit'],
                    'grams' => $value['grams'],
                    'category_id' => $value['category_id'],
                    'subcategory_id' => $value['subcategory_id'],
                    'subcategory_title' => $value['subcategory_title'],
                    'quantity' => $quantity,
                    'VAT' => $this->getVAT());
                array_push($cartProducts, new Model_EshopProduct($data));
            }
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $cartProducts;
    }

    private function setOrderParameters($result) {
        $this->eshop_id = $result[0]['order_id'];
        $this->order_id = $result[0]['order_id'];
        $this->invoice_no = $result[0]['invoice_no'];
        $this->status_id = $result[0]['status_id'];
        $this->country_id = $result[0]['country_id'];
        $this->country_name = $this->getCountryName($this->country_id);
        $this->shipping_id = $result[0]['shipping_id'];
        $this->shipping_title = $this->getShippingTitle($this->shipping_id);
        $this->shipping_count = $result[0]['shipping_count'];
        $this->shipping_price = $result[0]['shipping_price'];
        $this->shipping_address = $result[0]['shipping_address'];
        $this->payment_id = $result[0]['payment_id'];
        $this->payment_title = $this->getPaymentTitle($this->payment_id);
        $this->payment_count = $result[0]['payment_count'];
        $this->payment_price = $result[0]['payment_price'];
        $this->order_timestamp = $result[0]['order_timestamp'];
        $this->order_lang = $result[0]['order_lang'];
        $this->order_diff = $result[0]['order_diff'];
        $this->order_name = $result[0]['order_name'];
        $this->order_diff_name = $result[0]['order_diff_name'];
        $this->order_surname = $result[0]['order_surname'];
        $this->order_diff_surname = $result[0]['order_diff_surname'];
        $this->order_address = $result[0]['order_address'];
        $this->order_diff_address = $result[0]['order_diff_address'];
        $this->order_city = $result[0]['order_city'];
        $this->order_diff_city = $result[0]['order_diff_city'];
        $this->order_zip = $result[0]['order_zip'];
        $this->order_diff_zip = $result[0]['order_diff_zip'];
        $this->order_company = $result[0]['order_company'];
        $this->order_company_name = $result[0]['order_company_name'];
        $this->order_company_id = $result[0]['order_company_id'];
        $this->order_phone = $result[0]['order_phone'];
        $this->order_email = $result[0]['order_email'];
        $this->order_VAT = $result[0]['order_VAT'];
        $this->order_message = '' . $result[0]['order_message'];
    }

    public function setFormParameters($formParams) {
        $orderSession = new Zend_Session_Namespace('Order');
        $this->country_id = isset($formParams['country_id']) ? $formParams['country_id'] : $orderSession->country_id;
        $this->country_name = $this->getCountryName($this->country_id);
        $this->shipping_id = isset($formParams['shipping_id']) ? $formParams['shipping_id'] : $orderSession->shipping_id;
        $this->shipping_title = (empty($this->shipping_id)) ? "" : $this->getShippingTitle($this->shipping_id);
        $this->shipping_count = (empty($this->shipping_id)) ? "" : $this->getShippingCount();
        $this->shipping_price = (empty($this->shipping_id)) ? "" : $this->getShippingPrice($this->shipping_id);
        $this->shipping_address = isset($formParams['shipping_address']) ? $formParams['shipping_address'] : $orderSession->shipping_address;
        $this->payment_id = isset($formParams['payment_id']) ? $formParams['payment_id'] : $orderSession->payment_id;
        $this->payment_title = (empty($this->payment_id)) ? "" : $this->getPaymentTitle($this->payment_id);
        $this->payment_count = (empty($this->payment_id)) ? "" : $this->getPaymentCount();
        $this->payment_price = (empty($this->payment_id)) ? "" : $this->getPaymentPrice($this->payment_id);
        $this->order_diff = isset($formParams['order_diff']) ? $formParams['order_diff'] : $orderSession->order_diff;
        $this->order_name = isset($formParams['order_name']) ? trim($formParams['order_name']) : $orderSession->order_name;
        $this->order_diff_name = isset($formParams['order_diff_name']) ? trim($formParams['order_diff_name']) : $orderSession->order_diff_name;
        $this->order_surname = isset($formParams['order_surname']) ? trim($formParams['order_surname']) : $orderSession->order_surname;
        $this->order_diff_surname = isset($formParams['order_diff_surname']) ? trim($formParams['order_diff_surname']) : $orderSession->order_diff_surname;
        $this->order_address = isset($formParams['order_address']) ? trim($formParams['order_address']) : $orderSession->order_address;
        $this->order_diff_address = isset($formParams['order_diff_address']) ? trim($formParams['order_diff_address']) : $orderSession->order_diff_address;
        $this->order_city = isset($formParams['order_city']) ? trim($formParams['order_city']) : $orderSession->order_city;
        $this->order_diff_city = isset($formParams['order_diff_city']) ? trim($formParams['order_diff_city']) : $orderSession->order_diff_city;
        $this->order_zip = isset($formParams['order_zip']) ? trim($formParams['order_zip']) : $orderSession->order_zip;
        $this->order_diff_zip = isset($formParams['order_diff_zip']) ? trim($formParams['order_diff_zip']) : $orderSession->order_diff_zip;
        $this->order_company = isset($formParams['order_company']) ? $formParams['order_company'] : $orderSession->order_company;
        $this->order_company_name = isset($formParams['order_company_name']) ? trim($formParams['order_company_name']) : $orderSession->order_company_name;
        $this->order_company_id = isset($formParams['order_company_id']) ? trim($formParams['order_company_id']) : $orderSession->order_company_id;
        $this->order_phone = isset($formParams['order_phone']) ? trim($formParams['order_phone']) : $orderSession->order_phone;
        $this->order_email = isset($formParams['order_email']) ? trim($formParams['order_email']) : $orderSession->order_email;
        $this->order_message = isset($formParams['order_message']) ? trim($formParams['order_message']) : $orderSession->order_message;
        $this->order_agree = isset($formParams['order_agree']) ? $formParams['order_agree'] : $orderSession->order_agree;
        $this->order_adult = isset($formParams['order_adult']) ? $formParams['order_adult'] : $orderSession->order_adult;
        $this->user_id = isset($formParams['user_id']) ? $formParams['user_id'] : null;

        //$orderSession->setExpirationSeconds(600);
        $orderSession->country_id = $this->country_id;
        $orderSession->shipping_id = $this->shipping_id;
        $orderSession->shipping_address = $this->getShippingAddress();
        $orderSession->payment_id = $this->payment_id;
        $orderSession->order_diff = $this->order_diff;
        $orderSession->order_name = $this->order_name;
        $orderSession->order_diff_name = $this->order_diff_name;
        $orderSession->order_surname = $this->order_surname;
        $orderSession->order_diff_surname = $this->order_diff_surname;
        $orderSession->order_address = $this->order_address;
        $orderSession->order_diff_address = $this->order_diff_address;
        $orderSession->order_city = $this->order_city;
        $orderSession->order_diff_city = $this->order_diff_city;
        $orderSession->order_zip = $this->order_zip;
        $orderSession->order_diff_zip = $this->order_diff_zip;
        $orderSession->order_company = $this->order_company;
        $orderSession->order_company_name = $this->order_company_name;
        $orderSession->order_company_id = $this->order_company_id;
        $orderSession->order_phone = $this->order_phone;
        $orderSession->order_email = $this->order_email;
        $orderSession->order_message = $this->order_message;
        $orderSession->order_agree = $this->order_agree;
        $orderSession->order_adult = $this->order_adult;
        $orderSession->user_id = $this->user_id;
    }

    public function saveOrder($status_id = null) {
        $token = md5(session_id() . microtime());
        if (isset($status_id)) {
            $operation = 'sale';
        } else if ($this->getPaymentWait() == 1) {
            $status_id = '1'; // přijata a čeká platbu
            $operation = 'order';
        } else {
            $status_id = '3'; // přijata
            $operation = 'order';
        }
        //8 je pro prodej na krámě
        if (isset($status_id) && $status_id == 8) {
            $invoice_no = null;
        } else {
            $invoice_no = $this->createInvoiceNumber();
        }
        $data = array(
            'status_id' => $status_id,
            'eshop_id' => APP_ID,
            'invoice_no' => $invoice_no,
            'country_id' => $this->getCountryId(),
            'payment_id' => $this->getPaymentId(),
            'payment_count' => $this->getPaymentCount(),
            'payment_price' => $this->getPaymentPrice(),
            'shipping_id' => $this->getShippingId(),
            'shipping_count' => $this->getShippingCount(),
            'shipping_price' => $this->getShippingPrice(),
            'shipping_address' => $this->getShippingAddress(),
            'user_id' => $this->getUserId(),
            'order_lang' => $this->session->lang,
            'order_diff' => $this->getDiff(),
            'order_name' => $this->getName(),
            'order_diff_name' => $this->getDiffName(),
            'order_surname' => $this->getSurName(),
            'order_diff_name' => $this->getDiffSurName(),
            'order_address' => $this->getAddress(),
            'order_diff_address' => $this->getDiffAddress(),
            'order_city' => $this->getCity(),
            'order_diff_city' => $this->getDiffCity(),
            'order_zip' => $this->getZip(),
            'order_diff_zip' => $this->getDiffZip(),
            'order_company' => $this->getCompany(),
            'order_company_name' => $this->getCompanyName(),
            'order_company_id' => $this->getCompanyId(),
            'order_phone' => $this->getPhone(),
            'order_email' => $this->getEmail(),
            'order_sendmail' => 0,
            'order_message' => $this->getMessage(),
            'order_vat' => $this->getVAT(),
            'token' => $token
        );
        $model = new Model_DbTable_EshopOrders();
        $this->order_id = $model->saveOrder($data);
        $this->token = $token;
        $this->cartProducts = $this->getCartProducts();

        $detailsModel = new Model_DbTable_EshopOrderDetails();
        //Pokud jde o testovací objednávku, neukládá se historie do inventáře
        if ($this->getName() == 'TEST' && $this->getSurName() == 'TEST') {
            $detailsModel->saveOrderDetails($this->order_id, $this->cartProducts, $operation, false);
        } else {
            $detailsModel->saveOrderDetails($this->order_id, $this->cartProducts, $operation);
        }
        $historyModel = new Model_DbTable_EshopOrderHistory();
        $historyModel->saveHistory($this->order_id, $status_id);

        return 1;
    }

    public function dispatchOrder($order_id = null) {
        if (empty($order_id)) {
            $order_id = $this->getOrderId();
        }
        if (empty($this->orderProducts)) {
            $this->orderProducts = $this->fetchOrderProducts($order_id);
        }
        $productHistory = new Model_DbTable_EshopProductHistory();
        foreach ($this->orderProducts as $product) {
            $productHistory->updateInventory($product->getProductId(), 'dispatch', $product->getQuantityInCart(), $order_id);
        }
    }

    public function handoverOrder($order_id = null) {
        if (empty($order_id)) {
            $order_id = $this->getOrderId();
        }
        if (empty($this->orderProducts)) {
            $this->orderProducts = $this->fetchOrderProducts($order_id);
        }
        $productHistory = new Model_DbTable_EshopProductHistory();
        foreach ($this->orderProducts as $product) {
            $productHistory->updateInventory($product->getProductId(), 'handover', $product->getQuantityInCart(), $order_id);
        }
    }

    public function cancelOrder($order_id = null) {
        if (empty($order_id)) {
            $order_id = $this->getOrderId();
        }
        if (empty($this->orderProducts)) {
            $this->orderProducts = $this->fetchOrderProducts($order_id);
        }
        $productHistory = new Model_DbTable_EshopProductHistory();
        foreach ($this->orderProducts as $product) {
            $productHistory->updateInventory($product->getProductId(), 'cancel', $product->getQuantityInCart(), $order_id);
        }
    }

    public function cancelShippedOrder($order_id = null) {
        if (empty($order_id)) {
            $order_id = $this->getOrderId();
        }
        if (empty($this->orderProducts)) {
            $this->orderProducts = $this->fetchOrderProducts($order_id);
        }
        $productHistory = new Model_DbTable_EshopProductHistory();
        foreach ($this->orderProducts as $product) {
            $productHistory->updateInventory($product->getProductId(), 'cancelshipped', $product->getQuantityInCart(), $order_id);
        }
    }

    public function getOrderLang() {
        //pokud není jazyk již uložen v objednávce načtené z databáze,
        //tak se vezme z aktuální session
        if (empty($this->order_lang)) {
            $session = new Zend_Session_Namespace('Default');
            return $session->lang;
        } else {
            return $this->order_lang;
        }
    }

    public function getCartProducts() {
        if (empty($this->cartProducts)) {
            $this->cartProducts = $this->fetchCartProducts();
        }
        return $this->cartProducts;
    }

    //tabulka s objednanými věcmi
    public function getProductTable() {
        $view = Zend_Layout::getMvcInstance()->getView();
        $langselector = new Plugin_LangSelector();
        $langselector->setStrings($this->getOrderLang());
        $productTable = "<h2>$view->str_order_details</h2>"
                . "<table width='560'>"
                . "<tr><td>ID</td><td>$view->str_product</td><td>$view->str_items</td><td>$view->str_price</td><td>$view->str_sum</td></tr>";
        $totalPrice = 0;
        $products = $this->getOrderProducts();
        if (empty($products)) {
            $products = $this->getCartProducts();
        }
        foreach ($products as $product) {
            $productTable .= "<tr><td>" . $product->getProductId() . "</td>" .
                    "<td>" . $product->getTitle() . "</td>" .
                    "<td align='right'>" . $product->getQuantityInCart() . "</td>" .
                    "<td align='right'>" . $view->currency->toCurrency($product->getPrice()) . "</td>" .
                    "<td align='right'>" . $view->currency->toCurrency($product->getTotalPrice()) . "</td></tr>";
            $totalPrice += $product->getTotalPrice();
        }
        $shippingPrice = $this->getShippingPrice();
        if ($shippingPrice > 0) {
            $productTable .= "<tr><td></td>"
                    . "<td>$view->str_shipping1</td>"
                    . "<td align='right'>" . $this->getShippingCount() . "</td>"
                    . "<td align='right'>" . $view->currency->toCurrency($this->getShippingPrice()) . "</td>"
                    . "<td align='right'>" . $view->currency->toCurrency($this->getShippingTotal()) . "</td></tr>";
            $totalPrice += $this->getShippingTotal();
        }
        $paymentPrice = $this->getPaymentPrice();
        if ($paymentPrice > 0) {
            $productTable .= "<tr><td></td>"
                    . "<td>$view->str_payment1</td>"
                    . "<td align='right'>" . $this->getPaymentCount() . "</td>"
                    . "<td align='right'>" . $view->currency->toCurrency($this->getPaymentPrice()) . "</td>"
                    . "<td align='right'>" . $view->currency->toCurrency($this->getPaymentTotal()) . "</td></tr>";
            $totalPrice += $this->getPaymentTotal();
        }
        $productTable .= "<tr><td></td><td>$view->str_order_total_price:</td>" .
                "<td></td><td></td><td align='right'>" . $view->currency->toCurrency($totalPrice) . "</td></tr>";
        $productTable .= "</table>";
        $productTable .= "$view->str_order_VATincluded";

        return $productTable;
    }

    public function getCountryId() {
        return $this->country_id;
    }

    public function getCountryName($country_id = null) {
        if (empty($country_id)) {
            $country_id = $this->country_id;
        }
        if (!empty($country_id)) {
            $model = new Model_DbTable_EshopCountries();
            $result = $model->find($country_id);
            foreach ($result as $value) {
                $title = $value->title_cz;
            }
            return $title;
        } else {
            return null;
        }
    }

    public function getCountryNameLocale($country_id = null) {
        if (empty($country_id)) {
            $country_id = $this->country_id;
        }
        if (!empty($country_id)) {
            $model = new Model_DbTable_EshopCountries();
            $result = $model->find($country_id);
            $lang = $this->getOrderLocale();
            $title = 'title_' . $lang;
            foreach ($result as $value) {
                $title = $value->$title;
            }
            return $title;
        } else {
            return null;
        }
    }

    public function getToken() {
        return $this->token;
    }

    public function getOrderId() {
        return $this->order_id;
    }

    public function getName() {
        return $this->order_name;
    }

    public function getFullName() {
        return $this->order_name . " " . $this->order_surname;
    }

    public function getSurName() {
        return $this->order_surname;
    }

    public function getAddress() {
        return $this->order_address;
    }

    public function getCity() {
        return $this->order_city;
    }

    public function getZip() {
        return $this->order_zip;
    }

    public function getDiffName() {
        return $this->order_diff_name;
    }

    public function getDiffFullName() {
        if ($this->getDiff()) {
            return $this->order_diff_name . " " . $this->order_diff_surname;
        } else {
            return null;
        }
    }

    public function getDiffSurName() {
        if ($this->getDiff()) {
            return $this->order_diff_surname;
        } else {
            return null;
        }
    }

    public function getDiff() {
        return $this->order_diff;
    }

    public function getDiffAddress() {
        if ($this->getDiff()) {
            return $this->order_diff_address;
        } else {
            return null;
        }
    }

    public function getDiffCity() {
        if ($this->getDiff()) {
            return $this->order_diff_city;
        } else {
            return null;
        }
    }

    public function getDiffZip() {
        if ($this->getDiff()) {
            return $this->order_diff_zip;
        } else {
            return null;
        }
    }

    public function getCompany() {
        return $this->order_company;
    }

    public function getCompanyName() {
        if ($this->getCompany()) {
            return $this->order_company_name;
        } else {
            return null;
        }
    }

    public function getCompanyId() {
        if ($this->getCompany()) {
            return $this->order_company_id;
        } else {
            return null;
        }
    }

    public function getPhone() {
        return $this->order_phone;
    }

    public function getEmail() {
        return $this->order_email;
    }

    public function getMessage() {
        return "" . $this->order_message;
    }

    public function getTimestamp() {
        return $this->order_timestamp;
    }

    public function getOrderProducts() {
        return $this->orderProducts;
    }

    public function getVAT() {
        if (empty($this->order_VAT)) {
            $settings = new Model_DbTable_Settings();
            $this->order_VAT = $settings->getVAT();
        }
        return $this->order_VAT;
    }

    public function getInvoiceNumber() {
        return $this->invoice_no;
    }

    public function createInvoiceNumber() {
        $model = new Model_DbTable_EshopOrders();
        $prevNumber = $model->getPrevInvoiceNumber();
        $prevId = substr($prevNumber, -4);
        $newId = $prevId + 1;
        $newNumber = date("ymd") . $newId;
        return $newNumber;
    }

    public function getOrderDate() {
        return date('d. m. Y', strtotime($this->order_timestamp));
    }

    public function getDueDate() {
        $day = date("d", strtotime($this->order_timestamp));
        $month = date("m", strtotime($this->order_timestamp));
        $year = date("Y", strtotime($this->order_timestamp));
        return date('d. m. Y', mktime(0, 0, 0, $month + 1, $day, $year));
    }

    public function getStatusId() {
        return $this->status_id;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function getStatusTitle() {
        $model = new Model_DbTable_EshopOrderStatus();
        return $model->fetchTitle($this->getStatusId());
    }

    public function getNextStatus($status_id = null, $shipping_id = null) {
        if (!isset($status_id)) {
            $status_id = $this->getStatusId();
        }
        if (!isset($shipping_id)) {
            $shipping_id = $this->getShippingId();
        }
        switch ($status_id) {
            case 1:
                $nextstatus = 2;
                break;
            case 2:
            case 3:
                $nextstatus = 4;
                break;
            case 4:
                if ($shipping_id == 1) {
                    $nextstatus = 6;
                } else {
                    $nextstatus = 5;
                }
                break;
            case 5:
            case 6:
                $nextstatus = 7;
                break;
            case 7:
            case 8:
            case 0:
                $nextstatus = null;
        }
        return $nextstatus;
    }

    /* shipping metody */

    public function getShippingId() {
        return $this->shipping_id;
    }

    public function getShippingAddress() {
        $string1 = str_replace("[", "", $this->shipping_address);
        $string2 = str_replace("]", "", $string1);
        return $string2;
    }

    public function getShippingTitle($shipping_id = null) {
        if (!empty($this->shipping_title)) {
            return $this->shipping_title;
        } else {
            if (empty($shipping_id)) {
                $shipping_id = $this->shipping_id;
            }
            $model = new Model_DbTable_EshopShipping();
            $result = $model->find($shipping_id);
            foreach ($result as $value) {
                $this->shipping_title = $value['title_' . $this->lang];
            }
            return $this->shipping_title;
        }
    }

    public function getShippingTitleLang($shipping_id = null) {
        if (empty($shipping_id)) {
            $shipping_id = $this->shipping_id;
        }
        $model = new Model_DbTable_EshopShipping();
        $result = $model->find($shipping_id);
        foreach ($result as $value) {
            $shipping_title = $value['title_' . $this->getOrderLang()];
        }
        return $shipping_title;
    }

    public function getShippingPrice($shipping_id = null, $country_id = null, $weight = null) {
        if (isset($this->shipping_price)) {
            return $this->shipping_price;
        } else {
            if (empty($shipping_id)) {
                $shipping_id = $this->getShippingId();
            }
            if (empty($country_id)) {
                $country_id = $this->getCountryId();
            }
            if (empty($weight)) {
                $weight = $this->getOrderWeight();
            }
            if (empty($shipping_id) or empty($country_id)) {
                return null;
            } else {
                $model = new Model_DbTable_EshopShipping();
                $result = $model->fetchShipping($country_id, $weight, $shipping_id);
                //ochcávka - převádí se to kurzem namísto vlastní ceny pro německé stránky
                if (APP_LOCALE == 'de') {
                    $view = Zend_Layout::getMvcInstance()->getView();
                    $price = $view->currency->exchangeToEuroNumber($result['price']);
                } else {
                    $price = $result['price'];
                }
                $this->shipping_title = $result['title'];
                $this->shipping_price = $price;
                return $this->shipping_price;
            }
        }
    }

    public function getShippingPriceNoVAT($shipping_id = null) {
        $price = $this->getShippingPrice($shipping_id);
        return round($price / ($this->getVAT() / 100 + 1), 2);
    }

    public function getShippingTotal($shipping_id = null) {
        return $this->getShippingPrice($shipping_id) * $this->getShippingCount();
    }

    /* vrátí množství balíčků, do kterého se rozdělí zásilka */

    public function getShippingCount() {
        if (APP_ID == 3) {
            $cart = new Model_EshopCart();
            $count = $cart->countProducts();
            $shipping_count = $count / 6;
            $this->shipping_count = ceil($shipping_count);
        } else {
            $this->shipping_count = 1;
        }
        return $this->shipping_count;
        /*
          if (empty($this->cartProducts)) {
          $this->cartProducts = $this->fetchCartProducts();
          if (empty($this->cartProducts)) {
          return null;
          }
          }
          $glass = 0;
          $weight = 0;
          $count_weight = 1;
          $count_glass = 1;
          foreach ($this->cartProducts as $product) {
          switch ($product->getType()) {
          case 'lahváč':
          $glass += $product->getQuantityInCart();
          $weight += $product->getQuantityInCart() * $product->getSizeValue() * 1.9;
          break;
          case 'sklo' :
          $glass += $product->getQuantityInCart();
          $weight += $product->getQuantityInCart() * $product->getSizeValue() * 0.9;
          break;
          case 'sypký materiál' :
          $size = $product->getQuantityInCart();
          $weight += $product->getQuantityInCart() * $product->getSizeValue();
          break;
          case 'jiné' :
          $size = $product->getSizeValue();
          $weight += $product->getQuantityInCart() * $product->getSizeValue();
          break;
          }
          }
          if ($weight > 20) {
          $count_weight = ceil($weight / 20);
          }
          if ($glass > 12) {
          $count_glass = ceil($glass / 12);
          }
          $this->shipping_count = max(array($count_weight, $count_glass));

          $view = Zend_Layout::getMvcInstance()->getView();
          $view->shipping_weight = $weight;
          $view->shipping_glass = $glass;

          return $this->shipping_count; */
    }

    public function getShippingTotalNoVAT($shipping_id = null) {
        $total = $this->getShippingPrice($shipping_id) * $this->getShippingCount();
        return round($total / ($this->getVAT() / 100 + 1), 2);
    }

    public function getPaymentId() {
        return $this->payment_id;
    }

    public function getPaymentTitle($payment_id = null) {
        if (!empty($this->payment_title)) {
            return $this->payment_title;
        } else {
            if (empty($payment_id)) {
                $payment_id = $this->payment_id;
            }
            $model = new Model_DbTable_EshopPayments();
            $result = $model->find($payment_id);
            $this->setPaymentPrice($result);
            return $this->payment_title;
        }
    }

    public function getPaymentTitleLang($payment_id = null) {
        if (empty($payment_id)) {
            $payment_id = $this->payment_id;
        }
        $model = new Model_DbTable_EshopPayments();
        $result = $model->find($payment_id);
        foreach ($result as $value) {
            $payment_title = $value['title_' . $this->getOrderLang()];
        }
        return $payment_title;
    }

    public function getPaymentWait($payment_id = null) {
        if (isset($this->payment_wait)) {
            return $this->payment_wait;
        } else {
            if (empty($payment_id)) {
                $payment_id = $this->payment_id;
            }
            $model = new Model_DbTable_EshopPayments();
            $result = $model->find($payment_id);
            foreach ($result as $value) {
                $this->payment_title = $value['title_' . $this->lang];
                $this->payment_price = $value['price_' . APP_LOCALE];
                $this->payment_wait = $value['wait'];
            }
            return $this->payment_wait;
        }
    }

    public function getPaymentPrice($payment_id = null) {
        if (isset($this->payment_price)) {
            return $this->payment_price;
        } else {
            if (empty($payment_id)) {
                $payment_id = $this->payment_id;
            }
            $model = new Model_DbTable_EshopPayments();
            $result = $model->find($payment_id);
            $session = new Zend_Session_Namespace('Default');
            if (APP_LOCALE == 'de') {
                $view = Zend_Layout::getMvcInstance()->getView();
                if (isset($result['price'])) {
                    $price = $view->currency->exchangeToEuroNumber($result['price']);
                    $this->setPaymentPrice($price);
                }
            } else {
                if (isset($result['price'])) {
                    $price = $result['price'];
                    $this->setPaymentPrice($price);
                }
            }

            return $this->payment_price;
        }
    }

    private function setPaymentPrice($result) {
        foreach ($result as $value) {
            $this->payment_title = $value['title_' . $this->lang];
            if (!empty($value['price_percent'])) {
                $percent = ($this->getOrderTotal(false) / 100) * $value['price_percent'];
                $percent = round($percent, 0, PHP_ROUND_HALF_UP);
                $this->payment_price = $percent;
            } else {
                $this->payment_price = $value['price_' . APP_LOCALE];
            }
            $this->payment_wait = $value['wait'];

            break;
        }
    }

    public function getPaymentPriceNoVAT() {
        $price = $this->getPaymentPrice();
        return round($price / ($this->getVAT() / 100 + 1), 2);
    }

    public function getPaymentTotal() {
        return $this->getPaymentPrice() * $this->getPaymentCount();
    }

    public function getPaymentTotalNoVAT() {
        $total = $this->getPaymentPrice() * $this->getPaymentCount();
        return round($total / ($this->getVAT() / 100 + 1), 2);
    }

    public function getPaymentCount() {
        if (!empty($this->payment_count)) {
            return $this->payment_count;
        } else {
            if ($this->getPaymentId() == 4) {
                $this->payment_count = $this->getShippingCount();
            } else {
                $this->payment_count = 1;
            }
            return $this->payment_count;
        }
    }

    public function getOrderWeight() {
        $grams = 0;
        $products = $this->getCartProducts();
        foreach ($products as $value) {
            $grams = $grams + $value->getQuantityInCart() * $value->getGrams();
        }
        //převedeme gramy na kilogramy
        $kilograms = $grams / 1000;
        return $kilograms;
    }

    public function getOrderTotal($payment = true) {
        $total = 0;
        $products = $this->getOrderProducts();
        if (empty($products)) {
            $products = $this->getCartProducts();
        }
        foreach ($products as $value) {
            $total += $value->getTotalPrice();
        }
        $total += $this->getShippingTotal();
        if ($payment) {
            $total += $this->getPaymentTotal();
        }
        return $total;
    }

    public function getOrderTotalNoVat() {
        $total = 0;
        foreach ($this->getOrderProducts() as $value) {
            $total += $value->getTotalPrice();
        }
        $total += $this->getShippingTotal();
        $total += $this->getPaymentTotal();
        return round($total / ($this->getVAT() / 100 + 1), 2);
    }

    public function getOrderTotalEuro() {
        $total = $this->getOrderTotal();
        $settings = new Model_DbTable_Settings();
        $rate = $settings->getExchangeRate();
        return round($total / $rate, 1);
    }

    public function getOrderTotalNoVatEuro() {
        $total = $this->getOrderTotalNoVat();
        $settings = new Model_DbTable_Settings();
        $rate = $settings->getExchangeRate();
        return round($total / $rate, 1);
    }

    private function checkNumber($input, $float = false) {
        if ($float) {
            $temp = str_replace(',', '.', $input);
            $temp2 = preg_replace('/[^0-9.]*/', '', $temp);
            $input = round($temp2, 2);
        } else {
            $temp = str_replace(',', '.', $input);
            $temp2 = preg_replace('/[^0-9.]*/', '', $temp);
            $input = ceil($temp2);
        }
        if ($input > 100) {
            $input = 100;
        } else if ($input < 0) {
            $input = 0;
        }
        return $input;
    }

    /* deprecated */

    public function getDeliveryId() {
        return $this->getShippingId();
    }

    public function getDeliveryTitle($shipping_id = null) {
        return $this->getShippingTitle($shipping_id);
    }

    public function getDeliveryPrice($shipping_id = null) {
        return $this->getShippingPrice($shipping_id);
    }

    public function getDeliveryTotal($shipping_id = null) {
        return $this->getShippingTotal($shipping_id);
    }

    public function getDeliveryCount() {
        return $this->getShippingCount();
    }

    public function getOrderLocale() {
        if ($this->eshop_id == 2) {
            return 'de';
        } else {
            return 'cz';
        }
    }

    public function addProduct($product_id, $quantity = 1, $size = null, $price = null) {
        if (isset($this->cart->$product_id)) {
            $quantity = $this->cart->$product_id->quantity + $this->checkNumber($quantity);
        }
        $this->updateProduct($product_id, $quantity, $size, $price);
    }

    public function updateProduct($product_id, $quantity = 1, $size = null, $price = null) {
        $obj = new stdClass();
        $obj->quantity = $quantity;
        $obj->size = $size;
        $obj->price = $price;

        $this->cart->$product_id = $obj;
        $this->session->sum = $this->getCartSum();
    }

}
