<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_Calendar extends Zend_Db_Table_Abstract {

    protected $_name = 'calendar';
    protected $_primary = 'calendar_date';

    public function fetchCalenderDates() {
        $query = $this->select();
        $defaultSession = new Zend_Session_Namespace('Default');
        if ($defaultSession->lang == 'en') {
            $query->from('concerts', array('calendar_date', 'title_en AS title', 'text_en AS text'));
        } else if ($defaultSession->lang == 'de') {
            $query->from('concerts', array('calendar_date', 'title_de AS title', 'text_de AS text'));
        } else if ($defaultSession->lang == 'ru') {
            $query->from('concerts', array('calendar_date', 'title_ru AS title', 'text_ru AS text'));
        } else {
            $query->from('concerts', array('calendar_date', 'title_cz AS title', 'text_cz AS text'));
        }
        $result = $this->fetchAll($query);

        $textarray  = $result->toArray();
        for ($i = 0; $i < count($textarray); $i++) {
            $newkey             = $textarray[$i]['calendar_date'];
            $textarray[$newkey] = $textarray[$i];
            unset($textarray[$i]);
        }
        return $textarray;
    }
    
    public function fetchDateDetails($date) {
        $select = $this->select()->from('calendar')->where('calendar_date = ?', $date);
        $result = $this->fetchRow($select);
        $concert = array();
        $concert['concert_date'] = $result->calendar_date;
        $concert['title_cz']     = $result->title_cz;
        $concert['text_cz']      = $result->text_cz;
        $concert['title_en']     = $result->title_en;
        $concert['text_en']      = $result->text_en;
        $concert['title_ru']     = $result->title_ru;
        $concert['text_ru']      = $result->text_ru;
        $concert['title_de']     = $result->title_de;
        $concert['text_de']      = $result->text_de;
        return $concert;
    }
    /*
    public function fetchTodaysConcert() {      
        $defaultSession = new Zend_Session_Namespace('Default');
        if ($defaultSession->lang == 'en') {
            $query = $this->select()->from('concerts', array('concert_date', 'band_id', 'title_en AS title', 'text_en AS text', 'www'))->where('concert_date = CURDATE()');
        } else if ($defaultSession->lang == 'ru') {
            $query = $this->select()->from('concerts', array('concert_date', 'band_id', 'title_ru AS title', 'text_ru AS text', 'www'))->where('concert_date = CURDATE()');
        } else {
            $query = $this->select()->from('concerts', array('concert_date', 'band_id', 'title_cz AS title', 'text_cz AS text', 'www'))->where('concert_date = CURDATE()');
        }
        $result = $this->fetchRow($query);
        
        return $result;
    }*/
    
    public function deleteDate($date) {
        $where = $this->getAdapter()->quoteInto('calendar_date = ?', $date);
        $this->delete($where);
    }
}