<?php

class Default_SitemapController extends Zend_Controller_Action {

    function init() {
        $this->view->title = "";
    }

    function indexAction() {
        //$this->view->breadcrumbs_url = array("sitemap");
        //$this->view->breadcrumbs_title = array($this->view->str->sitemap);
        $sitemap = new Model_Sitemap();
        $this->view->sitemap = $sitemap->getSitemap();
    }

    function xmlAction() {
        //$this->_helper->_layout->setLayout('xml');
        $this->view->layout()->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(TRUE);
        $sitemap = new Sitemap();
        $this->view->sitemap = $sitemap->getXmlSitemap($this->lang);
        /* echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
          <urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">"; */
    }

}

?>