<?php
class Admin_OrdersController extends Zend_Controller_Action {

    private $adminSession = null;

    public function init() {
        $this->adminSession = new Zend_Session_Namespace('Admin');
        
        if (!isset($this->adminSession->eshop_id)) {
            $this->adminSession->eshop_id = 0;
        }
        if (!isset($this->adminSession->year)) {
            $this->adminSession->year = date('Y');
        }
        if (!isset($this->adminSession->filter)) {
            $this->adminSession->filter = array(
                '0' => null,
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => null,
                '8' => null
            );
        }
    }

    public function preDispatch() {
        $status = new Model_DbTable_OrderStatus();
        $statusArray = $status->fetchAll('status_id >= 0 AND status_id < 8')->toArray();

        $this->view->status = $statusArray;
        $this->view->eshop_id = $this->adminSession->eshop_id;
        $this->view->year = $this->adminSession->year;
        $this->view->filter = $this->adminSession->filter;
    }

    public function indexAction() {
        $model = new Model_DbTable_EshopOrders();
        $this->view->orders = $model->fetchOrders($this->adminSession->year, $this->adminSession->filter, $this->adminSession->eshop_id);
    }

    public function filterAction() {
        $this->adminSession->eshop_id = $this->getParam('eshop_id');
        $this->adminSession->year = $this->getParam('year');
        $this->adminSession->filter = array(
            '0' => $this->getParam('0'),
            '1' => $this->getParam('1'),
            '2' => $this->getParam('2'),
            '3' => $this->getParam('3'),
            '4' => $this->getParam('4'),
            '5' => $this->getParam('5'),
            '6' => $this->getParam('6'),
            '7' => $this->getParam('7'),
            '8' => $this->getParam('8')
        );
        $this->_redirect('/admin/orders/');
    }

    public function editAction() {
        $order_id = $this->getParam('order_id');
        $order = new Model_EshopOrder($order_id);
        $this->view->order = $order;
        $history = new Model_DbTable_EshopOrderHistory();
        $this->view->orderHistory = $history->fetchOrderHistory($order_id);
        $status = new Model_DbTable_OrderStatus();
        $statuses = $status->fetchAll('status_id >= 0 AND status_id < 8')->toArray();      
        unset($statuses['1']);
        unset($statuses['3']);
        $status_id = $order->getStatusId();
        switch ($status_id) {
            case '0':
                $statuses = null;
                break;
            case '2':
            case '3':
                unset($statuses['2']);
            case '1':
                unset($statuses['5']);
                unset($statuses['6']);
                unset($statuses['7']);
                break;
            case '4':
                unset($statuses['2']);
                unset($statuses['4']);
                if ($order->getShippingId() == '1') {
                    unset($statuses['5']);
                } else {
                    unset($statuses['6']);                    
                }
                unset($statuses['7']);
                break;                
            case '5':
            case '6':                
                unset($statuses['2']);
                unset($statuses['4']);
                unset($statuses['5']);
                unset($statuses['6']);
                break;
            case '7':
                unset($statuses['2']); 
                unset($statuses['4']);
                unset($statuses['5']);
                unset($statuses['6']);
                unset($statuses['7']);
                break;
        }
        $this->view->status = $statuses;
    }

    public function invoiceAction() {
        $tisk = $this->_getParam('tisk');
        $this->view->tisk = $tisk;

        $order_id = $this->_getParam('order_id');
        $order = new Model_EshopOrder($order_id);
        $this->view->order = $order;
        
        $settings = new Model_DbTable_Settings();
        $this->view->exchange = $settings->getExchangeRate();
        
        if (isset($tisk)) {
           $this->view->layout()->disableLayout();   
           if ($tisk == 'a5') {
               $this->render('invoicea5');
           }
        }
    }
    
    function printa4Action() {
        $order_id = $this->_getParam('order_id');
        $this->_redirect("/admin/orders/invoice/?order_id=$order_id&tisk=a4");
    }
    
    function printa5Action() {
        $order_id = $this->_getParam('order_id');
        $this->_redirect("/admin/orders/invoice/?order_id=$order_id&tisk=a5");
    }

    public function statusAction() {
        $order_id = $this->getParam('order_id');
        $status_id = $this->getParam('status_id');
        $mailaction = $this->getParam('mailaction');
        $redirect = $this->getParam('redirect');

        $model = new Model_DbTable_EshopOrders();
        $modelOrder = new Model_EshopOrder($order_id);
        switch ($status_id) {
            //týká se odesílání mailů
            case -1:
                $model->changeStatus($order_id, $status_id);
                $model->changeStatus($order_id, abs($status_id));
                break;
            case 0:
                $model->changeStatus($order_id, $status_id);
                //pokud se objednávka ruší před expedováním (status menší než 5)
                if ($modelOrder->getShippingId() > 1 && $modelOrder->getStatusId() > 4) {
                    echo "<p>1 cancelshipped</p>";
                    $modelOrder->cancelShippedOrder($order_id);
                } else if ($modelOrder->getShippingId() == 1 && $modelOrder->getStatusId() == 7) {
                    echo "<p>2 cancelshipped</p>";
                    $modelOrder->cancelShippedOrder($order_id);
                } else {
                    echo "<p>cancel</p>";
                    $modelOrder->cancelOrder($order_id);
                }
                break;
            case 2:
            case 3:
                $model->changeStatus($order_id, $status_id);
                break;
            //pokud se jedná o změnu statusu na 4 nebo 5, je třeba odeslat mail o připravené či odeslané zásilce
            case 5:
            case 6:
                if ($mailaction == 'sent') {
                    $model->changeStatus($order_id, '-2');
                    $model->changeStatus($order_id, $status_id);
                } else if ($mailaction == 'skip') {
                    $model->changeStatus($order_id, $status_id);
                } else {
                    $this->_redirect('/admin/mail/confirm/?order_id=' . $order_id . '&status_id=' . $status_id);
                }
                //pokud objednávka není k vyzvednutí osobně, tak se produkty odečtou z inventáře
                if ($modelOrder->getShippingId() != 1) {
                    $modelOrder->dispatchOrder($order_id);
                }
                break;
            //pokud je objednávka vyřízena, musí se upravit počty v inventáři    
            case 7: 
                //pokud je objednávka k vyzvednutí, odečte se z inventáře až tehdy, když je uzavřena
                if ($modelOrder->getShippingId() == 1) {
                    $modelOrder->handoverOrder($order_id);
                }
            default:                
                $model->changeStatus($order_id, $status_id);
                break;
        }
        //echo $status_id; 
        if (!empty($redirect)) {
            $this->_redirect('/admin/' . $redirect . '?order_id=' . $order_id);
        } else {
            $this->_redirect('/admin/orders/');
        }
    }

}
