<?php
/**
 * Plugin načte layout pro aktuální modul.
 * @author Daniel Vála
 */
class Plugin_MenuLoader extends Zend_Controller_Action_Helper_Abstract {

    public function init() {
        $module                 = $this->getRequest()->getModuleName();
        $config                 = new Zend_Config_Ini(APPLICATION_PATH . '/configs/menuitems.ini', $module);
        //nahodíme view, abychom do něj mohli poslat pole s položkami menu
        $view = Zend_Layout::getMvcInstance()->getView();
        $view->menuitems   = $config->toArray();
    }
}
