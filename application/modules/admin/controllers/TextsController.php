<?php

class Admin_TextsController extends Zend_Controller_Action {
    
    private $adminSession = null;

    public function init() {
        $this->adminSession = new Zend_Session_Namespace('Admin');        
        if ($this->getParam('eshop_id')) {            
            $this->adminSession->eshop_id = $this->getParam('eshop_id');
        } else {
            $this->adminSession->eshop_id = 0;
        }
    }

    public function indexAction() {
        $model = new Model_DbTable_Texts();
        if (!empty($this->adminSession->eshop_id)) {
            $where = "eshop_id = '".$this->adminSession->eshop_id."'";
        } else {
            $where = null;
        }
        $this->view->texts = $model->fetchAll($where, 'text_name');        
        $this->view->form = new Form_EshopSelectform();
    }

    public function editAction() {          
        $text_id = $this->_getParam('text_id');        
        $model = new Model_DbTable_Texts();
        $text       = $model->find($text_id)->toArray();
        //nasolíme text do připraveného formuláře (pro češtinu a angličtinu)
        $form       = new Form_TextformCZEN('/admin/texts/save/');
        $form->populate($text[0]);
        $this->view->form = $form;
    }

    public function saveAction() {
        $table = new Model_DbTable_Texts();
        $table->updateText();        
        $this->_redirect('/admin/texts/');
    }
}

