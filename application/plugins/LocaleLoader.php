<?php

/**
 * Plugin zjištuje požadavky na změnu jazyka webu v parametru.
 * Pokud odpovídá českému nebo anglickému jazyku, nastaví ji do Zend session.
 * @author Daniel Vála
 */
class Plugin_LocaleLoader extends Zend_Controller_Action_Helper_Abstract {

    public function init() {   
        $currency = new Model_Currency(APP_LOCALE);
        $view = Zend_Layout::getMvcInstance()->getView();
        $view->currency = $currency;
    }

}
