<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Mailinglistform extends Zend_Form {

    public function __construct($action) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('class', 'admintable');
        
        $address = new Zend_Form_Element_Text('address', array('style' => "width: 220px;"));
        $address->setLabel('Adresa :')->setRequired(true);
        
        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'style' => "width: 225px; margin-left: 270px; "));

        $this->addElements(array(
            $address, $submit
        ));
    }
}
