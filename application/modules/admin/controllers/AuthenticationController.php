<?php

/**
 * Authentication controller pro přihlášení (a odhlášení) do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Admin_AuthenticationController extends Zend_Controller_Action {

    /**
     * Přihlášení se do administrace zadáním jména a hesla.
     */
    public function loginAction() {
        $this->view->headTitle('Přihlašování', 'POSTEND');
        $form = new Form_Login();
        $request = $this->getRequest();
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $auth_adapter = $this->getAuthAdapter();
                $username = md5($form->getValue('username'));
                $password = md5($form->getValue('password'));
                $auth_adapter->setIdentity($username)->setCredential($password);
                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($auth_adapter);
                if ($result->isValid()) {
                    $identity = $auth_adapter->getResultRowObject();
                    $auth_storage = $auth->getStorage();
                    $auth_storage->write($identity);
                    $this->_redirect('/admin/orders/');
                } else {
                    $this->view->errormessage = "";
                }
            }
        }
        $this->view->form = $form;
    }

    /**
     * Získání adaptéru s daty pro přihlášení (z databáze).
     * @return Zend_Auth_Adapter_DbTable
     */
    private function getAuthAdapter() {
        $auth_adapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $auth_adapter->setTableName('users')->setIdentityColumn('user_login_hashed')->setCredentialColumn('user_pswd_hashed');
        return $auth_adapter;
    }

    /**
     * Odhlášení a přesměrování na login.
     */
    public function logoutAction() {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_redirect('admin/authentication/login');
    }

}
