<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopOrderDetails extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_order_details';
    protected $_primary = 'order_detail_id';

    public function saveOrderDetails($order_id, $data, $operation = 'order', $savehistory = true) {
        $productHistory = new Model_DbTable_EshopProductHistory();
        foreach ($data as $product) {
            $size = $product->getSize();
            if (strpos($size, ';')) {
                $sizes = explode(';', $size);    
                $id = $product->getProductId();                 
                $index = $_SESSION['Cart'][$id]->size;
                $size = $sizes[$index];
            } 
            if (empty($size)) {
                $size = '0';
            }
            try {
                $this->insert(array(
                    'order_id' => $order_id,
                    'product_id' => $product->getProductId(),
                    'product_title' => $product->getTitle(),
                    'product_code' => $product->getCode(),                    
                    'product_size' => $size,
                    'product_price' => $product->getPrice(),
                    'product_quantity' => $product->getQuantityInCart()
                ));
                if ($savehistory) {
                    $productHistory->updateInventory($product->getProductId(), $operation, $product->getQuantityInCart(), $order_id);
                }    
            } catch (Zend_Exception $e) {
                echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
                echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            }
        }
        return 1;
    }

}
