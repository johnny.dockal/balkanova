<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_OrderStep2form extends Zend_Form {

    public function __construct($deliveryOptions, $deliveryCount = 1) {
        parent::__construct();
        $this->setMethod('POST')->setName('formorderstep2')->setAction('/cart/order/step/3/');
        $this->setAttrib('class', 'formorder');
        //načteme view, ve kterém jsou již připravené texty v patřičném jazyce
        $view = Zend_Layout::getMvcInstance()->getView(); 

        $step = new Zend_Form_Element_Hidden('current_step', array('value' => '1'));
        $step->setDecorators(array('ViewHelper'));

        $delivery_options_formatted = array();
        foreach ($deliveryOptions as $value) { 
            $note = "";
            //ochcávka kvůli cenám v německu, teď se to musí převést do eura z korun
            if (APP_ID == 2) {
                $price = $view->currency->exchangeToEuroNumber($value['price']);
            } else {
                $price = $value['price'];
            }
            if (($deliveryCount > 1) && ($price > 0)) {
                $sum = $deliveryCount * $price;
                $priceIsRight = $deliveryCount . "x " . $price . " = +" . $sum;
                $note = "<p class='order-description'>*Vaše objednávka bude rozdělena do $deliveryCount balíčků. Za tuto cenu za dopravu můžete nakoupit ještě další zboží.</p>"; 
            } else if ($price > 0) {
                $priceIsRight = $view->currency->toCurrency($price);
            } else {
                $priceIsRight = $view->str_order_free;
            }
            if (($value['public'] == 1) && !empty($price)) {                
                $delivery_options_formatted[$value['shipping_id']] = "&nbsp;<span class='order-title'><strong>" . $value['title'] . ": <span class='yellow'>" . $priceIsRight . "</span> </strong></span><div class='order-description'>" . $value['text'] . "</div>";
            }
        }
        $delivery = new Zend_Form_Element_Radio('shipping_id', array(
                    'multiOptions' => $delivery_options_formatted,
                    'escape' => false,
                    'required' => true
                ));
        $delivery->setLabel($view->str_order_select_shipping);        
        
        $message = new Zend_Form_Element_Textarea('order_message', array('style' => 'height: 100px; width: 98%'));
        $message->setAttrib('maxlength','1000');  
        $message->setLabel($view->str_order_message);     
        
        $back = new Zend_Form_Element_Submit('cartbuttonprev', array('label' => $view->form_back, 'class' => "button button-white floatleft"));
        $back->removeDecorator('DtDdWrapper');
        $submit = new Zend_Form_Element_Submit('cartbuttonnext', array('label' => $view->form_select_payment, 'class' => "button button-yellow floatright"));
        $submit->removeDecorator('DtDdWrapper');

        $this->addElements(array($step, $delivery, $message, $submit, $back));
    }

}
