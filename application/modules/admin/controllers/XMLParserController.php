<?php

class Admin_XMLParserController extends Zend_Controller_Action {

    public function init() {
        $this->view->headTitle('Uživatelé', 'POSTEND');
    }

    public function indexAction() {
        
    }

    public function parseAction() {
        $model = new Model_DbTable_EshopProducts();
        $products = $model->fetchProducts(1);
        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>'
                          .'<shop></shop>');
        foreach ($products as $value) {   
            $shopitem = $xml->addChild('shopitem');
            $shopitem->addChild('item_id', $value['product_id']);
            $shopitem->addChild('productname', $value['title']);            
            $shopitem->addChild('product', $value['full_title']);
            $shopitem->addChild('description', $value['text']);                       
            $shopitem->addChild('url', "www.balkanova.cz/".$value['category_alias']."/".$value['subcategory_alias']."/".$value['alias']);
            $shopitem->addChild('imgurl', "www.balkanova.cz/images/eshop_products/".$value['product_id']."_big.jpg");
            $shopitem->addChild('imgurl_alternative', "www.balkanova.cz/images/eshop_products/".$value['product_id']."_detail.jpg");
            $shopitem->addChild('price_vat', $value['price']); 
            $shopitem->addChild('categorytext', $value['category_title']); 
        }    
        unlink('heureka.xml');
        $xml->asXML('heureka.xml');
        
        //$writer->save("sitemap.xml");
        //$this->_redirect('/admin/xmlparser/');
    }

}
