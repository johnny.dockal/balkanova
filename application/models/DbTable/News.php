<?php

/**
 * Model tabulky article.
 *
 * @package default
 * @author Daniel Vála
 */
class Model_DbTable_News extends Zend_Db_Table_Abstract {

    protected $_name = 'news';
    protected $_primary = 'news_id';

    /**
     * Vybere z databáze všechny texty na stránky.
     * Předpokládá se, že text_id článku bude pojmenovávat typ článku.
     * Text_id se ve výsledném řetězci přehazuje jako klíč k poli $textarray, aby k tomu byl jednodužší přístup.
     * 
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function fetchNewsAll() {
        $query = $this->select();
        $query->from($this->_name, array('news_id', 'date', 'image', 'link_cz AS link','title_cz AS title', 'text_cz AS text'));
        $query->where('eshop_id = '.APP_ID);
        $query->order('date DESC');
        $result     = $this->fetchAll($query);
        $newsarray  = $result->toArray();        
        return $newsarray;
    }
    
    public function fetchNewsLatest() {
        $query = $this->select();
        $query->from($this->_name, array('news_id', 'date', 'image', 'link_cz AS link','title_cz AS title', 'text_cz AS text'));
        $query->where('eshop_id = '.APP_ID);
        $query->order('date DESC');
        $result     = $this->fetchRow($query);
        $newsarray  = $result->toArray();    
        return $newsarray;
    }
    
    public function getNewsActiveAjax() {
        $session = new Zend_Session_Namespace('Default');
        if (isset($session->news_active)) {
            return $session->news_active;
        } else {
            return true;
        }
    }
    
    public function setNewsActiveAjax($bool) {
        $session = new Zend_Session_Namespace('Default');
        $session->news_active = $bool;
    }
    
}