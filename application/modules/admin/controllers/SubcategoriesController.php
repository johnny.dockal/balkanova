<?php

class Admin_SubcategoriesController extends Zend_Controller_Action {
    
    private $adminSession = null;

    public function init() {
        $this->adminSession = new Zend_Session_Namespace('Admin');        
        if ($this->getParam('eshop_id')) {            
            $this->adminSession->eshop_id = $this->getParam('eshop_id');
        } else {
            $this->adminSession->eshop_id = 0;
        }
    }

    public function indexAction() {
        $subcatmodel = new Model_DbTable_EshopSubCategories();   
        $this->view->subcategories = $subcatmodel->fetchSubCategoriesAdmin($this->adminSession->eshop_id);         
        $this->view->form = new Form_EshopSelectform();
    }

    public function editAction() {
        $subcatmodel = new Model_DbTable_EshopSubCategories();
        $catmodel = new Model_DbTable_EshopCategories();
        $subcategoryId = $this->getParam('subcategory_id');
        $subcategory = $subcatmodel->find($subcategoryId)->toArray();
        $catoptions = $catmodel->fetchAll()->toArray();
        if (APP_ID == 2) {
            $lang = 'německy';
        } else {
            $lang = 'česky';
        }
        $form = new Form_SubcategoriesformCZEN('/admin/subcategories/save/', $catoptions, $lang, $subcategoryId);
        //nasolíme text do připraveného formuláře včetně seznamu kategorií
        if (empty($subcategory)) {
            $this->view->title = "nová";
        } else {
            $form->populate($subcategory[0]);
            $this->view->title = $subcategory[0]['title_cz'];
        }
        $this->view->catoptions = $catoptions;
        $this->view->form = $form;
    }

    public function saveAction() {
        $table      = new Model_DbTable_EshopSubCategories();
        $form       = new Form_SubcategoriesformCZEN();
        $subcat_id  = $this->getParam('subcategory_id');
        $data       = array(
            'category_id' => $this->getParam('category_id'),
            'sequence' => $this->getParam('sequence'),
            'public' => $this->getParam('public'),
            'title_cz' => $this->getParam('title_cz'),
            'url_cz' => $this->getParam('url_cz'),
            'text_cz' => $this->getParam('text_cz'),
            'full_title_cz' => $this->getParam('full_title_cz'),
            'maintainance_cz' => $this->getParam('maintainance_cz'),
            'title_en' => $this->getParam('title_en'),
            'url_en' => $this->getParam('url_en'),
            'text_en' => $this->getParam('text_en'),
            'full_title_en' => $this->getParam('full_title_en'),
            'maintainance_en' => $this->getParam('maintainance_en'),
            'softness' => $this->getParam('softness'),
            'rowdisplay' => $this->getParam('rowdisplay')
        );
        if (empty($subcat_id)) {
            $table->insert($data);
        } else {
            $where = $table->getAdapter()->quoteInto('subcategory_id = ?', $subcat_id);
            $table->update($data, $where);
        }
        if ($form->image->isUploaded()) {
            echo "<br/>obrázek vybrán<br/>";
            $path = getcwd() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'eshop_subcategories' . DIRECTORY_SEPARATOR;
            $target_name = $subcat_id . ".jpg";
            $target_path = $path . DIRECTORY_SEPARATOR . $target_name;

            $validator = new Zend_Validate_File_Exists();
            $validator->addDirectory(getcwd());

            if ($validator->isValid($target_path)) {
                unlink($target_path);
            }

            $form->image->setDestination($path);
            $form->image->addFilter('Rename', $target_name);
            /*
              echo "teď se bude přenastavovat velikost...";
              $filterChain = new Zend_Filter();
              $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
              'directory' => $path . 'l',
              'width' => 300,
              'height' => 600,
              'cropToFit' => true,
              'keepRatio' => true,
              'keepSmaller' => false,
              )));

              $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
              'directory' => $path . 'm',
              'width' => 100,
              'height' => 200,
              'cropToFit' => true,
              'keepRatio' => true,
              'keepSmaller' => false,
              )));
              $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
              'directory' => $path . 's',
              'width' => 50,
              'height' => 50,
              'cropToFit' => true,
              'keepRatio' => true,
              'keepSmaller' => false,
              )));
              $form->product_image->addFilter($filterChain);
              echo ini_get('upload_max_filesize');
              echo "<br/>teď se bude ukládat...";
             * */
            try {
                echo "zkusíme ho uložit ($target_name)<br/>";
                if ($form->image->receive()) {
                    echo "povedlo se <br/>";
                } else {
                    echo "NEpovedlo se<br/>";
                }
            } catch (Zend_File_Transfer_Exception $e) {
                throw new Exception('Unable to recieve : ' . $e->getMessage() . " <br/>PATH: " . $path);
            }
        } else {
            echo "Obrázek nebyl vybrán";
        }
        $this->_redirect('/admin/subcategories/');
    }

    public function deleteAction() {
        $subCategoryId = $this->getParam('subcategory_id');
        if (!empty($subCategoryId)) {
            $table = new Model_DbTable_SubCategories();
            $where = $table->getAdapter()->quoteInto('subcategory_id = ?', $subCategoryId);
            $table->delete($where);
        }
        $this->_redirect('/admin/subcategories/');
    }

}
