<?php
/**
 * Poslední aktualizace 2015-02-21 (locale)
 */
class Model_DbTable_Texts extends Zend_Db_Table_Abstract {

    protected $_name = 'texts';
    protected $_primary = 'text_id';
    protected $lang = null;

    /**
     * Vybere z databáze všechny texty na stránky.
     * Předpokládá se, že text_id článku bude pojmenovávat typ článku.
     * Text_id se ve výsledném řetězci přehazuje jako klíč k poli $textarray, aby k tomu byl jednodužší přístup.
     * 
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function init() {
        $session = new Zend_Session_Namespace('Default');        
        //v německém locale je správná informace uložena v kolonce pro češtinu
        if ($session->lang == 'de') {
            $this->lang = 'cz';
        } else {
            $this->lang = $session->lang;
        }
    }
    
    public function fetchTexts() {
        $texts = $this->select();
        //jazyk musí být dvoupísmenná zkratka, jestli tomu tak je se kontroluje už v pluginu LangSelector
        $texts->from('texts', array('text_id', 'text_name', 'title_'.$this->lang.' AS title', 'text_'.$this->lang.' AS text'));
        $texts->where("eshop_id = '".APP_ID."'");
        $result     = $this->fetchAll($texts);
        $textarray  = $result->toArray();
        for ($i = 0; $i < count($textarray); $i++) {
            $newkey             = $textarray[$i]['text_name'];
            $textarray[$newkey] = $textarray[$i];
            unset($textarray[$i]);
        }
        return $textarray;
    }
    
    public function fetchText($text_id) {
        //ochcávka kvůli balkanově, kde jsou německé texty v tabulce pod CZ
        if ($this->lang == 'de') {
            $lang = 'cz';
        } else {
            $lang = $this->lang;
        }
        $sql = $this->select();
        $sql->from('texts', array('text_id', 'title_'.$lang.' AS title', 'text_'.$lang.' AS text'));
        $sql->where("text_id = '$text_id' AND eshop_id = '".APP_ID."'");      
        try {
            $result = $this->fetchRow($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }
    
    public function fetchTextName($text_name) {
        //ochcávka kvůli balkanově, kde jsou německé texty v tabulce pod CZ
        if ($this->lang == 'de') {
            $lang = 'cz';
        } else {
            $lang = $this->lang;
        }
        $sql = $this->select();
        $sql->from('texts', array('text_id', 'text_name', 'title_'.$lang.' AS title', 'text_'.$lang.' AS text'));
        $sql->where("text_name = '$text_name' AND eshop_id = '".APP_ID."'");              
        try {
            $result = $this->fetchRow($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }
    
    /* vrátí jeden vybraný text z tabulky texts ve všech jazykových verzích (je potřeba zobecnit) */
    public function fetchBeer() {
        $texts = $this->select();
        //tohle je třeba zobecnit pro libovolně nastavený počet jazyků
        $texts->from($this->_name, array('text_id', 'title_cz', 'text_cz', 'text_en'));
        $beer = array('nacepu', 'pivo1', 'pivo2', 'pivo3', 'pivo4', 'pivo5', 'pivo6');
        $texts->where('text_id IN (?)', $beer);
        $result = $this->fetchAll($texts);
        return $result->toArray();
    }
    
    function getEmailText($lang, $transport = null, $payment = null) {   
        //ochcávka kvůli němčině
        if ($lang == 'de') {
            $lang = 'cz';
        }
        $db = Zend_Db_Table::getDefaultAdapter();
        // transport
        // 3 = Česká pošta 
        // 6 = Česká pošta express tarif
        // 5 = Česká pošta economy tarif
        if (in_array($transport, array('3', '5', '6'))) {
            // dobírka
            if ($payment == '2') {
                $variant = '1';
            // převod na kč, slovenský a SIPO účet
            } else if (in_array($payment, array('7', '9', '10'))) {
                $variant = '7';
            // paypal/karta
            } else {
                $variant = '2';
            }
        // 1 = Osobní odběr
        } else if ($transport == '1') {
            // převod na kč, slovenský a SIPO účet
            if (in_array($payment, array('7', '9', '10'))) {
                $variant = '9';
            // platba při osobním převzetí
            } else if ($payment == '3') {
                $variant = '5';
            // dobírka
            } else {
                $variant = '6';
            }
        // 2 = PPL/DHL/balík
        // 4 = Pošta bez hranic
        // 7 = PPL na slovensko    
        } else if ($transport == '4') {
            // převod na kč, slovenský a SIPO účet
            if (in_array($payment, array('7', '9', '10'))) {
                $variant = '12';
            // paypal/karta
            } else if ($payment == '1') {
                $variant = '11';
            } else {
                $variant = '10';
            }        
        // 17 = zásilkovna    
        } else if ($transport == '17') {
            // 17 = dobírka přes zásilkovnu
            if ($payment == '17') {
                $variant = '13';
            // 1 = paypal    
            } else if ($payment == '1') {
                $variant = '14';
            // 7, 9, 10 = převod na účet
            } else {
                $variant = '15';
            }                  
        } else {
            // převod na kč, slovenský a SIPO účet
            if (in_array($payment, array('7', '9', '10'))) {
                $variant = '8';
            // paypal/karta
            } else if ($payment == '1') {
                $variant = '3';
            } else {
                $variant = '4';
            }
        }
        if (empty($transport) && empty($payment)) {
            $variant = 'sent';
        }
        $sql = "SELECT title_$lang AS title, text_$lang AS text "
                . "FROM $this->_name "
                . "WHERE text_name = 'email$variant' AND eshop_id = '".APP_ID."'";
        try {
            $result = $db->fetchRow($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception ".__METHOD__.": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }
    
    public function updateAkce($text) {
        $data = array(
            'text_cz'      => $text,
            'text_en'      => $text
        );
        $where = $this->getAdapter()->quoteInto("text_id = 'akce'");
        $this->update($data, $where);
    }
    
    /* Vzhledem k tomu, že formuláře standartních textů jsou všechny stejné, 
     * můžeme si dovolit ukládat texty zde, abychom nemuseli sbírat parametry v každém kontrolleru zvlášť 
     * POZOR, tohle je nestandartní, normálně by se měli parametry předávat z controlleru, ale
     * tohle mi zabrání duplikaci kódu v kontrolerech. 
     * zároveň si tato funkce přečte, kolik je jazyků v configu, takže je použitelná všeobecně
     * pro libovolný počet jazyků v nastavení
     */
    public function updateText() {
        //abychom získali parametry z postu, musíme si zavolat singleton Froncontrolleru
        $fc                     = Zend_Controller_Front::getInstance();
        //zjistíme si dvoupísmenné jazyky nastavené v configu
        $config                 = Zend_Registry::get('config');        
        $langArray['enabled']   = explode(" ", $config->settings->languages->enabled);
        $text_id                     = $fc->getRequest()->getParam('text_id');
        $data                   = array();
        foreach ($langArray['enabled'] as $lang) {
            $data['title_'.$lang]  = $fc->getRequest()->getParam('title_'.$lang);
            $data['text_'.$lang]   = $fc->getRequest()->getParam('text_'.$lang);
        }
        $where = $this->getAdapter()->quoteInto('text_id = ?', $text_id);
        $this->update($data, $where);
    }
    
    function array_push_assoc($array, $key, $value){
        $array[$key] = $value;
        return $array;
    }
}