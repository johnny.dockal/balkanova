<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_EshopShippingRates extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_deliveries_prices_sets';
    protected $_primary = 'id';
    
    public function fetchTarifs() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT id, price_set_id FROM eshop_deliveries_prices_sets GROUP BY price_set_id";
        try {
            $tarifs = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception during saving order details: " . get_class($e) . "\n <br/>";
            echo "Message: " . $e->getMessage() . "\n <br/>";
            echo "SQL: " . $sql . "\n <br/>";
        }
        return $tarifs;
    }
}