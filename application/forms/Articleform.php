<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Articleform extends Zend_Form {

    public function __construct($action, $values = null, $categories = null) {
        parent::__construct($values);
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('class', 'admintable');
        $this->setAttrib('enctype', 'multipart/form-data');
        
        //momentálně je formulář nastaven na dva jazyky (cz a en), nevím jak ho upravit pro libovolný počet jazyků z configu
        $values_id = new Zend_Form_Element_Hidden('article_id', array('value' => $values[0]['article_id']));
        //zabrání zobrazení labelu
        $values_id->setDecorators(array('ViewHelper'));
        
        $title = new Zend_Form_Element_Text('article_title', array('class' => 'textboxwide', 'value' => $values[0]['title']));
        $title->setLabel('Nadpis článku:')->setRequired(true);
        
        if (empty($values[0]['date']) or $values[0]['date'] == "0000-00-00") {
            $date_value = date("Y-m-d");
        } else {
            $date_value = $values[0]['date'];
        }
        $date = new Zend_Dojo_Form_Element_DateTextBox('article_date', array('value' => $date_value));
        $date->setLabel('Zobrazené datum:')->setRequired(true);
        
        $status = new Zend_Form_Element_Radio('article_status');
        $status->addMultiOptions(array(
	               'private'    => 'Nezveřejněný',
	               'public'     => 'Veřejný',
                       'deleted'    => 'Vyřazený (určený ke smazání)'));
	if ($values[0]['status'] == 'public') {
            $status->setValue('public');
        } else {
            $status->setValue('private');
        }
        $status->setLabel('Status:')->setRequired(true);
        
        $image = new Zend_Form_Element_File('article_image');
        $image->setLabel('Obrázek ke článku 200*134 pixelů JPG (pokud se nenahraje obrázek, bude se zobrazovat výchozí - pípy):')
              ->setDestination(getCWD().'/images/articles/');
        $image->addValidator('Count', false, 1);
        $image->addValidator('Size', false, 102400);
        $image->addValidator('Extension', false, 'jpg');
        
        $perex = new Zend_Dojo_Form_Element_Textarea('article_perex', array('class' => "valuesboxbig", 'value' => $values[0]['perex']));
        $perex->setLabel('Perex:')->setRequired(true);

        $text = new Zend_Dojo_Form_Element_Textarea('article_text', array('class' => "valuesboxbig", 'value' => $values[0]['text']));
        $text->setLabel('Text:')->setRequired(true);

        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "savebutton"));
        
        if (!empty($values)) {
            //Na tohle pak musim přijítů
            //$this->populate($values[0]);
        }
        
        $this->addElement($title);
        
        if (!empty($categories)) {
            $category_id = new Zend_Form_Element_Select('article_category_id');
            $category_id->setLabel('Kategorie:')->setRequired(true);;
            foreach ($categories as $value) {
                $category_id->addMultiOption($value['category_id'], $value['title']);
            } 
            $category_id->setValue($values[0]['category_id']);
            $this->addElement($category_id);
        }
        
        $this->addElements(array(
            $values_id, $status, $date, $image, $perex, $text, $submit
        ));
        
    }
}
