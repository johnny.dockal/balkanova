<?php

class Model_EshopCart {

    private $cart = null;

    function __construct() {
        $this->cart = new Zend_Session_Namespace('Cart');
    }

    public function addProductAjax($product_id, $quantity = 1, $price = 0, $size = 0) {
        if (isset($this->cart->$product_id)) {
            $quantity = ($this->cart->$product_id->quantity + $quantity);
        }
        $this->updateProduct($product_id, $quantity, $size, $price);

        // calculate sum and count of all products in the cart
        $sum = (float)0.0;
        $count = 0;
        foreach ($this->cart as $val) {
            $sum += (float)((float)$val->price * (float)$val->quantity);
            $count += $val->quantity;
        }

        return array('quantity' => $count, 'price' => $sum);
    }

    public function countProducts($distinct = true) {
        if (!isset($_SESSION['Cart'])) {
            return 0;
        } else if ($distinct) {
            $sum = 0;
            foreach ($this->cart as $product) {
                $sum = $sum + $product->quantity;
            }
            return $sum;
        } else {
            return count($_SESSION['Cart']);
        }
    }

    public function countTotal() {
        if (!isset($_SESSION['Cart'])) {
            return 0;
        } else {
            $sum = 0;
            foreach ($this->cart as $product) {
                $sum = $sum + ($product->price * $product->quantity);
            }
            return $sum;
        } 
    }

    public function addProduct($product_id, $quantity = 1, $price = null, $size = null) {
        //if (isset($this->cart->$product_id)) {
        if (isset($_SESSION['Cart'][$product_id])) {
            $quantity = $this->cart->$product_id->quantity + $quantity;
        }
        $this->updateProduct($product_id, $quantity, $size, $price);
    }

    public function updateProduct($product_id, $quantity = 1, $size = null, $price = null) {
        $obj = new stdClass();
        $obj->quantity = $quantity;
        $obj->size = $size;
        $obj->price = $price;

        $this->cart->$product_id = $obj;
    }

}
