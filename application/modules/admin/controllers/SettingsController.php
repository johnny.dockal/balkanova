<?php
class Admin_SettingsController extends Zend_Controller_Action
{
     public function init()
    {
        /* Initialize action controller here */
    }

    function indexAction() {
        $modelSettings          = new Model_DbTable_Settings();
        $settings               = $modelSettings->fetchAll()->toArray();
        $settingforms           = array();
        $i = 0;
        foreach ($settings as $setting) {            
            $form               = new Form_Settingsform($setting, $i++);
            array_push ($settingforms, $form);     
        }
        $this->view->aSettingForms = $settingforms;
    }
    
    function saveAction() {
        $modelSettings          = new Model_DbTable_Settings();
        $i                      = $this->getParam('setting_number');
        $setting_id_i           = $this->getParam('setting_id_'.$i);
        $setting_id             = str_replace('_'.$i, '', $setting_id_i);
        $data = array(
            'setting_value1'     => $this->getParam('setting_value1_'.$i),
            'setting_value2'     => $this->getParam('setting_value2_'.$i),
            'setting_text_cz'       => trim($this->getParam('setting_text_cz_'.$i)),
            'setting_text_en'       => trim($this->getParam('setting_text_en_'.$i)),            
            'setting_text_de'       => trim($this->getParam('setting_text_de_'.$i))
        );
        $where = $modelSettings->getAdapter()->quoteInto('setting_id = ?', $setting_id);
        $modelSettings->update($data, $where);
        $this->_redirect('/admin/settings/');
    }
}
?>
