<?php
/*
 * 22.1.2014 dopln�n� funkce na tvorbu token�
 * 22.1.2014 upraven� atributy controlleru $this->mailmodel
 */
class MailinglistliteController extends Zend_Controller_Action {

        private $mailmodel = null;
        private $mailform  = null;
        
    public function init() {
        $this->mailmodel = new DbTable_Mailinglist();
        $this->mailform  = new Form_Mailinglistform('/mailinglist/save/');
        
	$defaultSession = new Zend_Session_Namespace('Default');
	$this->lang = $defaultSession->lang;

	$constants=new GetConstants();
	$this->config = $constants->getConfig();
		
	$this->view->config = $this->config;
	$this->view->str = $constants->getLangStr($this->lang);
		
		
	$this->view->title = $this->view->str->ourproducts;
	$this->display = $this->_getParam('display');
        $this->view->headimg = "/graphics/nase-vyrobky_$this->lang.gif";
    }
    
    public function indexAction() {
        $this->view->form = $this->form;
    }

    public function saveAction() {
        $address = $this->_request->_getPost('address');
        if (isset($address)) {
            $status = 1;
        } else {
            $address    = $this->_request->_getQuery('address');            
            $token      = $this->_request->_getQuery('token');
            if ($token == md5($address."a1b2c3")) {
                $status = 3;                
            } else {
                $status = 0;
            }
        }
        //zkontrolujeme, zda je adresa relevantn�
        if (!filter_var($address, FILTER_VALIDATE_EMAIL)) {
            $status = 0;
        }
        /*/*zkontrolujeme, zda m� adresa existruj�c� MX z�znam
        if (!checkdnsrr($domain, 'MX')) {
            $status = 0;
        }*/
        if ($status > 0) {
            //zkontrolujeme, zda u� n�hodou nen� adresa v datab�zi
            $entry = $this->mailmodel->fetchRow("mail = '$address'");
            if (isset($entry)) {
                //adresa je v datab�zi, ale je odhl�en� od odb�ru
                if ($entry->status <= 0) {
                    $data = array(
                        'status' => '4'
                    );
                    $where = "mail_id = '$entry->mail_id'";
                    $this->mailmodel->update($data, $where);
                    $this->view->message = "<h2>Adresa $address �sp�n� registrov�na. D�kujeme v�m za d�v�ru..</h2>";
                } else {
                    $this->view->message = "<h2>Tuto adresu ($address) ji� m�me v datab�zi.</h2>";
                }
            } else {
                $data = array(
                    'mail'      => $address,
                    'status'    => $status,
                    'token'     => md5($address . "a1b2c3")
                );
                $mailmodel->insert($data);
                $this->view->message = "<h2>Adresa $address �sp�n� registrov�na. D�kujeme v�m za d�v�ru.</h2>";
            }
        } else {
            $this->view->message = "<h2>Adresa nen� zad�na spr�vn�.</h2>";  
            $this->view->form = $this->mailform;
        }
    }

    public function deleteAction() {
        $mail_id    = $this->_getParam('mail_id');
        $token      = $this->_getParam('token');
        
        if (isset($token)) {
            $where = "token = '$token'";
            $entry = $this->mailmodel->fetchRow($where);
            $this->view->mail_id    = $entry->mail_id;
            $this->view->mail       = $entry->mail;
            $this->render('confirm');
        } else if (isset($mail_id)) {
            $where = "mail_id = '$mail_id'";
            $entry = $this->mailmodel->fetchRow($where);
            $data = array(
                'status' => '-1'
            );
            $this->mailmodel->update($data, $where);
            $this->view->message = "<h2>Adresa ($entry->mail) odstran�na z datab�ze.</h2>";
        }
    }
}