<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopSubCategories extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_subcategories';
    protected $_primary = 'subcategory_id';
    protected $lang = null;

    public function init() {
        $session = new Zend_Session_Namespace('Default');
        //v německém locale je správná informace uložena v kolonce pro češtinu
        if ($session->lang == 'de') {
            $this->lang = 'cz';
        } else {
            $this->lang = $session->lang;
        }
    }

    public function fetchSubCategories($category_id = null, $subcategory_id = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $query = "SELECT "
                . "es.subcategory_id, "
                . "es.public, "
                . "es.sequence, "
                . "es.sequence AS subcat_sequence, "
                . "es.url_$this->lang AS subcat_alias, "
                . "es.title_$this->lang AS subcat_title, "
                . "es.full_title_$this->lang AS subcat_full_title, "
                . "es.text_$this->lang AS subcat_text, "
                . "es.softness, "
                . "es.maintainance_type, "
                . "es.maintainance_$this->lang AS maintainance, "
                . "es.rowdisplay, "
                . "ec.category_id AS category_id, "
                . "ec.url_$this->lang AS cat_alias, "
                . "ec.title_$this->lang AS cat_title, "
                . "ec.text_$this->lang AS cat_text, "
                . "ec.eshop_id "
                . "FROM eshop_categories AS ec JOIN eshop_subcategories AS es
                        ON ec.category_id = es.category_id";
        if (isset($category_id)) {
            try {
                $query .= " WHERE ec.category_id = '$category_id' AND ec.eshop_id = '" . APP_ID . "'";
                $query .= " ORDER BY ec.eshop_id, es.category_id, es.sequence";
                $result = $db->fetchAll($query);
            } catch (Zend_Exception $e) {
                echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
                echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
                echo "\n <br/>SQL: " . $sql . "\n <br/>";
            }
        } else if (isset($subcategory_id)) {
            try {
                $query .= " WHERE es.subcategory_id = '$subcategory_id'";
                $result = $db->fetchRow($query);
            } catch (Zend_Exception $e) {
                echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
                echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
                echo "\n <br/>SQL: " . $sql . "\n <br/>";
            }
        } else {
            try {
                $query .= " ORDER BY ec.eshop_id, ec.category_id, es.sequence";
                $result = $db->fetchAll($query);
            } catch (Zend_Exception $e) {
                echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
                echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
                echo "\n <br/>SQL: " . $sql . "\n <br/>";
            }
        }
        return $result;
    }

    public function fetchSubCategoriesAdmin($eshop_id = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT * "
                . "FROM eshop_categories AS ec JOIN eshop_subcategories AS es
                        ON ec.category_id = es.category_id";
        if (isset($eshop_id) && !empty($eshop_id)) {
            $sql .= " WHERE ec.eshop_id = '$eshop_id' ORDER BY ec.eshop_id, ec.category_id, es.sequence";
        } else {
            $sql .= " ORDER BY ec.eshop_id, ec.category_id, es.sequence";
        } try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }

    public function fetchSubCategoryByAlias($alias) {
        $query = $this->select()->from(array('subcat' => $this->_name), array(
            'subcategory_id', 
            'sequence', 
            'url_' . $this->lang . ' AS alias', 
            'full_title_' . $this->lang . ' AS full_title', 
            'title_' . $this->lang . ' AS title', 
            'text_' . $this->lang . ' AS text', 
            'rowdisplay'));
        $query->where("url_$this->lang = '$alias'");
        $result = $this->fetchRow($query);
        return $result;
    }
    
    public function fetchSubcategoriesOfGroup($group_id, $ids = array()) {
        $session = new Zend_Session_Namespace('Default');
        $db = Zend_Db_Table::getDefaultAdapter();
        // parent_id znamena, ke ktere kategorii tahle subkategorie patri
        // po zmene ziskavani dat pres produkty smazat z testovaci db sloupec parent_id (v produkcni neni)
        $sql = "SELECT DISTINCT esg.group_id, esg.subcategory_id, esc.url_$session->lang AS alias, esc.title_$session->lang AS title "
                . "FROM eshop_subcat_groups AS esg "
                . "JOIN eshop_subcategories AS esc ON esc.subcategory_id = esg.subcategory_id "
                . "WHERE esg.group_id = '$group_id' ";
        $i = 0;
        foreach ($ids as $id) {
            if ($i > 0) {
                $sql .= "OR ";
            } else {
                $sql .= "AND (";                
            }
            $sql .= "  esc.subcategory_id = '$id' ";
            $i++;
        }
        $sql .= ")";
        try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }

}
