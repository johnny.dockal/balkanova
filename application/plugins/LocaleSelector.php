<?php

/**
 * Plugin zjištuje požadavky na změnu jazyka webu v parametru.
 * Pokud odpovídá českému nebo anglickému jazyku, nastaví ji do Zend session.
 * @author Daniel Vála
 */
class Plugin_LocaleSelector extends Zend_Controller_Action_Helper_Abstract {

    public function init() {
        $localearray = array('cz', 'de');
        $session = new Zend_Session_Namespace('Default');
        // pokud je zjištěn požadavek na změnu jazyka, kontroluje se, zda je jazyk v požadavku jeden z uvedených v konfigu a nastaví se
        // pokud nevyhovuje nebo není vůbec nastaven, zachová se stávající jazyk
        $locale = $this->getRequest()->getParam('locale');
        if (isset($locale) && in_array($locale, $localearray)) {
            $session->locale = $locale;
        } else if (empty($session->locale)) {
            $url = $_SERVER['HTTP_HOST'];
            $url_array = explode('.', $url);            
            if (in_array($url_array[2], $localearray)) {
                $session->locale = $url_array[2];
            } else {
                $session->locale = 'cz';
            }
        }
        $view = Zend_Layout::getMvcInstance()->getView();
        $currency = new Model_Currency();
        $view->currency = $currency;
        $view->locale = $session->locale;
    }

}
