<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_EshopSelectform extends Zend_Form {

    public function __construct() {
        parent::__construct();
        $this->setMethod('POST')->setName('eshopselect')->setAction($_SERVER['REQUEST_URI']);  
        $this->setAttrib('enctype', 'multipart/form-data');

        $select = new Zend_Form_Element_Select('eshop_id');        
        $select->removeDecorator('label');      
        $select->removeDecorator('DtDdWrapper');
        $model = new Model_DbTable_EshopShops();
        $eshops = $model->fetchEshops();
        
        $select->addMultiOption("0", "VŠE");
        foreach ($eshops as $value) {
            $select->addMultiOption($value['eshop_id'], $value['title']);
        }
        $adminSession = new Zend_Session_Namespace('Admin');
        if (isset($adminSession->eshop_id)) {
            $select->setValue($adminSession->eshop_id);
        }
        
        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Filtr"));              
        $submit->removeDecorator('label'); 
        $submit->removeDecorator('DtDdWrapper');   

        $this->addElements(array(
            $select, $submit
        ));
    }

}
