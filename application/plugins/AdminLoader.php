<?php

/**
 * Plugin zjištuje požadavky na změnu jazyka webu v parametru.
 * Pokud odpovídá českému nebo anglickému jazyku, nastaví ji do Zend session.
 * @author Daniel Vála
 */
class Plugin_AdminLoader extends Zend_Controller_Plugin_Abstract {

    private $adminSession = null;
/*
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request) {
        if ('admin' != $request->getModuleName()) {
            // If not in this module, return early
            return;
        }
    }*/

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        echo "<p>init AdminLoader</p>";
        $this->adminSession = new Zend_Session_Namespace('Admin');
        //$request = new Zend_Controller_Request_Http();
        if (!isset($this->adminSession->eshop_id)) {
            $this->adminSession->eshop_id = 0;
            echo "<p>nic nepřišlo</p>";
        } else if ($request->getParam('eshop_id')) {
            $this->adminSession->eshop_id = $request->getParam('eshop_id');
            echo "<p>něco přišlo</p>";
        } else {            
            echo "<p>smůla ".$request->getParam('eshop_id')."</p>";
        }
        if (!isset($this->adminSession->year)) {
            $this->adminSession->year = date('Y');
        } else if ($request->getParam('year')) {
            $this->adminSession->year = $request->getParam('year');
        }
        if (!isset($this->adminSession->filter)) {
            $this->adminSession->filter = array(
                '0' => null,
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => null,
                '8' => null
            );
        } else if ($request->getParam('filter')) {
            $this->adminSession->filter = array(
                '0' => $request->getParam('0'),
                '1' => $request->getParam('1'),
                '2' => $request->getParam('2'),
                '3' => $request->getParam('3'),
                '4' => $request->getParam('4'),
                '5' => $request->getParam('5'),
                '6' => $request->getParam('6'),
                '7' => $request->getParam('7'),
                '8' => $request->getParam('8')
            );
        }
        $view = Zend_Layout::getMvcInstance()->getView();
        $view->eshop_id = $this->adminSession->eshop_id;
        $view->year = $this->adminSession->year;
        $view->filter = $this->adminSession->filter;
    }

}
