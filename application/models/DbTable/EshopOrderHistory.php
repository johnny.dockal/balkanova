<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopOrderHistory extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_order_history';
    protected $_primary = 'order_id';

    public function fetchOrderHistory($order_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT "
                . "oh.order_history_id, "
                . "oh.order_id, "
                . "u.user_name, "
                . "oh.user_id, "               
                . "oh.status_id, "             
                . "s.title_cz, "
                . "s.color, "
                . "oh.timestamp, "
                . "oh.text "
                . "FROM $this->_name AS oh "
                . "LEFT JOIN users AS u ON oh.user_id = u.user_id "                
                . "LEFT JOIN eshop_order_status AS s ON oh.status_id = s.status_id "
                . "WHERE order_id = '$order_id'"
                . "ORDER BY oh.timestamp";
        try {
            $history = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $history;
    }
    
    public function saveHistory($order_id, $status_id, $text = null) {      
        if (!isset($text)) {
            $text = '';
        }
        $user_id = (isset(Zend_Auth::getInstance()->getIdentity()->user_id)) ? Zend_Auth::getInstance()->getIdentity()->user_id : null; 
        $data = array(
            'order_id' => $order_id,
            'user_id' => $user_id,
            'status_id' => $status_id,
            'text' => $text
        );
        try {
            $this->insert($data);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
        }
    }
}
