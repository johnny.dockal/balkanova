<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Paymentform extends Zend_Form {

    public function __construct($settings) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction('/admin/payment/save/');
        $this->setAttrib('class', 'admintable');

        $i = 0;
        foreach ($settings as $value) {
            $i++;
            $setting_id = new Zend_Form_Element_Hidden($i.'payment_id');
            $setting_id->setValue($value['payment_id']);
            $public = new Zend_Form_Element_Radio($i.'public');
            $public->addMultiOptions(array(
                           '0'    => 'Zákázáno',
                           '1'    => 'Umožněno', 
                ));  
            $public->setValue($value['public']);
            $public->setLabel('Status:')->setRequired(true);
            $text_cz = new Zend_Dojo_Form_Element_Textarea($i.'text_cz', array('class' => 'textbox', 'value' => $value['text_cz']));
            $text_cz->setLabel('Doprovodný text česky')->setRequired(true);
            $text_en = new Zend_Dojo_Form_Element_Textarea($i.'text_en', array('class' => 'textbox', 'value' => $value['text_en']));
            $text_en->setLabel('Doprovodný text anglicky')->setRequired(true);  
            $price = new Zend_Form_Element_Text($i.'price', array('class' => 'textbox', 'value' => $value['price']));
            $price->setLabel('Cena:')->setRequired(true);  
            $this->addElements(array($setting_id, $public, $text_cz, $text_en, $price));
            $this->addDisplayGroup(array(        
                    $i.'public',
                    $i.'text_cz',
                    $i.'text_en',
                    $i.'price'
            ),$i.'group',array('legend' => $value['title_cz']));
            $group = $this->getDisplayGroup($i.'group');
            $group->setDecorators(array(        
                    'FormElements',
                    'Fieldset',
                    array('HtmlTag',array('tag'=>'div'))
            ));
        }
        
        $count = new Zend_Form_Element_Hidden('count');
        $count->setValue($i);
        $count->removeDecorator('label');
        $this->addElement($count);

        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "button bg-green border-style active"));
        $this->addElement($submit);
        
    }

}
