<?php

/**
 * Vytvoření uživatelských rolí guests a admin.
 * Nastavení jejich oprávnění, tj. na jaké stránky mají přístup.
 * @author Daniel Vála
 */
class Model_UserAcl extends Zend_Acl {

    function __construct() {
        // Roles
        $this->addRole(new Zend_Acl_Role('guest'));
        $this->addRole(new Zend_Acl_Role('editor'), 'guest');
        $this->addRole(new Zend_Acl_Role('admin'), 'editor');
        $this->addRole(new Zend_Acl_Role('superadmin'), 'admin');
        
        // Resources
        // Default module
        $this->add(new Zend_Acl_Resource('default'))                
                ->add(new Zend_Acl_Resource('default:index', 'default'))
                ->add(new Zend_Acl_Resource('default:session', 'default'))
                ->add(new Zend_Acl_Resource('default:cart', 'default'))
                ->add(new Zend_Acl_Resource('default:vlnene-deky', 'default'))
                ->add(new Zend_Acl_Resource('default:catalogue', 'default'))
                ->add(new Zend_Acl_Resource('default:kontakt', 'default'))
                ->add(new Zend_Acl_Resource('default:menu', 'default'))
                ->add(new Zend_Acl_Resource('default:napoje', 'default'))
                ->add(new Zend_Acl_Resource('default:obedy', 'default'))
                ->add(new Zend_Acl_Resource('default:pivo', 'default'))
                ->add(new Zend_Acl_Resource('default:gallery', 'default'))
                ->add(new Zend_Acl_Resource('default:mailinglist', 'default'))
                ->add(new Zend_Acl_Resource('default:sitemap', 'default'))
                ->add(new Zend_Acl_Resource('default:error', 'default'));         
        // Admin module
        $this->add(new Zend_Acl_Resource('admin'))                
                ->add(new Zend_Acl_Resource('admin:authentication', 'admin'))
                ->add(new Zend_Acl_Resource('admin:profile', 'admin'))
                ->add(new Zend_Acl_Resource('admin:inventory', 'admin'))
                ->add(new Zend_Acl_Resource('admin:index', 'admin'))
                ->add(new Zend_Acl_Resource('admin:news', 'admin'))
                ->add(new Zend_Acl_Resource('admin:error', 'admin'))
                ->add(new Zend_Acl_Resource('admin:orders', 'admin'))
                ->add(new Zend_Acl_Resource('admin:products', 'admin'))
                ->add(new Zend_Acl_Resource('admin:sales', 'admin'))                
                ->add(new Zend_Acl_Resource('admin:gallery', 'admin'))
                ->add(new Zend_Acl_Resource('admin:articles', 'admin'))
                ->add(new Zend_Acl_Resource('admin:mailinglist', 'admin'))                
                ->add(new Zend_Acl_Resource('admin:mail', 'admin'))             
                ->add(new Zend_Acl_Resource('admin:xmlparser', 'admin'))  
                
                ->add(new Zend_Acl_Resource('admin:categories', 'admin'))
                ->add(new Zend_Acl_Resource('admin:subcategories', 'admin'))
                ->add(new Zend_Acl_Resource('admin:settings', 'admin'))             
                ->add(new Zend_Acl_Resource('admin:countries', 'admin'))              
                ->add(new Zend_Acl_Resource('admin:rates', 'admin'))    
                ->add(new Zend_Acl_Resource('admin:shipping', 'admin'))
                ->add(new Zend_Acl_Resource('admin:payments', 'admin'))             
                ->add(new Zend_Acl_Resource('admin:texts', 'admin'));
        
        // SuperAdmin module
        $this->add(new Zend_Acl_Resource('superadminadmin'))
                ->add(new Zend_Acl_Resource('admin:users', 'admin'))
                ->add(new Zend_Acl_Resource('admin:overview', 'admin'))
                ->add(new Zend_Acl_Resource('admin:server', 'admin'));
        

        // Permission
        // Guests mohou jen na defaultní modul.    
        $this->allow('guest', 'default:index');    
        $this->allow('guest', 'default:session');   
        $this->allow('guest', 'default:cart');   
        $this->allow('guest', 'default:vlnene-deky');
        $this->allow('guest', 'default:catalogue');
        $this->allow('guest', 'default:kontakt');
        $this->allow('guest', 'default:menu');
        $this->allow('guest', 'default:gallery');
        $this->allow('guest', 'default:mailinglist');
        $this->allow('guest', 'default:sitemap');
        $this->allow('guest', 'default:error');
        $this->allow('guest', 'admin:xmlparser');

        // Admin mohou na defaultní i admin modul.
        $this->allow('editor', 'admin:authentication');
        $this->allow('editor', 'admin:profile');        
        $this->allow('editor', 'admin:inventory');
        $this->allow('editor', 'admin:index');  
        $this->allow('editor', 'admin:news'); 
        $this->allow('editor', 'admin:error'); 
        $this->allow('editor', 'admin:products');
        $this->allow('editor', 'admin:orders');
        $this->allow('editor', 'admin:sales');        
        $this->allow('editor', 'admin:mail');    
        
        //tohle se týká eshopu       
        $this->allow('admin', 'admin:mailinglist');  
        $this->allow('admin', 'admin:texts');
        $this->allow('admin', 'admin:gallery');
        $this->allow('admin', 'admin:categories');
        $this->allow('admin', 'admin:subcategories');      
        $this->allow('admin', 'admin:articles');     
        $this->allow('admin', 'admin:settings');               
        $this->allow('admin', 'admin:shipping');               
        $this->allow('admin', 'admin:payments');              
        $this->allow('admin', 'admin:rates');             
        $this->allow('admin', 'admin:countries');  
        
        //Superadmini mohou na users modul, kde mohou přidávat další uživatele s admin oprávněním
        $this->allow('superadmin', 'admin:overview');
        $this->allow('superadmin', 'admin:users');
        $this->allow('superadmin', 'admin:server');
        
    }

}
