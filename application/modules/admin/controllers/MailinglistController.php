<?php

class Admin_MailinglistController extends Zend_Controller_Action {
    
    private $adminSession = null;

    public function init() {
        $this->adminSession = new Zend_Session_Namespace('Admin');        
        if ($this->getParam('eshop_id')) {            
            $this->adminSession->eshop_id = $this->getParam('eshop_id');
        } else {
            $this->adminSession->eshop_id = 0;
        }
    }

    public function indexAction() {
        $mailmodel = new Model_DbTable_Mailinglist();
        if (!empty($this->adminSession->eshop_id)) {
            $where = "eshop_id = '".$this->adminSession->eshop_id."'";
        } else {
            $where = null;
        }
        $recipients = $mailmodel->fetchAll($where);
        $listform = new Form_Mailinglistform('/admin/mailinglist/save/');
        $mailform = new Form_Newsletterform('/admin/mailinglist/send/');

        $this->view->listform = $listform;
        $this->view->mailform = $mailform;
        $this->view->recipients = $recipients;
        $this->view->form = new Form_EshopSelectform();
    }

    public function saveAction() {
        $mailmodel = new Model_DbTable_Mailinglist();
        $address = $this->getParam('address');
        if (isset($address)) {
            //zkontrolujeme, zda už náhodou není adresa v databázi
            $entry = $mailmodel->fetchRow("mail = '$address'");
            if (isset($entry)) {
                //adresa je v databázi, ale je odhlášená od odběru
                if ($entry->status < 0) {
                    $data = array(
                        'status' => '5'
                    );
                    $where = "mail_id = '$entry->mail_id'";
                    $mailmodel->update($data, $where);
                    $this->view->message = "<h2>Adresa $address úspěšně obnovena (změněn status).</h2>";
                } else {
                    $this->view->message = "<h2>Tuto adresu ($address) již máme v databázi.</h2>";
                }
            } else {
                $data = array(
                    'mail' => $address,
                    'status' => '2',
                    'token' => md5($address . "a1b2c3")
                );
                $mailmodel->insert($data);
                $this->view->message = "<h2>Adresa $address úspěšně registrována. Děkujeme vám za důvěru.</h2>";
            }
        } else {
            $this->view->message = "<h2>Adresa není zadána správně.</h2>";
        }

        $this->indexAction();
        $this->render('index');
    }

    public function sendAction() {
        $today = date('Y-m-d');
        $subject = $this->getParam('subject');
        $text = $this->getParam('text');
        $mail       = new Zend_Mail('utf-8');
        $mailmodel = new Model_DbTable_Mailinglist();
        $recipients = $mailmodel->fetchAllActive();

        echo "<table class='admintable'>";
        echo "<th><tr>";
        echo "<td>email</td><td>naposledy odeslán</td><td>status</td>";
        echo "</tr></th>";
        foreach ($recipients as $value) {
            echo "<tr>";
            echo "<td>" . $value->mail . "</td>";
            echo "<td>" . $value->last_sent . "</td>";

            if ($today == $value->last_sent) {
                echo "<td>Email byl dnes již odeslán!</td>";
            } else {
                //$mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
                $mail->setSubject($subject);
                $mail->setFrom('info@balkanova.cz', 'Balkanova');
                //pro každého uživatele se generuje link na odhlášení zvlášť
                $link = "<br/><br/>Pokud již nechcete podobné nabídky přijímat, prosím klikněte na odkaz <a href='http://www.balkanova.cz/mailinglist/delete?token=$value->token'>[ODHLÁSIT]</a>";
                $mail->clearRecipients();
                $mail->addTo($value->mail);
                $mail->setBodyHtml($text . $link);
                try {
                    $mail->send();
                    $mailmodel->emailSent($value->mail_id);
                    echo "<td>Zend: Email odeslán!</td>";
                } catch (Exception $e) {
                    echo "<td>Zend: Něco selhalo!</td><pre>" . $e . "</pre>";
                }
            }
            echo "</tr>";
        }
        echo "</table>";
    }

    public function deleteAction() {
        $mailmodel = new Model_DbTable_Mailinglist();
        $data = array(
            'status' => '-2'
        );
        $where = "mail_id = '" . $this->getParam('mail_id') . "'";
        $mailmodel->update($data, $where);
        $this->_redirect('/admin/mailinglist/');
    }

}
