<?php
class Model_Currency extends Zend_Currency {

    private $currency = null;
    private $rate = null;
    
    function __construct() {        
        if (APP_LOCALE == 'cz') {
            $this->currency = new Zend_Currency('cs_CZ');
            $this->setFormat(array('currency' =>  'CZK', 'name' =>'koruna', 'symbol' => ' kč', 'precision' => 2, 'position' => Zend_Currency::RIGHT));
        } else {
            $this->currency = new Zend_Currency('de_DE');
            $this->setFormat(array('currency' =>  'EUR', 'name' =>'euro', 'symbol' => ' &euro;', 'precision' => 2, 'position' => Zend_Currency::RIGHT));
        }
    }
    
    public function toCurrency($input = null, array $options = Array()) {
        $output = $this->currency->toCurrency($input, $options); 
        return $output;
    }
    
    public function exchangeToEuroNumber($input = null) { 
        if (isset($input)) {
            $exchanged = round($input / $this->getExchangeRate(), 1, PHP_ROUND_HALF_UP);
            return $exchanged;
        } else {
            return null;
        }
    }
    
    public function exchangeToEuro($input = null, array $options = Array()) {         
        $exchanged = round($input / $this->getExchangeRate(), 1, PHP_ROUND_HALF_UP);
        $output = $this->toEuro($exchanged, $options); 
        return $output;
    }
    
    public function toKoruna($input = null, array $options = Array()) {
        $this->currency = new Zend_Currency('cs_CZ');
        $this->setFormat(array('currency' =>  'CZK', 'name' =>'koruna', 'symbol' => ' kč', 'precision' => 2, 'position' => Zend_Currency::RIGHT));
        $output = $this->currency->toCurrency($input, $options); 
        return $output;
    }
    
    public function toEuro($input = null, array $options = Array()) {
        $this->currency = new Zend_Currency('de_DE');
        $this->setFormat(array('currency' =>  'EUR', 'name' =>'euro', 'symbol' => ' &euro;', 'precision' => 2, 'position' => Zend_Currency::RIGHT));
        $output = $this->currency->toCurrency($input, $options); 
        return $output;
    }
    
    public function getExchangeRate() {
        if (empty($this->rate)) {
            $settings = new Model_DbTable_Settings();
            $this->rate = $settings->getExchangeRate();
        } 
        return $this->rate;
    }
}
