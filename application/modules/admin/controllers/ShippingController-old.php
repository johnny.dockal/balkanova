<?php
/*
 * 22.1.2014 doplněná funkce na tvorbu tokenů
 * 22.1.2014 upravené atributy controlleru $this->mailmodel
 */
class Admin_ShippingController extends Zend_Controller_Action {
    
    private $db = null;

    function preDispatch() {
        $auth = Zend_Auth::getInstance();
        if (!$auth->hasIdentity()) {
            $this->_redirect('admin/auth/login');
        }
    }
    
    public function init() {
        $this->db = Zend_Db_Table::getDefaultAdapter();
    }

    public function indexAction() {
        $sql = "SELECT * FROM eshop_languages";
        $stmt = $this->db->query($sql);
        $languages = $stmt->fetchAll();
        $sql = "SELECT id_price_set FROM eshop_deliveries_prices_sets GROUP BY id_price_set";
        $stmt = $this->db->query($sql);
        $tarifs = $stmt->fetchAll();

        $i = 0;
        $delivery_data = array();
        foreach ($languages as $value) {
            $sql = "SELECT eshop_deliveries.shipping_id, eshop_languages.id_language, eshop_languages.title AS language, eshop_deliveries_text_values.id, eshop_deliveries_text_values.title, eshop_deliveries_text_values.description, eshop_deliveries.package_price, eshop_deliveries.package_price_for_unit 
                    FROM eshop_deliveries
                    JOIN eshop_deliveries_text_values ON eshop_deliveries.shipping_id = eshop_deliveries_text_values.id_delivery
                    JOIN eshop_languages ON eshop_deliveries_text_values.id_language = eshop_languages.id_language 
                    WHERE eshop_languages.id_language = " . $value[id_language] . " ORDER BY shipping_id";
            $stmt = $this->db->query($sql);
            $values = $stmt->fetchAll();
            $delivery_data[$value[id_language]] = $values;
        }
        
        $this->view->delivery_data = $delivery_data;
    }
    
    public function paymentsAction() {
        $sql = "SELECT * FROM eshop_languages";
        $stmt = $this->db->query($sql);
        $languages = $stmt->fetchAll();
        $sql = "SELECT id_price_set FROM eshop_deliveries_prices_sets GROUP BY id_price_set";
        $stmt = $this->db->query($sql);
        $tarifs = $stmt->fetchAll();

        $i = 0;
        $payment_data = array();
        foreach ($languages as $value) {
            $sql = "SELECT eshop_payments.payment_id, eshop_languages.title AS language, eshop_payments_text_values.id, eshop_payments_text_values.title, eshop_payments_text_values.description, eshop_payments.price, eshop_payments.free_from
                    FROM eshop_payments
                    JOIN eshop_payments_text_values ON eshop_payments.payment_id = eshop_payments_text_values.id_payment       
                    JOIN eshop_languages ON eshop_payments_text_values.id_language = eshop_languages.id_language 
                    WHERE eshop_languages.id_language = " . $value[id_language];
            $stmt = $this->db->query($sql);
            $values = $stmt->fetchAll();
            $payment_data[$value[id_language]] = $values;
        }
        $this->view->payment_data = $payment_data;
    }

    public function ratesAction() {
        $sql = "SELECT * FROM eshop_languages";
        $stmt = $this->db->query($sql);
        $languages = $stmt->fetchAll();
        $sql = "SELECT id_price_set FROM eshop_deliveries_prices_sets GROUP BY id_price_set";
        $stmt = $this->db->query($sql);
        $tarifs = $stmt->fetchAll();

        $i = 0;
        $delivery_data = array();
        $payment_data = array();
        $tarif_data = array();
        $countries_data = array();
        foreach ($tarifs as $value) {
            $countries_data[$value[id_price_set]][id_price_set] = $value[id_price_set];
            $sql = "SELECT eshop_countries.country
                        FROM eshop_countries
                        JOIN eshop_country_delivery_payment ON eshop_countries.country_id = eshop_country_delivery_payment.id_country
                        JOIN eshop_languages ON eshop_countries.id_language = eshop_languages.id_language 
                        WHERE eshop_languages.id_language = 1
                        AND eshop_country_delivery_payment.id_price_set = " . $value[id_price_set] . "
                            GROUP BY eshop_countries.country_id";
            $stmt = $this->db->query($sql);
            $values = $stmt->fetchAll();
            $countries = array();
            foreach ($values as $v) {
                array_push($countries, $v['country']);
            }
            $countries_data[$value[id_price_set]][countries] = $countries;
            $sql = "SELECT eshop_deliveries.shipping_id, eshop_languages.id_language, eshop_deliveries_text_values.title, eshop_deliveries.package_price, eshop_deliveries.package_price_for_unit, eshop_deliveries_prices_sets.id, eshop_deliveries_prices_sets.id_price_set, eshop_deliveries_prices_sets.weight_from, eshop_deliveries_prices_sets.weight_till, eshop_deliveries_prices_sets.price, eshop_deliveries_prices_sets.free_from
                    FROM eshop_deliveries
                    JOIN eshop_deliveries_text_values ON eshop_deliveries.shipping_id = eshop_deliveries_text_values.id_delivery
                    JOIN eshop_languages ON eshop_deliveries_text_values.id_language = eshop_languages.id_language 
                    JOIN eshop_deliveries_prices_sets ON eshop_deliveries.shipping_id = eshop_deliveries_prices_sets.id_delivery
                    WHERE eshop_languages.id_language = 1
                    AND eshop_deliveries_prices_sets.id_price_set = " . $value[id_price_set];
            $stmt = $this->db->query($sql);
            $values = $stmt->fetchAll();
            $countries_data[$value[id_price_set]][prices] = $values;
        }
        $this->view->countries_data = $countries_data;
        $this->view->delivery_data = $this->getDeliveries();
    }

    public function countriesAction() {        
        $addedId = $this->_getParam('addedId');
        $sql = "SELECT  eshop_countries.country_id, 
                        eshop_countries.country, 
                        
                        eshop_deliveries_text_values.title AS delivery, 
                        
                        eshop_country_delivery_payment.id,
                        eshop_country_delivery_payment.id_price_set,
                        eshop_country_delivery_payment.status,
                        
                        eshop_payments_text_values.title AS payment,
                        
                        eshop_payments.price
                FROM eshop_countries
                JOIN eshop_country_delivery_payment ON eshop_countries.country_id = eshop_country_delivery_payment.id_country
                JOIN eshop_deliveries_text_values ON eshop_deliveries_text_values.id_delivery = eshop_country_delivery_payment.id_delivery
                JOIN eshop_payments ON eshop_payments.payment_id = eshop_country_delivery_payment.id_payment
                JOIN eshop_payments_text_values ON eshop_payments_text_values.id_payment = eshop_country_delivery_payment.id_payment
                WHERE eshop_countries.id_language = '1' AND eshop_deliveries_text_values.id_language = '1' AND eshop_payments_text_values.id_language = '1'
                ORDER BY eshop_countries.country_id, eshop_deliveries_text_values.id_delivery";
        $stmt = $this->db->query($sql);
        $countries_data = $stmt->fetchAll();
        //tohle je pro země, které se v prvním výběru nezobrazují, protože nemají zadané kombinace plateb a doručování
        $sql = "SELECT  eshop_countries.country_id, 
                        eshop_countries.country, 
                        eshop_country_delivery_payment.id_delivery
                FROM eshop_countries
                LEFT JOIN eshop_country_delivery_payment ON eshop_countries.country_id = eshop_country_delivery_payment.id_country
                WHERE eshop_countries.id_language = '1' AND eshop_country_delivery_payment.id_delivery IS NULL
                ORDER BY eshop_countries.country_id";
        $stmt = $this->db->query($sql);
        $countries_extra_data = $stmt->fetchAll();
        
        $this->view->addedId = $addedId;
        $this->view->countries_data = $countries_data;
        $this->view->countries_extra_data = $countries_extra_data;
        $this->view->payments_data = $this->getPayments();
        $this->view->deliveries_data = $this->getDeliveries();
        $this->view->rates_data = $this->getRates();
    }

    public function saveAction() {
        $id = $this->_getParam('id');
        $price = $this->_getParam('price');
        $title = $this->_getParam('title');
        $description = $this->_getParam('description');
        $redirect = $this->_getParam('redirect');
        $id_payment = $this->_getParam('id_payment');
        $save = $this->_getParam('save');
                
        if ($save == 'rate') {
           $sql = "UPDATE eshop_deliveries_prices_sets SET price = '$price' WHERE id = '$id'"; 
           $stmt = $this->db->query($sql);
        } else if ($save == 'payment') {
           $sql = "UPDATE eshop_payments SET price = '$price' WHERE id_payment = '$id_payment'"; 
           $stmt = $this->db->query($sql);
           $sql = "UPDATE eshop_payments_text_values SET title = '$title', description = '$description' WHERE id = '$id'"; 
           $stmt = $this->db->query($sql);
        } else if ($save == 'delivery') {
           $sql = "UPDATE eshop_deliveries_text_values SET title = '$title', description = '$description' WHERE id = '$id'"; 
           $stmt = $this->db->query($sql);
        }        
        $stmt = $db->query($sql);
        $this->_redirect("/admin/shipping/$redirect");
    }
    
    public function savecountriesAction() {
        $id_country = $this->_getParam('id_country');
        $id_payment = $this->_getParam('id_payment');
        $id_delivery = $this->_getParam('id_delivery');
        $id_price_set = $this->_getParam('id_price_set');
        
        if (isset($id_country) && isset($id_payment) && isset($id_delivery) && isset($id_price_set)) {
            $id = $this->_getParam('combinationId');
            $sql = "INSERT INTO eshop_country_delivery_payment VALUES ('', '$id_country', '$id_delivery', '$id_price_set', '$id_payment', '1')";
            $stmt = $this->db->query($sql);
            $result = $this->db->fetchRow("SELECT MAX(id) FROM eshop_country_delivery_payment");
            $this->_redirect("/admin/shipping/countries/?addedId=".$result['MAX(id)']);
        } else {
            echo "Některý z parametrů nebyl nastaven.";
        }
    }
    
    public function savepaymentAction() {
        $title1 = $this->_getParam('title1');
        $title2 = $this->_getParam('title2');
        $price = $this->_getParam('price');
        $description1 = $this->_getParam('description1');
        $description2 = $this->_getParam('description2');
        
        $result = $this->db->fetchRow("SELECT MAX(id_payment) FROM eshop_payments");
        $id_payment = $result['MAX(id_payment)'] + 1; 
        
        $sql = "INSERT INTO eshop_payments VALUES ('$id_payment', '$price', '0.00')";
        $stmt = $this->db->query($sql);
        $sql = "INSERT INTO eshop_payments_text_values VALUES ('', '$id_payment', '1', '$title1', '$description1')";
        $stmt = $this->db->query($sql);
        $sql = "INSERT INTO eshop_payments_text_values VALUES ('', '$id_payment', '2', '$title2', '$description2')";
        $stmt = $this->db->query($sql);
        $this->_redirect("/admin/shipping/payments/");        
    }
    
    public function savecountryAction() {
        $title1 = $this->_getParam('title1');
        $title2 = $this->_getParam('title2');
        
        $db = Zend_Registry::get('db');
        $result = $this->db->fetchRow("SELECT MAX(id_country) FROM eshop_countries");
        $id_country = $result['MAX(id_country)'] + 1; 
        
        $sql = "INSERT INTO eshop_countries VALUES ('$id_country', '1', '$title1', '1')";
        $stmt = $this->db->query($sql);
        $sql = "INSERT INTO eshop_countries VALUES ('$id_country', '2', '$title2', '1')";
        $stmt = $this->db->query($sql);
        $this->_redirect("/admin/shipping/countries/");        
    }
    
    public function saverateAction() {
        $id_price_set   = $this->_getParam('id_price_set');
        $id_delivery    = $this->_getParam('id_delivery');
        $weight_from    = $this->_getParam('weight_from');
        $weight_till    = $this->_getParam('weight_till');
        $price          = $this->_getParam('price');
        
        $sql = "INSERT INTO eshop_deliveries_prices_sets VALUES ('', '$id_delivery', '$id_price_set', '$weight_from', '$weight_till', '$price', '0.00')";
        $stmt = $this->db->query($sql);
        $this->_redirect("/admin/shipping/rates/");        
    }
    
    public function activateAction() {
        $id = $this->_getParam('combinationId');
        $sql = "UPDATE eshop_country_delivery_payment SET status = '1' WHERE id = '$id'";
        $stmt = $this->db->query($sql);
        $this->_redirect("/admin/shipping/countries/");
    }
    
    public function deactivateAction() {
        $id = $this->_getParam('combinationId');      
        $sql = "UPDATE eshop_country_delivery_payment SET status = '0' WHERE id = '$id'";
        $stmt = $this->db->query($sql);
        $this->_redirect("/admin/shipping/countries/");
    }
    
    public function deletecountryAction() {
        $id = $this->_getParam('countryId');     
        $sql = "DELETE FROM eshop_countries WHERE country_id = '$id'";
        $stmt = $this->db->query($sql);
        $this->_redirect("/admin/shipping/countries/");
    }
    
    public function deleteAction() {
        $id = $this->_getParam('combinationId');       
        $sql = "DELETE FROM eshop_country_delivery_payment WHERE id = '$id'";
        $stmt = $this->db->query($sql);
        $this->_redirect("/admin/shipping/countries/");
    }
    
    private function getPayments() {       
        $sql = "SELECT * FROM eshop_payments JOIN eshop_payments_text_values ON eshop_payments.payment_id = eshop_payments_text_values.id_payment WHERE eshop_payments_text_values.id_language = '1'";
        $stmt = $this->db->query($sql);
        $payments_data = $stmt->fetchAll();
        return $payments_data;
    }
    
    
    
    private function getRates() {       
        $sql = "SELECT id_price_set FROM eshop_deliveries_prices_sets GROUP BY id_price_set";
        $stmt = $this->db->query($sql);
        $rates_data = $stmt->fetchAll();
        return $rates_data;
    }
}