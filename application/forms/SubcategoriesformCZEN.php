<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_SubcategoriesformCZEN extends Zend_Form {

    public function __construct($action = null, $options = null, $lang1 = null, $id = null) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('class', 'admintable');
        
        //momentálně je formulář nastaven na dva jazyky (cz a en), nevím jak ho upravit pro libovolný počet jazyků z configu
        $subcategory_id = new Zend_Form_Element_Hidden('subcategory_id');
        $subcategory_id->removeDecorator('Label');
        
        $sequence = new Zend_Dojo_Form_Element_TextBox('sequence', array('class' => 'textboxwide'));
        $sequence->setLabel('Sekvence:')->setRequired(true);
        
        $public = new Zend_Form_Element_Radio('public');
        $public->addMultiOptions(array(
	               '0'    => 'Nezveřejněný',
	               '1'     => 'Veřejný - zobrazuje se na stránkách'));
        $public->setLabel('Status:')->setRequired(true);
        
        $category_id = new Zend_Form_Element_Select('category_id', array('class' => 'textboxwide'));
        $category_id->setLabel('Kategorie:');
        if (isset($options)) {
            foreach ($options as $value) {
               $category_id->addMultiOption($value['category_id'], $value['title_cz']);
            }
        }    
        
        $title_cz = new Zend_Dojo_Form_Element_TextBox('title_cz', array('class' => 'textboxwide'));
        $title_cz->setLabel('Nadpis '.$lang1.':')->setRequired(true);
        $alias_cz = new Zend_Dojo_Form_Element_TextBox('url_cz', array('class' => 'textboxwide'));
        $alias_cz->setLabel('URL alias '.$lang1.':')->setRequired(true);
        $text_cz = new Zend_Dojo_Form_Element_Textarea('text_cz', array('class' => "textboxbig"));
        $text_cz->setLabel('Text '.$lang1.':')->setRequired(true);
        
        $title_en = new Zend_Dojo_Form_Element_TextBox('title_en', array('class' => 'textboxwide'));
        $title_en->setLabel('Nadpis anglicky:')->setRequired(true);
        $alias_en = new Zend_Dojo_Form_Element_TextBox('url_en', array('class' => 'textboxwide'));
        $alias_en->setLabel('URL alias anglicky:')->setRequired(true);
        $text_en = new Zend_Dojo_Form_Element_Textarea('text_en', array('class' => "textboxbig"));
        $text_en->setLabel('Text anglicky:')->setRequired(true);

        $config = Zend_Registry::get('config');
        if ($config->project == 'balkanova') {        
            $full_title_cz = new Zend_Dojo_Form_Element_TextBox('full_title_cz', array('class' => 'textboxwide'));
            $full_title_cz->setLabel('Dlouhý nadpis '.$lang1.':')->setRequired(true);
            $full_title_en = new Zend_Dojo_Form_Element_TextBox('full_title_en', array('class' => 'textboxwide'));
            $full_title_en->setLabel('Dlouhý nadpis anglicky:')->setRequired(true);
            
            $maintainance_cz = new Zend_Dojo_Form_Element_Textarea('maintainance_cz', array('class' => "textboxbig"));
            $maintainance_cz->setLabel('Údržba '.$lang1.':')->setRequired(true);
            $maintainance_en = new Zend_Dojo_Form_Element_Textarea('maintainance_en', array('class' => "textboxbig"));
            $maintainance_en->setLabel('Údržba anglicky:')->setRequired(true);
            
            $softness = new Zend_Dojo_Form_Element_TextBox('softness', array('class' => 'textboxwide'));
            $softness->setLabel('Měkkost (kolik peříček):')->setRequired(true);
            
            $rowdisplay = new Zend_Dojo_Form_Element_TextBox('rowdisplay', array('class' => 'textboxwide'));
            $rowdisplay->setLabel('Kolik se zobrazí řad výrobků po dvou nalevo od textu, než se začnou zobrazovat řady po čtyřech přes celou stránku:')->setRequired(true);
        }
        
        $image = new Zend_Form_Element_File('image');
        $image->setLabel('Obrázek subkategorie (velikost 190*165pixelů):');
        $image->addValidator('Count', false, 1);
        //$image->addValidator('Size', false, 10240000);
        $image->addValidator('Extension', false, 'jpg,JPG');
        if (isset($id)) {
        $image->addDecorators(array(
                array(array("img" => "HtmlTag"), array(
                        "tag" => "img",
                        "openOnly" => true,
                        "src" => "/images/eshop_subcategories/$id.jpg",
                        "align" => "middle",
                        "class" => ""
                    )),
                array(array("span" => "HtmlTag"), array(
                        "tag" => "span",
                        "class" => "myElement"
                    ))
        ));
        }
        
        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Uložit", 'class' => "savebutton"));
        
        if ($config->project == 'balkanova') {
            $this->addElements(array(
                $subcategory_id, 
                $category_id, 
                $sequence, 
                $public, 
                $softness, 
                $rowdisplay, 
                $title_cz, 
                $alias_cz, 
                $full_title_cz, 
                $text_cz, 
                $maintainance_cz, 
                $title_en, 
                $alias_en,
                $full_title_en, 
                $text_en, 
                $maintainance_en,
                $image,
                $submit
            ));
        } else {
            $this->addElements(array(
                $subcategory_id, 
                $category_id, 
                $sequence, 
                $public, 
                $title_cz, 
                $alias_cz, 
                $text_cz, 
                $title_en, 
                $alias_en, 
                $text_en, 
                $submit
            ));
        }
    }
}
