<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_OrderStep3form extends Zend_Form {

    public function __construct($paymentOptions, $deliveryCount = 1, $totalPrice = null) {
        parent::__construct();
        $this->setMethod('POST')->setName('formorderstep3')->setAction('/cart/order/step/4/');
        $this->setAttrib('class', 'formorder');  
        //načteme view, ve kterém jsou již připravené texty v patřičném jazyce        
        $view = Zend_Layout::getMvcInstance()->getView();  
        
        $payment_options_formatted = array();
        foreach ($paymentOptions as $value) {
            $note = "";
            //ochcávka kvůli cenám v německu, teď se to musí převést do eura z korun
            if (APP_ID == 2) {
                $price = $view->currency->exchangeToEuroNumber($value['price']);
            } else {
                $price = $value['price'];
            }
            if (!empty($value['percentage'])) { 
                $priceString = "+".$value['percentage']."%";
                if (isset($totalPrice)) {
                    $percent = ($totalPrice / 100) * $value['percentage'];
                    $percent = round($percent, 0, PHP_ROUND_HALF_UP);
                $priceString .= " $view->str_order_percent_of ". $view->currency->toCurrency($totalPrice)." = ".$view->currency->toCurrency($percent);
                }
            } else if (($deliveryCount > 1) && ($price > 0)) {
                $sum = $deliveryCount * $price;
                $priceString = $deliveryCount . "x " . $price . " = +" . $sum;
                $note = "<p class='order-description'>*Vaše objednávka bude rozdělena do $deliveryCount balíčků.</p>"; 
            } else if ($price > 0) {
                $priceString = $view->currency->toCurrency($price);
            } else {
                $priceString = $view->str_order_free;
            }
            if ($value['public'] == 1) {
                $payment_options_formatted[$value['payment_id']] = "&nbsp;<span class='order-title'><strong>" . $value['title'] . ": <span class='yellow'>" . $priceString . "</span> </strong></span><div class='order-description'>" . $value['text'] . "</div>";
            }
        }
        $payment = new Zend_Form_Element_Radio('payment_id', array(
                    'multiOptions' => $payment_options_formatted,
                    'escape' => false,
                    'required' => true
                ));
        $payment->setLabel($view->str_order_select_payment); 
        
        $message = new Zend_Form_Element_Textarea('order_message', array('style' => 'height: 100px; width: 98%'));
        $message->setAttrib('maxlength','1000');  
        $message->setLabel($view->str_order_message);   
        
        $back = new Zend_Form_Element_Submit('cartbuttonprev', array('label' => $view->form_back, 'class' => "button button-white floatleft"));
        $back->removeDecorator('DtDdWrapper');
        $submit = new Zend_Form_Element_Submit('cartbuttonnext', array('label' => $view->form_summary, 'class' => "button button-yellow floatright"));
        $submit->removeDecorator('DtDdWrapper');

        $this->addElements(array($payment, $message, $submit, $back));
    }

}
