<?php

/**
 * Plugin kontroluje podle pravidel acl oprávnění.
 * Při pokusu o zobrazení neoprávněné stránky přesměruje na přihlašovací formulář.
 * @author Daniel Vála
 */
class Plugin_AccessCheck extends Zend_Controller_Plugin_Abstract {

    private $_acl = null;

    /**
     * Konstruktor si uloží Zend_Acl instanci.
     * @param Zend_Acl $acl acl
     */
    public function __construct(Zend_Acl $acl) {        
        $this->_acl = $acl;
    }

    /**
     * Při pokusu o zobrazení neoprávněné stránky přesměruje na přihlašovací formulář.
     * @param Zend_Controller_Request_Abstract $request request
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        $action = $request->getActionName();
        // Pokud není povolen přístup, přesměruje.
        if (!$this->_acl->isAllowed(Zend_Registry::get('user_role'), $module . ':' . $controller, $action)) {
            $request->setModuleName('admin')->setControllerName('authentication')->setActionName('login');
        }
    }

}
