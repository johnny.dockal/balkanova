<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Skipform extends Zend_Form {

    public function __construct($action) {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction($action);
        $this->setAttrib('class', 'admintable');
        $this->setAttrib('enctype', 'multipart/form-data');
        
        $status_id = new Zend_Form_Element_Hidden('status_id');
        //zabrání zobrazení labelu
        $status_id->setDecorators(array('ViewHelper'));
        
        $skipmail = new Zend_Form_Element_Hidden('mailaction');
        $skipmail->setValue('skip');
        //zabrání zobrazení labelu
        $skipmail->setDecorators(array('ViewHelper'));
        
        $order_id = new Zend_Form_Element_Hidden('order_id');
        //zabrání zobrazení labelu
        $order_id->setDecorators(array('ViewHelper'));
        
        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Přeskočit odesílání mailu", 'class' => "savebutton"));

        $this->addElements(array(
            $order_id, $status_id, $skipmail, $submit
        ));
        
    }
}
