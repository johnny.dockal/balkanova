<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_OrderStep1form extends Zend_Form {

    public function __construct($options) {
        parent::__construct();
        $this->setMethod('POST')->setName('formorderstep1')->setAction('/cart/order/step/2/');
        $this->setAttrib('class', 'formorder');
        //načteme view, ve kterém jsou již připravené texty v patřičném jazyce        
        $view = Zend_Layout::getMvcInstance()->getView();

        $step = new Zend_Form_Element_Hidden('current_step', array('value' => '1'));
        $step->setDecorators(array('ViewHelper'));

        $order_name = new Zend_Dojo_Form_Element_TextBox('order_name', array('class' => 'textbox'));
        $order_name->setAttrib('maxlength', '255');
        $order_name->setLabel($view->form_name)->setRequired(true);

        $order_surname = new Zend_Dojo_Form_Element_TextBox('order_surname', array('class' => 'textbox'));
        $order_surname->setAttrib('maxlength', '255');
        $order_surname->setLabel($view->form_surname)->setRequired(true);

        $order_company_name = new Zend_Dojo_Form_Element_TextBox('order_company_name', array('class' => 'textbox'));
        $order_company_name->setAttrib('maxlength', '255');
        $order_company_name->setLabel($view->form_order_company_name)->setRequired(true);

        $order_company_id = new Zend_Dojo_Form_Element_TextBox('order_company_id', array('class' => 'textbox'));
        $order_company_id->setAttrib('maxlength', '255');
        $order_company_id->setLabel($view->form_order_company_id)->setRequired(true);

        $order_phone = new Zend_Dojo_Form_Element_TextBox('order_phone', array('class' => "textbox", 'value' => '+420'));
        $order_phone->setAttrib('maxlength', '16');
        $order_phone->setLabel($view->form_phone)->setRequired(true);

        $order_email = new Zend_Dojo_Form_Element_TextBox('order_email', array('class' => 'textbox', 'value' => '@'));
        $order_email->setAttrib('maxlength', '255');
        $order_email->setLabel($view->form_email)->setRequired(true);

        $order_address = new Zend_Dojo_Form_Element_TextBox('order_address', array('class' => "textbox"));
        $order_address->setAttrib('maxlength', '255');
        $order_address->setLabel($view->form_address)->setRequired(true);

        $order_city = new Zend_Dojo_Form_Element_TextBox('order_city', array('class' => "textbox"));
        $order_city->setAttrib('maxlength', '255');
        $order_city->setLabel($view->form_city)->setRequired(true);

        $order_zip = new Zend_Dojo_Form_Element_TextBox('order_zip', array('class' => "textbox"));
        $order_zip->setAttrib('maxlength', '6');
        $order_zip->setLabel($view->form_zip)->setRequired(true);

        $country_id = new Zend_Form_Element_Select('country_id', array('class' => 'textbox'));
        $country_id->setLabel($view->form_country)->setRequired(true);
        foreach ($options as $value) {
            $country_id->addMultiOption($value['country_id'], $value['title']);
        }

        if ($this->confirm_adult) {
            $order_adult = new Zend_Form_Element_Checkbox('order_adult', array('class' => 'float-left', 'value' => '1'));
            $order_adult->setLabel($view->form_order_adult )->setRequired(true)->setChecked(false);
        }

        $order_agree = new Zend_Form_Element_Checkbox('order_agree', array('class' => 'float-left', 'value' => '1'));
        $order_agree->setLabel($view->str_agree2)->setRequired(true)->setChecked(false);

        $order_company = new Zend_Form_Element_Checkbox('order_company', array('class' => 'float-left', 'value' => '1', 'onchange' => 'showComform(this)'));
        $order_company->setLabel($view->form_order_company)->setRequired(true)->setChecked(false);

        $order_diff = new Zend_Form_Element_Checkbox('order_diff', array('class' => 'float-left', 'value' => '1', 'onchange' => 'showDiffform(this)'));
        $order_diff->setLabel($view->form_order_diff)->setRequired(true)->setChecked(false);

        $order_sendmail = new Zend_Form_Element_Checkbox('order_sendmail', array('class' => 'float-left', 'value' => '1'));
        $order_sendmail->setLabel($view->form_order_sendmail)->setRequired(true)->setChecked(false);

        $order_diff_name = new Zend_Dojo_Form_Element_TextBox('order_diff_name', array('class' => 'textbox'));
        $order_diff_name->setAttrib('maxlength', '255');
        $order_diff_name->setLabel($view->form_name)->setRequired(false);

        $order_diff_surname = new Zend_Dojo_Form_Element_TextBox('order_diff_surname', array('class' => 'textbox'));
        $order_diff_surname->setAttrib('maxlength', '255');
        $order_diff_surname->setLabel($view->form_surname)->setRequired(false);

        $order_diff_address = new Zend_Dojo_Form_Element_TextBox('order_diff_address', array('class' => 'textbox'));
        $order_diff_address->setAttrib('maxlength', '255');
        $order_diff_address->setLabel($view->form_address)->setRequired(false);

        $order_diff_city = new Zend_Dojo_Form_Element_TextBox('order_diff_city', array('class' => 'textbox'));
        $order_diff_city->setAttrib('maxlength', '255');
        $order_diff_city->setLabel($view->form_city)->setRequired(false);

        $order_diff_zip = new Zend_Dojo_Form_Element_TextBox('order_diff_zip', array('class' => 'textbox'));
        $order_diff_zip->setAttrib('maxlength', '255');
        $order_diff_zip->setLabel($view->form_zip)->setRequired(false);

        $back = new Zend_Form_Element_Submit('cartbuttonprev', array('label' => $view->form_back, 'class' => 'button button-white floatleft cufon'));
        $back->removeDecorator('DtDdWrapper');
        $submit = new Zend_Form_Element_Submit('cartbuttonnext', array('label' => $view->form_select_shipping, 'class' => 'button button-yellow floatright cufon'));
        $submit->removeDecorator('DtDdWrapper');

        $this->addElement($order_company);
        $this->addDisplayGroup(array($order_name, $order_surname), 'nameform');
        $this->addDisplayGroup(array($order_company_name, $order_company_id), 'companyform');
        $this->addElements(array(
            $order_company, $order_phone, $order_email, $order_address, $order_city, $order_zip, $country_id, $order_diff
        ));
        $this->addDisplayGroup(array($order_diff_name, $order_diff_surname, $order_diff_address, $order_diff_city, $order_diff_zip), 'diffform');
        $this->addElements(array(
            $order_sendmail, $order_agree, $submit, $back
        ));
        $diffform = $this->getDisplayGroup('diffform');
        $diffform->setLegend($view->form_fill_shipping);
        $this->setDisplayGroupDecorators(array(
            'FormElements',
            'Fieldset',
        ));
    }

}
