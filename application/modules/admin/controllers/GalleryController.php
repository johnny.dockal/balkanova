<?php

class Admin_GalleryController extends Zend_Controller_Action
{

    public function init() {
        $this->view->headTitle('Galerie', 'POSTEND');
    }

    public function indexAction() {
        $gallery = new Model_ImageHandler();
        $imgFiles = $gallery->fetchGalleryImages();
        $this->view->imgFiles = $imgFiles;
    }
        
    public function saveAction() {
        $path = getcwd().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'gallery';
        if (isset($_FILES['img1']['name'])) {
            $filename1 = basename($_FILES['img1']['name']);
            $target_path = $path.DIRECTORY_SEPARATOR.$filename1;
            echo $target_path."<br/>";

            if (move_uploaded_file($_FILES['img1']['tmp_name'], $target_path)) {
                echo "The file $filename1 has been uploaded";
                $this->view->img1 = $filename1;
            } else{
                echo "Chyba při načítání obrázku, zkus to znovu!";
            }
        }

        if (isset($_FILES['img2']['name'])) {
            $filename2 = basename($_FILES['img2']['name']);
            $target_path = $path.DIRECTORY_SEPARATOR.$filename2;
            echo $target_path."<br/>";

            if (move_uploaded_file($_FILES['img2']['tmp_name'], $target_path)) {
                echo "The file $filename2 has been uploaded";
                $this->view->img2 = $filename2;
            } else{
                echo "Chyba při načítání obrázku, zkus to znovu!";
            }
        }

        if (isset($_FILES['img3']['name'])) {
            $filename3 = basename($_FILES['img3']['name']);
            $target_path = $path.DIRECTORY_SEPARATOR.$filename3;
            echo $target_path."<br/>";

            if (move_uploaded_file($_FILES['img3']['tmp_name'], $target_path)) {
                echo "The file $filename1 has been uploaded";
                $this->view->img3 = $filename3;
            } else{
                echo "Chyba při načítání obrázku, zkus to znovu!";
            }
        }
        $this->_redirect('/admin/gallery/');
    }
    
    public function deleteAction() {
        $images = $this->getParam('images');
        $path = getcwd().DIRECTORY_SEPARATOR.'img';
        foreach ($images as $v) {
            unlink($path.DIRECTORY_SEPARATOR.$v);
        }
        $this->_redirect('/admin/gallery/');
    }


}

