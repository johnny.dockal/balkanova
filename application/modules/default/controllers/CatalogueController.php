<?php

class Default_CatalogueController extends Zend_Controller_Action {

    private $category_id = null;
    private $subcategory_id = null;
    private $lang = null;

    function init() {
        $cat = new Model_URLParser();
        $this->catalogue = $cat->parseUrl($this->_getParam('url'));
        //var_dump($this->catalogue);
        
        $session = new Zend_Session_Namespace('Default');
        $this->lang = $session->lang;

        //pokud dostanu 404 nelze načíst zbylá data a je nutné volat render        
        if ($this->catalogue["page"] == "page404") {
            throw new Zend_Controller_Action_Exception('Not Found', 404);
            return;
        }
        $this->category_id = $this->catalogue["category_id"];
        if ($this->catalogue["page"] == "subcategory") {
            $this->subcategory_id = $this->catalogue["subcategory_id"];
        }
        $this->view->breadcrumbs_url = $this->catalogue["breadcrumbs_url"];
        $this->view->breadcrumbs_title = $this->catalogue["breadcrumbs_title"];
        $subcatModel = new Model_DbTable_EshopSubCategories();
        $softnessCatId = (APP_LOCALE == 'cz') ? '15' : '16';
        $this->view->softness = $subcatModel->fetchSubCategories($softnessCatId);
        
        //Pokud se jedná o kilimy, tak se musí vybrat pouze ty, co nejsou v katalogu, tedy subkategorie
        
        /*if (($this->category_id == '3') && empty($this->subcategory_id)) {
            $this->subcategory_id = 9;
        } else if ($this->category_id == '12' && empty($this->subcategory_id)){            
            $this->subcategory_id = 49;
        } */
        
        //pokud máme ID SUBkategorie
        if (!empty($this->subcategory_id)) {
            $subcat = $subcatModel->fetchSubCategories(null, $this->subcategory_id);
            $this->view->subcat = $subcat;
            $this->view->headimg = "/images/eshop_subcategories/" . $this->subcategory_id . "_$this->lang.gif";
            $this->view->aboutimg = "/images/eshop_subcategories/" . $this->category_id . "_about_$this->lang.gif";
            $this->view->rowdisplay = $subcat['rowdisplay'];
            if (empty($subcat['subcat_full_title'])) {
                $this->view->title = $subcat['subcat_title'];
            } else {
                $this->view->title = $subcat['subcat_full_title'];
            }
        } else if (!empty($this->category_id)) {
            $catinfo = $this->catalogue["category"];
            $this->view->category = $catinfo;
            $this->view->rowdisplay = $catinfo['rowdisplay'];
            if (empty($catinfo['full_title'])) {
                $this->view->title = $catinfo['title'];
            } else {
                $this->view->title = $catinfo['full_title'];
            }
        } else {
            echo "Fatal Error: No category or subgategory ID set!";
        }
    }

    function indexAction() {
        if ($this->catalogue["page"] == "product") {
            $this->showProduct();
        } elseif (!empty($this->subcategory_id)) {
            $this->showSubcategory();
        } else if (!empty($this->category_id)) {
            $this->showCategory();
        }        
    }

    private function showProduct() {   
        $product = $this->catalogue["product"];
        $this->view->title = $product->getFullTitle();
        $this->view->product = $product;
        $this->view->hair = $this->view->grams;
        $this->view->hairunit = $this->view->grams1;
        $this->renderScript('products/index.phtml');
    }

    private function showCategory() {   
        $subcatModel = new Model_DbTable_EshopSubCategories();
        $this->view->data = $subcatModel->fetchSubCategories($this->category_id);     
        $productModel = new Model_DbTable_EshopProducts();
        if ($this->category_id == '3') {
            $this->view->products = $productModel->fetchProductsBySubcat('9');
        } else if ($this->category_id == '17') {//17 katalog kilimů český jazyk
            $this->view->products = $productModel->fetchProductsBySubcat('17', 'title');
        } else if ($this->category_id == '18') {//18 katalog kilimů německý jazyk
            $this->view->products = $productModel->fetchProductsBySubcat('50', 'title');
        } else {
            $this->view->products = $productModel->fetchProductsByCat($this->category_id);
        }
        switch ($this->category_id) {
            case 1:
                $this->view->udrzbaImg = "udrzba_1_$this->lang.gif";
                $this->view->formbutton = null;
                $this->view->note = null;
                $this->view->hair = $this->view->grams;
                $this->view->hairunit = $this->view->grams1;
                $this->render('deky');
                break;
            case 2:
                $this->view->udrzbaImg = "udrzba_0_$this->lang.gif";
                $this->view->formbutton = $this->view->custom;
                $this->view->note = null;
                $this->view->hair = $this->view->pile;
                $this->view->hairunit = $this->view->pile1;
                $session = new Zend_Session_Namespace('Default');
                if (APP_LOCALE == 'de') {
                    $this->render('teppiche');
                } else {
                    $this->render('koberce');
                }
                break;
            case 3:
            case 12:   
                $this->view->udrzbaImg = "udrzba_0_$this->lang.gif";
                $this->view->formbutton = $this->view->ask;
                $this->view->note = null;
                $this->view->hair = null;
                $this->view->hairunit = null;
                $this->view->rowdisplay = 0;
                $this->render('kilimy');                
                break;
            case 4:
                $this->view->udrzbaImg = "udrzba_0_$this->lang.gif";
                $this->view->formbutton = $this->view->custom;
                $this->view->note = null;
                $this->view->hair = null;
                $this->view->hairunit = null;
                $this->view->rowdisplay = 3;
                $this->render('chaliste');
                break;
            case 5:
                $this->view->udrzbaImg = "udrzba_0_$this->lang.gif";
                $this->view->formbutton = $this->view->ask;
                $this->view->note = $this->view->size;
                $this->view->hair = null;
                $this->view->hairunit = null;
                $this->render('doplnky');
                break;
            case 6:
                $this->view->udrzbaImg = "udrzba_soupravy_$this->lang.gif";
                $this->view->formbutton = $this->view->ask;
                $this->view->note = $this->view->size;
                $this->view->hair = $this->view->grams;
                $this->view->hairunit = $this->view->grams1;
                $this->render('velkoobchod');
                break;
            case 11:
                $this->view->udrzbaImg = "udrzba_1_$this->lang.gif";
                $this->view->formbutton = $this->view->ask;
                $this->view->note = $this->view->size;
                $this->view->hair = $this->view->grams;
                $this->view->hairunit = $this->view->grams1;
                $this->render('chaliste');
                break;    
            case 17:
            case 18:
                $this->view->rowdisplay = 0;
                $this->render('kilimy-katalog');
                break;
            default:
                $this->view->hair = $this->view->grams;
                $this->view->hairunit = $this->view->grams1;
                $this->render('category');
        }
    }

    private function showSubcategory() {
        $subcatModel = new Model_DbTable_EshopSubCategories();
        $this->view->data = $subcatModel->fetchSubCategories(null, $this->subcategory_id);
        //Switch pro speciální případy na SELECT pro české stránky
        $modelProducts = new Model_DbTable_EshopProducts();
        $products = $modelProducts->fetchProductsBySubcat($this->subcategory_id);
        $this->view->formbutton = $this->view->str_ask;
        $this->view->note = null;
        $this->view->hair = null;
        $this->view->products = $products;
        switch ($this->subcategory_id) {
            //Toto se vztahuje na katalog kilim (ID 17 na českých, ID 50 na německých stránkách)
            case 17:
            case 50:                
                $this->render('kilimy-katalog');
                break;
            //látky se vzorem
            case 41:
            case 42:
                $this->view->modification = 1;
                $this->render('subcategory');
                break;
            case 48:
                //Toto se vztahuje na katalog kilim (ID 17, speciální případ)
                $this->render('koberce');
                $this->view->hair = $this->view->pile;
                $this->view->hairunit = $this->view->pile1;
                break;
            case 9:
            case 49:
                //Toto se vztahuje na katalog kilim (ID 17, speciální případ)
                $this->render('kilimy');
                break;
            default:
                $this->render('subcategory');
                break;
        }
    }

}

?>