<?php

/**
 * Index controller pro administrátorské rozhraní.
 *
 * @package admin
 * @author Daniel Vála
 */
class Admin_NewsController extends Zend_Controller_Action {

    public function init() {
        $this->adminSession = new Zend_Session_Namespace('Admin');        
        if ($this->getParam('eshop_id')) {            
            $this->adminSession->eshop_id = $this->getParam('eshop_id');
        } else {
            $this->adminSession->eshop_id = 0;
        }
        $modelNews = new Model_DbTable_News();
        $news = $modelNews->fetchNewsAll($this->adminSession->eshop_id);
        $this->view->news = $news;
        $this->view->headTitle('Administrace', 'POSTEND'); 
    }

    public function indexAction() {
        $this->view->form = new Form_EshopSelectform();        
    }
    
    public function newAction() {
        $form = new Form_NewsformCZ('/admin/news/save/');
        $form->setDefault('eshop_select', 3);
        $this->view->form = $form;
    }
    
    public function editAction() {         
        $news_id = $this->getParam('news_id'); 
        if (isset($news_id)) {
            $model = new Model_DbTable_News();
            $text       = $model->find($news_id)->toArray();
            //nasolíme text do připraveného formuláře
            $form       = new Form_NewsformCZ('/admin/news/save/');
            $form->populate($text[0]);
        }    
        $this->view->form = $form;
    }
    
    public function deleteAction() {
        $news_id = $this->getParam('news_id');
        if (!empty($news_id)) {
            $table = new Model_DbTable_News();
            $where = $table->getAdapter()->quoteInto('news_id = ?', $news_id);
            $table->delete($where);
        }
        $this->_redirect('/admin/index/');
    }
    
    public function saveAction() {
        $news_id = $this->getParam('news_id');  
        $model = new Model_DbTable_News();
        
        //podíváme se, zda je nahraný obrázek
        $form = new Form_NewsformCZ();
        if ($form->upload->isUploaded()) {
            $path = getcwd() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'eshop_news' . DIRECTORY_SEPARATOR;
            $target_name = makeAlias($form->upload->getFileName());
            $target_path = $path . $target_name;

            $validator = new Zend_Validate_File_Exists();
            $validator->addDirectory(getcwd());

            if ($validator->isValid($target_path)) {
                unlink($target_path);
            }
            $form->upload->setDestination($path);
            
            $filterChain = new Zend_Filter();
            $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
                'directory' => $path,
                'width' => 484,
                'height' => 426,
                'cropToFit' => true,
                'keepRatio' => true,
                'keepSmaller' => false,
            )));            
            $form->upload->addFilter($filterChain);
            $form->upload->addFilter(new Zend_Filter_File_Rename(array('target' => $target_name)));
            try {
                $form->upload->receive();
            } catch (Zend_File_Transfer_Exception $e) {
                throw new Exception('Unable to recieve : ' . $e->getMessage() . " <br/>PATH: " . $path);
            }
            //musíme okleštit filename od celé cesty
            $image = basename($target_name);
        } else {            
            $image = $this->getParam('image');
        }
        if (!empty($news_id)) {
            $data = array();
            $data['date'] = $this->getParam('date');
            $data['eshop_id'] = $this->getParam('eshop_select');
            $data['title_cz'] = $this->getParam('title_cz');
            $data['text_cz'] = $this->getParam('text_cz');
            if (!empty($image)) {
                $data['image'] = $image;                
            }
            $where = $model->getAdapter()->quoteInto('news_id = ?', $news_id);
            $model->update($data, $where);
        } else {
            $data = array();
            $data['date'] = $this->getParam('date');
            $data['eshop_id'] = $this->getParam('eshop_select');
            $data['title_cz'] = $this->getParam('title_cz');
            $data['text_cz'] = $this->getParam('text_cz');
            $image = $this->getParam('image');
            if (!empty($image)) {
                $data['image'] = $image;                
            }
            $model->insert($data);
        }
        //$this->_redirect('/admin/news/');
    }
   
}

