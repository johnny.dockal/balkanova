var myTWin = window.myTWin;

// after page loads
$(document).ready(function () {
    
});

function OpenMyWin(link,winName)
{
  var retValue=true;
  if (myTWin!=null && !myTWin.closed)
  {
    myTWin.focus();
    myTWin.location.href=link.href;
  }
  else
  {
    myTWin=window.open(""+link.href,winName,"left=20,top=20,width=725,height=480,toolbar=1,resizable=0");
    if (myTWin==null || typeof(myTWin)=="undefined")
      retValue=false;
    else
    {
      link.target=winName;
      myTWin.focus();
    }
  }
  return retValue;
}

function showKosik(id, num) {
    var elementkosik = 'kosik' + id;
    el = document.getElementById(elementkosik).style;
    el.display = (el.display == 'none') ? 'block' : 'block';

    var elementpocet = 'pocet' + id;
    el = document.getElementById(elementpocet).style;
    el.display = (el.display == 'none') ? 'block' : 'block';

    var i, j;
    i = document.getElementById(elementpocet).innerHTML;
    i = parseInt(i);
    num = parseInt(num);
    j = i + num;
    j = parseInt(j);
    el = document.getElementById(elementpocet).innerHTML = parseInt(j);
}

function showDiffform(checkbox) {
    if (checkbox.checked) {
        $('#fieldset-diffform').show();
    } else {
        $('#fieldset-diffform').hide();
    }
}

function showComform(checkbox) {
    if (checkbox.checked) {
        //$('#fieldset-nameform').hide();
        $('#fieldset-companyform').show();
    } else {
        $('#fieldset-companyform').hide();
        //$('#fieldset-nameform').show();
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}